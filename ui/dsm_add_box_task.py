# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './ui/dsm_add_box_task.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_DSMAddBox(object):
    def setupUi(self, DSMAddBox):
        DSMAddBox.setObjectName("DSMAddBox")
        DSMAddBox.resize(470, 214)
        font = QtGui.QFont()
        font.setFamily("Open Sans")
        font.setPointSize(11)
        DSMAddBox.setFont(font)
        self.gridLayout = QtWidgets.QGridLayout(DSMAddBox)
        self.gridLayout.setObjectName("gridLayout")
        self.label = QtWidgets.QLabel(DSMAddBox)
        self.label.setObjectName("label")
        self.gridLayout.addWidget(self.label, 0, 0, 1, 1)
        self.name_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.name_lineedit.setObjectName("name_lineedit")
        self.gridLayout.addWidget(self.name_lineedit, 0, 1, 1, 6)
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.label_2 = QtWidgets.QLabel(DSMAddBox)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout.addWidget(self.label_2)
        self.length_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.length_lineedit.setObjectName("length_lineedit")
        self.horizontalLayout.addWidget(self.length_lineedit)
        self.label_3 = QtWidgets.QLabel(DSMAddBox)
        self.label_3.setObjectName("label_3")
        self.horizontalLayout.addWidget(self.label_3)
        self.width_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.width_lineedit.setObjectName("width_lineedit")
        self.horizontalLayout.addWidget(self.width_lineedit)
        self.label_5 = QtWidgets.QLabel(DSMAddBox)
        self.label_5.setObjectName("label_5")
        self.horizontalLayout.addWidget(self.label_5)
        self.height_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.height_lineedit.setObjectName("height_lineedit")
        self.horizontalLayout.addWidget(self.height_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout, 1, 0, 1, 7)
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.label_6 = QtWidgets.QLabel(DSMAddBox)
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_2.addWidget(self.label_6)
        self.x_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.x_lineedit.setObjectName("x_lineedit")
        self.horizontalLayout_2.addWidget(self.x_lineedit)
        self.label_7 = QtWidgets.QLabel(DSMAddBox)
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_2.addWidget(self.label_7)
        self.y_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.y_lineedit.setObjectName("y_lineedit")
        self.horizontalLayout_2.addWidget(self.y_lineedit)
        self.label_8 = QtWidgets.QLabel(DSMAddBox)
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_2.addWidget(self.label_8)
        self.z_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.z_lineedit.setObjectName("z_lineedit")
        self.horizontalLayout_2.addWidget(self.z_lineedit)
        self.label_9 = QtWidgets.QLabel(DSMAddBox)
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_2.addWidget(self.label_9)
        self.rx_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.rx_lineedit.setObjectName("rx_lineedit")
        self.horizontalLayout_2.addWidget(self.rx_lineedit)
        self.label_10 = QtWidgets.QLabel(DSMAddBox)
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_2.addWidget(self.label_10)
        self.ry_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.ry_lineedit.setObjectName("ry_lineedit")
        self.horizontalLayout_2.addWidget(self.ry_lineedit)
        self.label_11 = QtWidgets.QLabel(DSMAddBox)
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_2.addWidget(self.label_11)
        self.rz_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.rz_lineedit.setObjectName("rz_lineedit")
        self.horizontalLayout_2.addWidget(self.rz_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout_2, 2, 0, 1, 7)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label_4 = QtWidgets.QLabel(DSMAddBox)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_3.addWidget(self.label_4)
        self.parent_frame_lineedit = QtWidgets.QLineEdit(DSMAddBox)
        self.parent_frame_lineedit.setObjectName("parent_frame_lineedit")
        self.horizontalLayout_3.addWidget(self.parent_frame_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout_3, 3, 0, 1, 7)

        self.retranslateUi(DSMAddBox)
        QtCore.QMetaObject.connectSlotsByName(DSMAddBox)

    def retranslateUi(self, DSMAddBox):
        _translate = QtCore.QCoreApplication.translate
        DSMAddBox.setWindowTitle(_translate("DSMAddBox", "Form"))
        self.label.setText(_translate("DSMAddBox", "Name"))
        self.label_2.setText(_translate("DSMAddBox", "Length"))
        self.label_3.setText(_translate("DSMAddBox", "Width"))
        self.label_5.setText(_translate("DSMAddBox", "Height"))
        self.label_6.setText(_translate("DSMAddBox", "X"))
        self.label_7.setText(_translate("DSMAddBox", "Y"))
        self.label_8.setText(_translate("DSMAddBox", "Z"))
        self.label_9.setText(_translate("DSMAddBox", "RX"))
        self.label_10.setText(_translate("DSMAddBox", "RY"))
        self.label_11.setText(_translate("DSMAddBox", "RZ"))
        self.label_4.setText(_translate("DSMAddBox", "Parent Frame [optional]"))
