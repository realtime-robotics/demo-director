# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './ui/wait_task.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_WaitTask(object):
    def setupUi(self, WaitTask):
        WaitTask.setObjectName("WaitTask")
        WaitTask.resize(434, 151)
        WaitTask.setMinimumSize(QtCore.QSize(430, 0))
        font = QtGui.QFont()
        font.setFamily("Open Sans")
        font.setPointSize(12)
        WaitTask.setFont(font)
        self.verticalLayout_5 = QtWidgets.QVBoxLayout(WaitTask)
        self.verticalLayout_5.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout_5.setObjectName("verticalLayout_5")
        self.horizontalLayout_2 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_2.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_2.setObjectName("horizontalLayout_2")
        self.verticalLayout = QtWidgets.QVBoxLayout()
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(WaitTask)
        self.label.setAlignment(QtCore.Qt.AlignCenter)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.wait_time_spinbox = QtWidgets.QDoubleSpinBox(WaitTask)
        self.wait_time_spinbox.setMaximum(600.0)
        self.wait_time_spinbox.setObjectName("wait_time_spinbox")
        self.verticalLayout.addWidget(self.wait_time_spinbox)
        self.horizontalLayout_2.addLayout(self.verticalLayout)
        spacerItem = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem)
        self.label_2 = QtWidgets.QLabel(WaitTask)
        self.label_2.setObjectName("label_2")
        self.horizontalLayout_2.addWidget(self.label_2)
        spacerItem1 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem1)
        self.verticalLayout_2 = QtWidgets.QVBoxLayout()
        self.verticalLayout_2.setObjectName("verticalLayout_2")
        self.label_3 = QtWidgets.QLabel(WaitTask)
        self.label_3.setAlignment(QtCore.Qt.AlignCenter)
        self.label_3.setObjectName("label_3")
        self.verticalLayout_2.addWidget(self.label_3)
        self.loop_add_time = QtWidgets.QDoubleSpinBox(WaitTask)
        self.loop_add_time.setMaximum(600.0)
        self.loop_add_time.setObjectName("loop_add_time")
        self.verticalLayout_2.addWidget(self.loop_add_time)
        self.horizontalLayout_2.addLayout(self.verticalLayout_2)
        spacerItem2 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Fixed, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem2)
        self.label_4 = QtWidgets.QLabel(WaitTask)
        self.label_4.setObjectName("label_4")
        self.horizontalLayout_2.addWidget(self.label_4)
        spacerItem3 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_2.addItem(spacerItem3)
        self.verticalLayout_5.addLayout(self.horizontalLayout_2)
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setContentsMargins(0, -1, 0, -1)
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.verticalLayout_3 = QtWidgets.QVBoxLayout()
        self.verticalLayout_3.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout_3.setObjectName("verticalLayout_3")
        self.label_5 = QtWidgets.QLabel(WaitTask)
        self.label_5.setAlignment(QtCore.Qt.AlignCenter)
        self.label_5.setObjectName("label_5")
        self.verticalLayout_3.addWidget(self.label_5)
        self.min_noise_spinbox = QtWidgets.QDoubleSpinBox(WaitTask)
        self.min_noise_spinbox.setMaximum(100.0)
        self.min_noise_spinbox.setSingleStep(0.25)
        self.min_noise_spinbox.setObjectName("min_noise_spinbox")
        self.verticalLayout_3.addWidget(self.min_noise_spinbox)
        self.horizontalLayout_3.addLayout(self.verticalLayout_3)
        self.verticalLayout_4 = QtWidgets.QVBoxLayout()
        self.verticalLayout_4.setContentsMargins(0, -1, -1, -1)
        self.verticalLayout_4.setObjectName("verticalLayout_4")
        self.label_6 = QtWidgets.QLabel(WaitTask)
        self.label_6.setAlignment(QtCore.Qt.AlignCenter)
        self.label_6.setObjectName("label_6")
        self.verticalLayout_4.addWidget(self.label_6)
        self.max_noise_spinbox = QtWidgets.QDoubleSpinBox(WaitTask)
        self.max_noise_spinbox.setMaximum(100.0)
        self.max_noise_spinbox.setSingleStep(0.25)
        self.max_noise_spinbox.setObjectName("max_noise_spinbox")
        self.verticalLayout_4.addWidget(self.max_noise_spinbox)
        self.horizontalLayout_3.addLayout(self.verticalLayout_4)
        self.noise_error_label = QtWidgets.QLabel(WaitTask)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Preferred)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.noise_error_label.sizePolicy().hasHeightForWidth())
        self.noise_error_label.setSizePolicy(sizePolicy)
        self.noise_error_label.setMinimumSize(QtCore.QSize(150, 0))
        self.noise_error_label.setMaximumSize(QtCore.QSize(200, 16777215))
        font = QtGui.QFont()
        font.setFamily("Open Sans")
        font.setPointSize(10)
        self.noise_error_label.setFont(font)
        self.noise_error_label.setStyleSheet("color: rgb(199, 0, 0);")
        self.noise_error_label.setText("")
        self.noise_error_label.setWordWrap(True)
        self.noise_error_label.setObjectName("noise_error_label")
        self.horizontalLayout_3.addWidget(self.noise_error_label)
        spacerItem4 = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.horizontalLayout_3.addItem(spacerItem4)
        self.verticalLayout_5.addLayout(self.horizontalLayout_3)

        self.retranslateUi(WaitTask)
        QtCore.QMetaObject.connectSlotsByName(WaitTask)

    def retranslateUi(self, WaitTask):
        _translate = QtCore.QCoreApplication.translate
        WaitTask.setWindowTitle(_translate("WaitTask", "Form"))
        self.label.setText(_translate("WaitTask", "Wait Time"))
        self.wait_time_spinbox.setSuffix(_translate("WaitTask", "s"))
        self.label_2.setText(_translate("WaitTask", "+"))
        self.label_3.setText(_translate("WaitTask", "Added Time Per Loop"))
        self.loop_add_time.setSuffix(_translate("WaitTask", "s"))
        self.label_4.setText(_translate("WaitTask", "+"))
        self.label_5.setText(_translate("WaitTask", "Min Noise"))
        self.min_noise_spinbox.setSuffix(_translate("WaitTask", "s"))
        self.label_6.setText(_translate("WaitTask", "Max Noise"))
        self.max_noise_spinbox.setSuffix(_translate("WaitTask", "s"))
