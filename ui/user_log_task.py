# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './ui/user_log_task.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_UserLog(object):
    def setupUi(self, UserLog):
        UserLog.setObjectName("UserLog")
        UserLog.resize(590, 58)
        font = QtGui.QFont()
        font.setFamily("Open Sans")
        font.setPointSize(11)
        UserLog.setFont(font)
        self.verticalLayout = QtWidgets.QVBoxLayout(UserLog)
        self.verticalLayout.setContentsMargins(0, 0, 0, 0)
        self.verticalLayout.setObjectName("verticalLayout")
        self.label = QtWidgets.QLabel(UserLog)
        font = QtGui.QFont()
        font.setFamily("Open Sans")
        font.setPointSize(11)
        self.label.setFont(font)
        self.label.setObjectName("label")
        self.verticalLayout.addWidget(self.label)
        self.message_line_edit = QtWidgets.QLineEdit(UserLog)
        self.message_line_edit.setMaxLength(500)
        self.message_line_edit.setObjectName("message_line_edit")
        self.verticalLayout.addWidget(self.message_line_edit)

        self.retranslateUi(UserLog)
        QtCore.QMetaObject.connectSlotsByName(UserLog)

    def retranslateUi(self, UserLog):
        _translate = QtCore.QCoreApplication.translate
        UserLog.setWindowTitle(_translate("UserLog", "Form"))
        self.label.setText(_translate("UserLog", "Message:"))
