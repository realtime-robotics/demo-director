# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './ui/rs_add_expected_object_task.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_RS_AddExpectedObject(object):
    def setupUi(self, RS_AddExpectedObject):
        RS_AddExpectedObject.setObjectName("RS_AddExpectedObject")
        RS_AddExpectedObject.resize(502, 93)
        self.gridLayout = QtWidgets.QGridLayout(RS_AddExpectedObject)
        self.gridLayout.setContentsMargins(0, 0, 0, 0)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_3 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_3.setObjectName("horizontalLayout_3")
        self.label = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label.setObjectName("label")
        self.horizontalLayout_3.addWidget(self.label)
        self.filter_id_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        self.filter_id_lineedit.setObjectName("filter_id_lineedit")
        self.horizontalLayout_3.addWidget(self.filter_id_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout_3, 0, 1, 1, 1)
        self.horizontalLayout_5 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_5.setObjectName("horizontalLayout_5")
        self.label_6 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_6.setMinimumSize(QtCore.QSize(45, 0))
        self.label_6.setObjectName("label_6")
        self.horizontalLayout_5.addWidget(self.label_6)
        self.centroid_x_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centroid_x_lineedit.sizePolicy().hasHeightForWidth())
        self.centroid_x_lineedit.setSizePolicy(sizePolicy)
        self.centroid_x_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.centroid_x_lineedit.setObjectName("centroid_x_lineedit")
        self.horizontalLayout_5.addWidget(self.centroid_x_lineedit)
        self.label_7 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_7.setMinimumSize(QtCore.QSize(45, 0))
        self.label_7.setObjectName("label_7")
        self.horizontalLayout_5.addWidget(self.label_7)
        self.centroid_y_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centroid_y_lineedit.sizePolicy().hasHeightForWidth())
        self.centroid_y_lineedit.setSizePolicy(sizePolicy)
        self.centroid_y_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.centroid_y_lineedit.setObjectName("centroid_y_lineedit")
        self.horizontalLayout_5.addWidget(self.centroid_y_lineedit)
        self.label_11 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_11.setMinimumSize(QtCore.QSize(45, 0))
        self.label_11.setObjectName("label_11")
        self.horizontalLayout_5.addWidget(self.label_11)
        self.centroid_z_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.centroid_z_lineedit.sizePolicy().hasHeightForWidth())
        self.centroid_z_lineedit.setSizePolicy(sizePolicy)
        self.centroid_z_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.centroid_z_lineedit.setObjectName("centroid_z_lineedit")
        self.horizontalLayout_5.addWidget(self.centroid_z_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout_5, 1, 1, 1, 1)
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.label_8 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_8.setMinimumSize(QtCore.QSize(45, 0))
        self.label_8.setObjectName("label_8")
        self.horizontalLayout_6.addWidget(self.label_8)
        self.length_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.length_lineedit.sizePolicy().hasHeightForWidth())
        self.length_lineedit.setSizePolicy(sizePolicy)
        self.length_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.length_lineedit.setObjectName("length_lineedit")
        self.horizontalLayout_6.addWidget(self.length_lineedit)
        self.label_9 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_9.setMinimumSize(QtCore.QSize(45, 0))
        self.label_9.setObjectName("label_9")
        self.horizontalLayout_6.addWidget(self.label_9)
        self.width_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.width_lineedit.sizePolicy().hasHeightForWidth())
        self.width_lineedit.setSizePolicy(sizePolicy)
        self.width_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.width_lineedit.setObjectName("width_lineedit")
        self.horizontalLayout_6.addWidget(self.width_lineedit)
        self.label_10 = QtWidgets.QLabel(RS_AddExpectedObject)
        self.label_10.setMinimumSize(QtCore.QSize(45, 0))
        self.label_10.setObjectName("label_10")
        self.horizontalLayout_6.addWidget(self.label_10)
        self.height_lineedit = QtWidgets.QLineEdit(RS_AddExpectedObject)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.height_lineedit.sizePolicy().hasHeightForWidth())
        self.height_lineedit.setSizePolicy(sizePolicy)
        self.height_lineedit.setMaximumSize(QtCore.QSize(60, 16777215))
        self.height_lineedit.setObjectName("height_lineedit")
        self.horizontalLayout_6.addWidget(self.height_lineedit)
        self.gridLayout.addLayout(self.horizontalLayout_6, 2, 1, 1, 1)

        self.retranslateUi(RS_AddExpectedObject)
        QtCore.QMetaObject.connectSlotsByName(RS_AddExpectedObject)

    def retranslateUi(self, RS_AddExpectedObject):
        _translate = QtCore.QCoreApplication.translate
        RS_AddExpectedObject.setWindowTitle(_translate("RS_AddExpectedObject", "Form"))
        self.label.setText(_translate("RS_AddExpectedObject", "Filter ID"))
        self.label_6.setText(_translate("RS_AddExpectedObject", "X"))
        self.label_7.setText(_translate("RS_AddExpectedObject", "Y"))
        self.label_11.setText(_translate("RS_AddExpectedObject", "Z"))
        self.label_8.setText(_translate("RS_AddExpectedObject", "Length"))
        self.label_9.setText(_translate("RS_AddExpectedObject", "Width"))
        self.label_10.setText(_translate("RS_AddExpectedObject", "Height"))
