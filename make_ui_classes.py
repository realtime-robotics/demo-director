#!/usr/bin/env python3

import os
import sys


def main():
    '''
    This script loops over everything in the ui/ folder and converts all
    .ui files to their respective .py class using the pyuic5 converter

    Make sure to run this script from the demo-director folder for correct
    relative paths for the icons.
    '''
    if sys.platform == 'win32':
        print('Windows is not supported yet.')
        return
    if 'ui' not in os.listdir('.'):
        print('Could not locate the ui/ folder. Are you running the script from the demo-director directory?')
        return

    # Loop over all files in the ui/ folder
    files = os.listdir('./ui')
    for file_name in files:
        if file_name[-3:] == '.ui':
            base, extension = file_name.split('.')
            os.system(f'pyuic5 ./ui/{file_name} -o ./ui/{base}.py')

    # Loop over all files in the lib/sick_plb/ui folder
    path = './lib/sick_plb/ui'
    files = os.listdir(path)
    for file_name in files:
        if file_name[-3:] == '.ui':
            base, extension = file_name.split('.')
            os.system(f'pyuic5 {path}/{file_name} -o {path}/{base}.py')


if __name__ == '__main__':
    main()
