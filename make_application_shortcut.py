#!/usr/bin/env python3
import os, shutil, sys, stat

def main():
    # Currently, linux is the only supported platform for installing the application, so exit if os does not detect linux
    if not sys.platform == 'linux':
        print(f'Demo HMI can only be installed as an application on Linux!')
        print(f'To run the application on other platforms, execute the python script directly.')
        print(f'Usage: python3 DemoDirector.py')
        return

    # Here we're asserting that the user is executing the install.py script from within the demo-director folder
    # by checking if the ui and lib folders are found in the output of ls
    ls = os.listdir('.')
    if 'ui' not in ls and 'lib' not in ls:
        print('You must execute the install.py script from the demo-director directory!')
        return

    # Ensure the DemoDirector.py file is executable
    os.chmod('./DemoDirector.py',stat.S_IRWXU)

    # Now create the DemoDirector.desktop file that linux expects in order to create a dashboard entry
    cwd = os.getcwd()
    with open('DemoDirector.desktop','w') as fp:
        fp.write(f'[Desktop Entry]\n')
        fp.write(f'Version=1.0.0\n')
        fp.write(f'Name=DemoDirector\n')
        fp.write(f'Comment=DemoDirector compatible with Rapidplan >2.2\n')
        fp.write(f'Exec={cwd}/DemoDirector.py\n')
        fp.write(f'Icon={cwd}/ui/icons/demo_director_icon.png\n')
        fp.write(f'Path={cwd}/\n')
        fp.write(f'Terminal=false\n')
        fp.write(f'Type=Application\n')
        fp.write(f'Categories=Utility;Application;\n')

    # Remove the existing dashboard entry from the current users local applications folder, if one exists, and then copy the new
    # DemoDirector.desktop file to the directory
    username = os.getlogin()
    try:
        os.remove(f'/home/{username}/.local/share/applications/DemoDirector.desktop')
        print('Removing existing desktop configuration.')
    except FileNotFoundError:
        print('Existing desktop file not found, so not removed.')
    shutil.copy('./DemoDirector.desktop',f'/home/{username}/.local/share/applications/DemoDirector.desktop')
    
    print('Desktop entry created successfully. Search for "DemoDirector", right click, then select "Add to favorites" to pin it')
    print('to the taskbar.')

if __name__ == '__main__':
    main()