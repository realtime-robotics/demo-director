# Demo Director
The intent of this software is to provide a GUI that acts as a task manager to test Realtime Robotic's RapidPlan software.  In short, it allows you to command multiple robots within a RapidPlan project to perform a specific sequence of tasks.  This can be very useful in visualizing how robots will interact with each other in the real world.  It can also be used to perform some common engineering tasks during the design phase of a project such as estimate cycle time.

## Terminology
The overall structure of the software is designed specifically for use with RapidPlan.  The concepts, and subsequent terminology, you may see in the GUI or documentation are listed below.
### Task Manager:
In general, this is a tool that is used to make assets perform tasks.  In this software, it is specificaly used to command robots that are being controlled by RapidPlan.

### Task Plan:
A task plan is meant to coordinate the control of every robot in a RapidPlan project via task lists.  Only one task plan can be running at a time. A task plan has a tab for every robot in the project.

### Task List:
The sequence of tasks that are executed for a given robot.  Each robot has its own task list for every task plan. You can modify the task list at any time, even while the task plan is running. You can modify the list by:
* Dragging and dropping to re-order the list
* Using the up/down arrow buttons on the right of the task list to move the selected line up or down a row.
* using the up/down arrow bars to move the task to the top or bottom of the task list.
* Use the trash can icon to remove the selected task.
* Use the delete key to remove the selected task.
* Use the pencil icon to edit the selected task.
* Use the double page icon to duplicate the selected task. You can also use ctrl+c/x/v to cut, copy, and paste a task

 ### Task:
A single action taken when a step in a task list is run.  The tasks available are fixed and do not cover everything that RapidPlan is capable of.  Tasks include functions mapped from the Realtime ASCII Interface (e.g. Move, SetInterruptBehavior) along with some general task manager requirements (e.g. Wait, Interlock).

## Dependencies
The application is build using PyQt5, which is installed along with Rapidplan, so if you have a megadeb installed, then you can skip straight to `Application Shortcut` or `Running From Source`. If not, then install the PyQt5 binaries and python dependencies with
```
pip3 install requests
pip3 install pyqt5
```
If you will be using the OnRobot gripper or Moxa IO module, then you will need to install the Modbus library with
```
pip3 install pymodbus==2.5.3
pip3 install pyserial-asyncio
```

## Application Shortcut
On linux, navigate to the `demo-director` folder in a terminal, and then run the `make_application_shortcut.py` file. Make sure the file path contains only letters, numbers, and underscores!
```
cd /path/to/demo-director
python3 make_application_shortcut.py
```
If the instaltion completes successfully, you should see the message
```
Desktop entry created successfully. Search for "DemoDirector", right click, then select "Add to favorites" to pin it to the taskbar.
```
print in the terminal.

Once installed, you can search for the application `DemoDirector` and add it to your favorites bar.

### Common Issues
* You are not using linux. Linux is the only supported enviornment for creating an installed application. On other platforms you can still run the application from source.
* demo-director is not your current working directory. The installer works by creating references to the actual folder, and does not install by copying the files to system directories which is technically the 'correct' way to do it. The installer expects to be run in the demo-director folder, so it knows where to find the rest of the files.
* You have moved the demo-director folder and now the application wont launch. As noted in the point above, the system creates references to the demo-director folder, and not a copy. This is simpler for the time being, but it does mean you'll have to rerun the `install.py` script if you move the demo-director folder.
* The folder was a duplicate and ' (#)' was appended to the name by the OS. Ensure only letters, numbers, and underscores are in the file path!

## Running from Source
If you don't want to install it as a system application, or you're on Windows, you can run the python file directly.
```
cd /path/to/demo-director
python3 DemoDirector.py
```
### Command Line Arguments
--noload : This will not load the cached "default" project on startup.

-d : This will launch DemoDirector with debug logs enabled. Note: you can also toggle debug logs under the About tab of the menu bar.

## Making Changes to DemoDirector
Most UI elements in Demo Director make use of Qt Desinger to generate the widget. This does not come installed with PyQt5 by default, so run either command to get a version of Qt Designer.
```
pip3 install pyqt5-tools
or
sudo apt-get install qttools5-dev-tools
```
then to generate python classes from the ui files you can use the helper script `make_ui_classes.py`. Navigate to the `demo-director` folder in a terminal, and then run the `make_ui_classes.py` file. This will convert all .ui files in the ui/ folder to python classes for you, and it must be run from the demo-director folder in order to preserve relative paths for icons used in the GUI.
```
cd /path/to/demo-director/
python3 make_application_shortcut.py
```