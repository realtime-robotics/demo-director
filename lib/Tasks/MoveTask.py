#!/usr/bin/env python3

from lib.appliance_client.appliance_client import ApplianceClient, Handler, Message
from lib.appliance_client.messages.ASCIICancelMove import ASCIICancelMove
from lib.appliance_client.messages.ASCIIMove import ASCIIMove
from lib.GlobalVariables import GlobalVariables
from lib.Utils import initialize_move_time_logger
from PyQt5.QtWidgets import QButtonGroup
from lib.Tasks.Task import Task
from typing import Any
import ui.move_task
import time
import copy


class MoveTask(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, presets_targets_dict, global_variables: GlobalVariables, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='Move',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.presets_targets_dict = presets_targets_dict
        self.global_variables = global_variables
        self.running = False
        self.timeout = None
        self.planned = True
        self.planning_time = 0
        self.complete = True
        self.error = False
        self.error_code = 0

        self.move_time_logger = initialize_move_time_logger(self.robot_name)

    def load_ui(self):
        self.ui = ui.move_task.Ui_MoveTask()
        self.ui.setupUi(self)
        self.configure_button_groups()
        self.ui.move_type_tab_widget.setCurrentIndex(0)
        self.ui.presets_combobox.addItem('< use active >')
        self.ui.presets_combobox.addItems(self.presets_targets_dict.keys())
        self.ui.presets_combobox.activated.connect(self.refresh_targets)
        self.ui.type_combobox.currentTextChanged.connect(self.type_change)
        self.refresh_targets()
        self.type_change()

    def refresh_targets(self):
        preset = self.ui.presets_combobox.currentText()
        if preset not in ['< use active >','']:
            targets = self.presets_targets_dict[preset]
        else:
            # Loop over every preset, and create a list of all unique assigned targets
            targets = []
            for prest in self.presets_targets_dict:
                for target in self.presets_targets_dict[prest]:
                    if target not in targets:
                        targets.append(str(target))
        targets.sort()
        self.ui.target_combobox.clear()
        self.ui.target_combobox.addItems(targets)

    def type_change(self):
        current_text = self.ui.type_combobox.currentText()
        external_axis_group = [self.ui.external_axis_1_name, self.ui.external_axis_1_inactive, self.ui.external_axis_1_active, self.ui.external_axis_1_value, self.ui.external_axis_1_value_setpoint,
                               self.ui.external_axis_2_active, self.ui.external_axis_2_inactive, self.ui.external_axis_2_name, self.ui.external_axis_2_value, self.ui.external_axis_2_value_setpoint]
        if current_text == 'Roadmap':
            self.ui.interp_label.setEnabled(False)
            self.ui.interp_combobox.setEnabled(False)
            self.ui.collision_check_label.setEnabled(False)
            self.ui.collision_check_off.setEnabled(False)
            self.ui.collision_check_on.setEnabled(False)
            self.ui.dsm_collision_check_label.setEnabled(False)
            self.ui.dsm_collision_check_off.setEnabled(False)
            self.ui.dsm_collision_check_on.setEnabled(False)
            self.ui.pose_absolute.setChecked(True)
            self.set_pose_abs_rel_enabled(True)
            self.ui.joint_absolute.setChecked(True)
            self.ui.joint_absolute.setEnabled(False)
            self.ui.joint_relative.setEnabled(False)
            self.set_external_axis_enabled(True)
            self.ui.planning_timeout_label.setEnabled(False)
            self.ui.planning_timeout_spinbox.setEnabled(False)
            self.ui.move_type_tab_widget.setTabEnabled(2, True)
            self.ui.constraints_frame.setEnabled(False)
            self.ui.roadmap_mode_combobox.setEnabled(True)
            self.ui.roadmap_mode_label.setEnabled(True)
        elif current_text == 'Direct':
            self.ui.interp_label.setEnabled(True)
            self.ui.interp_combobox.setEnabled(True)
            self.ui.collision_check_label.setEnabled(True)
            self.ui.collision_check_off.setEnabled(True)
            self.ui.collision_check_on.setEnabled(True)
            self.ui.dsm_collision_check_label.setEnabled(True)
            self.ui.dsm_collision_check_off.setEnabled(True)
            self.ui.dsm_collision_check_on.setEnabled(True)
            self.set_pose_abs_rel_enabled(True)
            self.ui.joint_absolute.setEnabled(True)
            self.ui.joint_relative.setEnabled(True)
            self.set_external_axis_enabled(True)
            self.ui.planning_timeout_label.setEnabled(False)
            self.ui.planning_timeout_spinbox.setEnabled(False)
            self.ui.move_type_tab_widget.setTabEnabled(2, True)
            self.ui.roadmap_mode_combobox.setEnabled(False)
            self.ui.roadmap_mode_label.setEnabled(False)
        elif current_text == 'Planning':
            self.ui.interp_label.setEnabled(False)
            self.ui.interp_combobox.setEnabled(False)
            self.ui.collision_check_label.setEnabled(False)
            self.ui.collision_check_off.setEnabled(False)
            self.ui.collision_check_on.setEnabled(False)
            self.ui.dsm_collision_check_label.setEnabled(False)
            self.ui.dsm_collision_check_off.setEnabled(False)
            self.ui.dsm_collision_check_on.setEnabled(False)
            self.ui.pose_absolute.setChecked(True)
            self.set_pose_abs_rel_enabled(True)
            self.ui.joint_absolute.setChecked(True)
            self.ui.joint_absolute.setEnabled(False)
            self.ui.joint_relative.setEnabled(False)
            self.set_external_axis_enabled(False)
            self.ui.planning_timeout_label.setEnabled(True)
            self.ui.planning_timeout_spinbox.setEnabled(True)
            self.ui.move_type_tab_widget.setTabEnabled(2, False)
            self.ui.constraints_frame.setEnabled(True)
            self.ui.roadmap_mode_combobox.setEnabled(False)
            self.ui.roadmap_mode_label.setEnabled(False)

    def set_external_axis_enabled(self, state: bool) -> None:
        self.ui.external_axis_1_name.setEnabled(state)
        self.ui.external_axis_1_inactive.setEnabled(state)
        self.ui.external_axis_1_active.setEnabled(state)
        self.ui.external_axis_1_value.setEnabled(state)
        self.ui.external_axis_1_value_setpoint.setEnabled(state)
        self.ui.external_axis_2_active.setEnabled(state)
        self.ui.external_axis_2_inactive.setEnabled(state)
        self.ui.external_axis_2_name.setEnabled(state)
        self.ui.external_axis_2_value.setEnabled(state)
        self.ui.external_axis_2_value_setpoint.setEnabled(state)
        self.ui.external_axis_label.setEnabled(state)
        if not state:
            self.ui.external_axis_1_inactive.setChecked(True)
            self.ui.external_axis_2_inactive.setChecked(True)

    def set_pose_abs_rel_enabled(self, state: bool) -> None:
        self.ui.pose_absolute.setEnabled(state)
        self.ui.pose_relative.setEnabled(state)
        self.ui.pose_reference_frame.setEnabled(state)
        self.ui.pose_reference_frame_label.setEnabled(state)

    def get_constraints_array(self):
        constraint_array = [0,0,0]
        constraint_count = 0
        if self.ui.x_orientation_constraint.isChecked():
            constraint_array[0] = 1
            constraint_count += 1
        if self.ui.y_orientation_constraint.isChecked():
            constraint_array[1] = 1
            constraint_count += 1
        if self.ui.z_orientation_constraint.isChecked():
            constraint_array[2] = 1
            constraint_count += 1
        if constraint_count == 1 and self.ui.type_combobox.currentText() == 'Planning':
            self.logger.error(f'{constraint_count} constraints were set, but at least 2 are required.')
        return constraint_array
        
    def set_constraints_checkboxes(self,array):
        if array[0]:
            self.ui.x_orientation_constraint.setChecked(True)
        if array[1]:
            self.ui.y_orientation_constraint.setChecked(True)
        if array[2]:
            self.ui.z_orientation_constraint.setChecked(True)

    def apply_ui_to_task(self, execute_once):
        self.parameters = {'execute_once': execute_once}

        def get_goal_type_from_idx():
            if self.ui.move_type_tab_widget.currentIndex() == 0:
                return 'target'
            elif self.ui.move_type_tab_widget.currentIndex() == 1:
                return 'pose'
            elif self.ui.move_type_tab_widget.currentIndex() == 2:
                return 'joint_config'
            else:
                return None
        self.parameters['goal_type'] = get_goal_type_from_idx()
        self.parameters['pose'] = [self.ui.x.text(), self.ui.y.text(), self.ui.z.text(),
                                   self.ui.rx.text(), self.ui.ry.text(), self.ui.rz.text()]
        self.parameters['target'] = self.ui.target_combobox.currentText()
        self.parameters['joint_config'] = [self.ui.j1.text(), self.ui.j2.text(), self.ui.j3.text(), self.ui.j4.text(
        ), self.ui.j5.text(), self.ui.j6.text(), {'state': self.ui.j7_active.isChecked(), 'value': self.ui.j7.text()}, {'state': self.ui.j8_active.isChecked(), 'value': self.ui.j8.text()}]
        self.parameters['speed'] = self.ui.speed_spinbox.value()
        self.parameters['move_type'] = self.ui.type_combobox.currentText().lower()
        self.parameters['smoothing'] = self.ui.smoothing_spinbox.value()
        self.parameters['interp'] = self.ui.interp_combobox.currentIndex()
        self.parameters['collision_check'] = self.ui.collision_check_on.isChecked()
        self.parameters['collision_check_dsm'] = self.ui.dsm_collision_check_on.isChecked()
        self.parameters['timeout'] = self.ui.planning_timeout_spinbox.value()
        self.parameters['pose_relative'] = self.ui.pose_relative.isChecked()
        self.parameters['joint_relative'] = self.ui.joint_relative.isChecked()
        if self.ui.pose_reference_frame.text() == '':
            self.parameters['ref_frame'] = None
        else:
            self.parameters['ref_frame'] = self.ui.pose_reference_frame.text()
        if self.ui.presets_combobox.currentText() in ['< use active >','']:
            self.parameters['preset_name'] = None
        else:
            self.parameters['preset_name'] = self.ui.presets_combobox.currentText()
        self.parameters['trigger'] = None  # not implemented
        self.parameters['queue'] = self.ui.queue.isChecked()
        self.parameters['roadmap_mode'] = self.ui.roadmap_mode_combobox.currentIndex()
        self.parameters['constraints'] = self.get_constraints_array()

        def get_external_axis_1_state():
            if self.ui.external_axis_1_inactive.isChecked():
                return 'inactive'
            if self.ui.external_axis_1_active.isChecked():
                return 'active'
            if self.ui.external_axis_1_value.isChecked():
                return 'value'

        def get_external_axis_2_state():
            if self.ui.external_axis_2_inactive.isChecked():
                return 'inactive'
            if self.ui.external_axis_2_active.isChecked():
                return 'active'
            if self.ui.external_axis_2_value.isChecked():
                return 'value'
        self.parameters['external_axis'] = [{'name': self.ui.external_axis_1_name.text(), 'state': get_external_axis_1_state(), 'value': self.ui.external_axis_1_value_setpoint.text()}, {
            'name': self.ui.external_axis_2_name.text(), 'state': get_external_axis_2_state(), 'value': self.ui.external_axis_2_value_setpoint.text()}]
        # determine which parameters to send from external axis on move cmd
        self.parameters['external_axis_cmd'] = {}
        for item in self.parameters['external_axis']:
            if item['state'] == 'inactive':
                self.parameters['external_axis_cmd'] = None
            elif item['state'] == 'active':
                self.parameters['external_axis_cmd'][item['name']] = 'active'
            else:
                self.parameters['external_axis_cmd'][item['name']
                                                     ] = item['value']

    def apply_task_to_ui(self):
        # goal_type ui
        def get_idx_from_goal_type():
            if self.parameters['goal_type'] == 'joint_config':
                return 2
            elif self.parameters['goal_type'] == 'pose':
                return 1
            else:
                return 0
        self.ui.move_type_tab_widget.setCurrentIndex(get_idx_from_goal_type())
        # pose ui
        self.ui.x.setText(self.parameters['pose'][0])
        self.ui.y.setText(self.parameters['pose'][1])
        self.ui.z.setText(self.parameters['pose'][2])
        self.ui.rx.setText(self.parameters['pose'][3])
        self.ui.ry.setText(self.parameters['pose'][4])
        self.ui.rz.setText(self.parameters['pose'][5])
        # target ui
        self.ui.target_combobox.setCurrentIndex(
            self.ui.target_combobox.findText(self.parameters['target']))
        # joint_config ui
        self.ui.j1.setText(self.parameters['joint_config'][0])
        self.ui.j2.setText(self.parameters['joint_config'][1])
        self.ui.j3.setText(self.parameters['joint_config'][2])
        self.ui.j4.setText(self.parameters['joint_config'][3])
        self.ui.j5.setText(self.parameters['joint_config'][4])
        self.ui.j6.setText(self.parameters['joint_config'][5])
        self.ui.j7_active.setChecked(
            self.parameters['joint_config'][6]['state'])
        self.ui.j7.setText(self.parameters['joint_config'][6]['value'])
        self.ui.j8_active.setChecked(
            self.parameters['joint_config'][7]['state'])
        self.ui.j8.setText(self.parameters['joint_config'][7]['value'])
        # speed ui
        self.ui.speed_spinbox.setValue(self.parameters['speed'])
        # move_type ui
        if self.parameters['move_type'] == 'roadmap':
            self.ui.type_combobox.setCurrentIndex(0)
        elif self.parameters['move_type'] == 'direct':
            self.ui.type_combobox.setCurrentIndex(1)
        elif self.parameters['move_type'] == 'planning':
            self.ui.type_combobox.setCurrentIndex(2)
        # smoothing ui
        self.ui.smoothing_spinbox.setValue(self.parameters['smoothing'])
        # interp ui
        self.ui.interp_combobox.setCurrentIndex(self.parameters['interp'])
        # colllision_check ui
        self.ui.collision_check_on.setChecked(
            self.parameters['collision_check'])
        self.ui.collision_check_off.setChecked(
            not self.parameters['collision_check'])
        # collision_check_dsm ui
        self.ui.dsm_collision_check_on.setChecked(
            self.parameters['collision_check_dsm']
        )
        self.ui.dsm_collision_check_off.setChecked(
            not self.parameters['collision_check_dsm']
        )
        # timeout ui
        self.ui.planning_timeout_spinbox.setValue(self.parameters['timeout'])
        # pose_relative ui
        self.ui.pose_relative.setChecked(self.parameters['pose_relative'])
        self.ui.pose_absolute.setChecked(not self.parameters['pose_relative'])
        # joint_relative_ui
        self.ui.joint_relative.setChecked(self.parameters['joint_relative'])
        self.ui.joint_absolute.setChecked(
            not self.parameters['joint_relative'])
        # ref_frame ui
        self.ui.pose_reference_frame.setText(self.parameters['ref_frame'])
        # preset ui
        self.ui.presets_combobox.setCurrentIndex(
            self.ui.presets_combobox.findText(self.parameters['preset_name']))
        # trigger ui (Not implemented)
        # external_axis ui
        self.ui.external_axis_1_name.setText(
            self.parameters['external_axis'][0]['name'])
        self.ui.external_axis_1_value_setpoint.setText(
            self.parameters['external_axis'][0]['value'])
        if self.parameters['external_axis'][0]['state'] == 'inactive':
            self.ui.external_axis_1_inactive.setChecked(True)
            self.ui.external_axis_1_active.setChecked(False)
            self.ui.external_axis_1_value.setChecked(False)
        elif self.parameters['external_axis'][0]['state'] == 'active':
            self.ui.external_axis_1_inactive.setChecked(False)
            self.ui.external_axis_1_active.setChecked(True)
            self.ui.external_axis_1_value.setChecked(False)
        elif self.parameters['external_axis'][0]['state'] == 'value':
            self.ui.external_axis_1_inactive.setChecked(False)
            self.ui.external_axis_1_active.setChecked(False)
            self.ui.external_axis_1_value.setChecked(True)

        self.ui.external_axis_2_name.setText(
            self.parameters['external_axis'][1]['name'])
        self.ui.external_axis_2_value_setpoint.setText(
            self.parameters['external_axis'][1]['value'])
        if self.parameters['external_axis'][1] == 'inactive':
            self.ui.external_axis_2_inactive.setChecked(True)
            self.ui.external_axis_2_active.setChecked(False)
            self.ui.external_axis_2_value.setChecked(False)
        elif self.parameters['external_axis'][1] == 'active':
            self.ui.external_axis_2_inactive.setChecked(False)
            self.ui.external_axis_2_active.setChecked(True)
            self.ui.external_axis_2_value.setChecked(False)
        elif self.parameters['external_axis'][0]['state'] == 'value':
            self.ui.external_axis_2_inactive.setChecked(False)
            self.ui.external_axis_2_active.setChecked(False)
            self.ui.external_axis_2_value.setChecked(True)

        self.ui.queue.setChecked(self.parameters['queue'])
        self.ui.roadmap_mode_combobox.setCurrentIndex(self.parameters['roadmap_mode'])
        self.set_constraints_checkboxes(self.parameters['constraints'])

    def get_message(self, **kwargs) -> ASCIIMove:
        self.logger.debug('Getting ASCIIMove message...')
        tic = time.time()
        move_message = ASCIIMove(
            robot_name=self.robot_name,
            speed=self.parameters['speed'] * kwargs.get('speed_override'),
            smoothing=self.parameters['smoothing'],
            move_type=self.parameters['move_type'],
            timeout=self.parameters['timeout'],
            preset_name=self.parameters['preset_name']
        )
        if self.parameters['move_type'] == 'roadmap':
            move_message.interp = self.parameters['interp']
            move_message.roadmap_mode = self.parameters['roadmap_mode']
        elif self.parameters['move_type'] == 'direct':
            move_message.interp = self.parameters['interp']
            move_message.collision_check = self.parameters['collision_check']
            move_message.collision_check_dsm = self.parameters['collision_check_dsm']
            if self.parameters['goal_type'] == 'joint_config':
                move_message.relative = self.parameters['joint_relative']
        elif self.parameters['move_type'] == 'planning':
            constraint_count = 0
            for val in self.parameters['constraints']:
                constraint_count += val
            if constraint_count >= 2:
                move_message.constraints = [{'type': 'orientation', 'axis': self.parameters['constraints']}]
            else:
                self.logger.debug(f'Only {constraint_count} constraints were provided, so omitting the argument entirely.')
        
        if self.parameters['goal_type'] == 'target':
            move_message.target = self.parameters['target']
        elif self.parameters['goal_type'] == 'pose':
            move_message.pose = self.parameters['pose'][:]
            move_message.external_axes = self.parameters['external_axis_cmd']
            move_message.relative = self.parameters['pose_relative']
            move_message.ref_frame = self.parameters['ref_frame']
        elif self.parameters['goal_type'] == 'joint_config':
            move_message.joint_config = self.parameters['joint_config']

        self.convert_global_parameters(move_message)
        self.logger.debug(
            f'[{self.robot_name}] Move message: {move_message} get_message() time: {tic - time.time()}')
        return move_message

    def msg_callback(self, msg: Message):
        if Message.is_response(msg):
            toc = time.time()
            self.planning_time = toc - self.tic
            self.logger.info(f'{self.robot_name}: Response Received ({self.uuid}): {msg} in {self.planning_time:.4f} seconds')
            self.planned = True
        if Message.is_feedback(msg):
            self.logger.info(
                f'{self.robot_name}: Feedback Received ({self.uuid}): {msg}')
        if Message.is_delayed_response(msg):
            self.logger.info(
                f'{self.robot_name}: Delayed Response Received ({self.uuid}): {msg}')
            self.complete = True
        if Message.has_error(msg):
            self.logger.error(
                f'{self.robot_name}: Error Received ({self.uuid}): {msg}')
            # self.error = True
            self.error_code = Message.get_error_code(msg)

    def execute(self, **kwargs):
        self.tic = time.time()
        self.running = True
        self.planned = False
        self.complete = False
        self.error = False
        self.logger.info(f"[{self.robot_name}] moving to [{self.get_goal_value()}] with speed override [{kwargs.get('speed_override')}]")
        move_message = self.get_message(**kwargs)

        if not self.running:
            return
        try:
            handler = self.appliance_client.send_msg(
                move_message, self.msg_callback)
            if not self.parameters['queue']:
                handler.wait(self.timeout, wait_callbacks_timeout=3)
        except Exception as e:
            self.logger.error(f"[{self.robot_name}] {move_message.get_topic()} ERROR: {e}")
            self.error = True
        else:
            execution_time = time.time() - self.tic
            self.logger.info(f'Robot {self.robot_name} {handler.original.get_topic()}  COMPLETE in {execution_time}s')
            self.move_time_logger.info(f'{self.robot_name} {self.get_goal_value()} {move_message.move_type} {move_message.speed} {move_message.smoothing} {self.planning_time:.3f} {execution_time:.3f}')
            
        self.running = False

    def configure_button_groups(self):
        # Create exclusivity for radio button groups
        self.collision_check_group = QButtonGroup()
        self.collision_check_group.addButton(self.ui.collision_check_on)
        self.collision_check_group.addButton(self.ui.collision_check_off)
        self.collision_check_group.setExclusive(True)
        self.ui.collision_check_on.setChecked(True)
        self.dsm_collision_check_group = QButtonGroup()
        self.dsm_collision_check_group.addButton(
            self.ui.dsm_collision_check_on)
        self.dsm_collision_check_group.addButton(
            self.ui.dsm_collision_check_off)
        self.dsm_collision_check_group.setExclusive(True)
        self.ui.dsm_collision_check_on.setChecked(True)
        self.pose_type_group = QButtonGroup()
        self.pose_type_group.addButton(self.ui.pose_absolute)
        self.pose_type_group.addButton(self.ui.pose_relative)
        self.pose_type_group.setExclusive(True)
        self.ui.pose_absolute.setChecked(True)
        self.joint_type_group = QButtonGroup()
        self.joint_type_group.addButton(self.ui.joint_absolute)
        self.joint_type_group.addButton(self.ui.joint_relative)
        self.joint_type_group.setExclusive(True)
        self.ui.joint_absolute.setChecked(True)
        self.external_axis_1_group = QButtonGroup()
        self.external_axis_1_group.addButton(self.ui.external_axis_1_inactive)
        self.external_axis_1_group.addButton(self.ui.external_axis_1_active)
        self.external_axis_1_group.addButton(self.ui.external_axis_1_value)
        self.external_axis_1_group.setExclusive(True)
        self.ui.external_axis_1_inactive.setChecked(True)
        self.external_axis_2_group = QButtonGroup()
        self.external_axis_2_group.addButton(self.ui.external_axis_2_inactive)
        self.external_axis_2_group.addButton(self.ui.external_axis_2_active)
        self.external_axis_2_group.addButton(self.ui.external_axis_2_value)
        self.external_axis_2_group.setExclusive(True)
        self.ui.external_axis_2_inactive.setChecked(True)

    def get_goal_value(self) -> Any:
        if self.parameters['goal_type']:
            if self.parameters['goal_type'] == 'target':
                return self.parameters['target']
            elif self.parameters['goal_type'] == 'pose':
                return self.parameters['pose']
            elif self.parameters['goal_type'] == 'joint_config':
                return self.parameters['joint_config']
            else:
                return None
        else:
            return None

    def send_cancel_move(self, acceleration_factor):
        self.logger.info(
            f'[{self.robot_name}] cancelling Move command at acceleration factor {acceleration_factor}')
        handler = self.appliance_client.send_msg(ASCIICancelMove(
            robot_name=self.robot_name, acceleration_factor=acceleration_factor))
        handler.close_handler()

    def stop_task(self):
        self.send_cancel_move(0.8)
        self.running = False

    def pause_task(self):
        self.send_cancel_move(0.5)
        self.running = False

    def to_string(self):
        string = f'{self.type}: {self.get_goal_value()}'
        if self.parameters['queue']:
            string += ' (Queued)'
        return string

    def convert_global_parameters(self, move_message: ASCIIMove):
        self.logger.debug(f"Converting global variables on: {move_message}")
        if not move_message.pose:
            self.logger.debug(f'Not a pose move. No conversion needed')
            return
        pose: list = move_message.pose
        for idx, value in enumerate(pose):
            if 'globals' in value:
                var_name = value.replace("globals.", "")
                if var_name in self.global_variables.get_variables():
                    var_value = self.global_variables.get_value(var_name)
                    self.logger.info(
                        f'Replacing pose[{idx}]:{pose[idx]} with {var_value}')
                    move_message.pose[idx] = var_value
                else:
                    self.logger.info(
                        f'Global variable [{var_name}] not found')
            else:
                pose[idx] = float(value)
