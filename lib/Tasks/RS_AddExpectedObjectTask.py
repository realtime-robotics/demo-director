#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIRS_AddExpectedObject import ASCIIRS_AddExpectedObject
from lib.appliance_client.appliance_client import ApplianceClient
from ui.rs_add_expected_object_task import Ui_RS_AddExpectedObject
from lib.Tasks.Task import Task

class RS_AddExpectedObjectTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='RS_AddExpectedObject', robot_name=robot_name, parameters=parameters)
        self.timeout = 30

    def load_ui(self):
        self.ui = Ui_RS_AddExpectedObject()
        self.ui.setupUi(self)

    def to_string(self):
        try:
            filter_id = self.parameters['filter_id']
        except:
            filter_id = 'None'
        string = f'{self.type}: {filter_id}'
        return string
    
    def apply_ui_to_task(self, execute_once):
        filter_id = self.ui.filter_id_lineedit.text()
        x = self.ui.centroid_x_lineedit.text()
        y = self.ui.centroid_y_lineedit.text()
        z = self.ui.centroid_z_lineedit.text()
        length = self.ui.length_lineedit.text()
        width = self.ui.width_lineedit.text()
        height = self.ui.height_lineedit.text()
        self.parameters.update({'filter_id':filter_id})
        self.parameters.update({'x':x})
        self.parameters.update({'y':y})
        self.parameters.update({'z':z})
        self.parameters.update({'length':length})
        self.parameters.update({'width':width})
        self.parameters.update({'height':height})
        self.parameters.update({'execute_once':execute_once})

    def apply_task_to_ui(self):
        filter_id = self.parameters['filter_id']
        x = self.parameters['x']
        y = self.parameters['y']
        z = self.parameters['z']
        length = self.parameters['length']
        width = self.parameters['width']
        height = self.parameters['height']
        self.ui.filter_id_lineedit.setText(filter_id)
        self.ui.centroid_x_lineedit.setText(x)
        self.ui.centroid_y_lineedit.setText(y)
        self.ui.centroid_z_lineedit.setText(z)
        self.ui.length_lineedit.setText(length)
        self.ui.width_lineedit.setText(width)
        self.ui.height_lineedit.setText(height)

    def execute(self, **kwargs):
        try:
            filter_id = int(self.parameters['filter_id'])
        except:
            filter_id = self.parameters['filter_id']
        x = float(self.parameters['x'])
        y = float(self.parameters['y'])
        z = float(self.parameters['z'])
        length = float(self.parameters['length'])
        width = float(self.parameters['width'])
        height = float(self.parameters['height'])
        message = ASCIIRS_AddExpectedObject(filter_id=filter_id,centroid=[x,y,z],length=length,width=width,height=height)
        
        self.error = False
        try:
            handler = self.appliance_client.send_msg(message).wait(30.0)
        except Exception as e:
            self.logger.error(f"RS_AddExpectedObject failed for filter ID [{filter_id}] with error: {e}")
            self.error = True
