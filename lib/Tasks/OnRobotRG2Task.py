#!/usr/bin/env python3

from ui.onrobot_rg2_task import Ui_Form
from lib.Utils import initialize_logger
from lib.Tasks.Task import Task
import time, socket
logger = initialize_logger()
try:
    from pymodbus.client.sync import ModbusTcpClient as ModbusClient
except:
    logger.error(f'Failed to import pymodbus, OnRobot task will not succeed. See README for installtion steps.')

class OnRobotRG2(Task):
    connection_data = {}

    def __init__(self,robot_name,parameters, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type= 'OnRobot RG2', robot_name = robot_name, parameters= parameters)
        self.host = None
        self.port = None
        self.connected = False

    def load_ui(self):
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.connect_button_onrobot.clicked.connect(self.manual_connect)
    
    def to_string(self):
        try:
            width = self.parameters['position']
        except:
            width = '0'
        string = f'{self.type}: {width}'
        return string
    
    def manual_connect(self):
        self.apply_ui_to_task(False)
        self.connect()
        if self.connected:
            self.ui.connection_status_label.setText('CONNECTED')

    def connect(self):
        if self.robot_name in self.connection_data.keys():
            self.logger.info(f"{self.type} already connected.")
            self.onrobot_client = self.connection_data[self.robot_name]
            self.connected = True
        else:
            self.logger.info(f"Connecting to the {self.type} for {self.robot_name}")
            self.onrobot_client = ModbusClient(self.parameters['ip_address'], port=self.parameters['port_number'], stopbits=1, bytesize=8, parity='E', baudrate=115200, timeout=1)
            # Connecting to the OnRobot Computer Box via modbus client
            self.onrobot_client.connect()
            # Since connect always succeeds, check if we can get the position of the gripper
            try:
                self.get_status()[0]
            except:
                self.logger.error(f"Robot [{self.robot_name}] failed to connect to the {self.type}.")
                self.connected = False
                self.onrobot_client = None
                return
            self.set_target_force(40)
            # self.set_target_speed(100)
            self.connection_data.update({self.robot_name: self.onrobot_client})
            self.connected = True
            self.logger.info(f"Successfully connected to the {self.type} for {self.robot_name}")
    
    def get_status(self):
        """Reads current device status.
        This status field indicates the status of the gripper and its motion.
        It is composed of 7 flags, described in the table below.

        Bit      Name            Description
        0 (LSB): busy            High (1) when a motion is ongoing,
                                  low (0) when not.
                                  The gripper will only accept new commands
                                  when this flag is low.
        1:       grip detected   High (1) when an internal- or
                                  external grip is detected.
        2:       S1 pushed       High (1) when safety switch 1 is pushed.
        3:       S1 trigged      High (1) when safety circuit 1 is activated.
                                  The gripper will not move
                                  while this flag is high;
                                  can only be reset by power cycling.
        4:       S2 pushed       High (1) when safety switch 2 is pushed.
        5:       S2 trigged      High (1) when safety circuit 2 is activated.
                                  The gripper will not move
                                  while this flag is high;
                                  can only be reset by power cycling.
        6:       safety error    High (1) when on power on any of
                                  the safety switch is pushed.
        10-16:   reserved        Not used.
        """
        # address   : register number
        # count     : number of registers to be read
        # unit      : slave device address
        result = self.onrobot_client.read_holding_registers(
            address=268, count=1, unit=65)
        status = format(result.registers[0], '016b')
        status_list = [0] * 7
        if int(status[-1]):
            self.logger.debug("A motion is ongoing so new commands are not accepted.")
            status_list[0] = 1
        if int(status[-2]):
            self.logger.debug("An internal- or external grip is detected.")
            status_list[1] = 1
        if int(status[-3]):
            self.logger.debug("Safety switch 1 is pushed.")
            status_list[2] = 1
        if int(status[-4]):
            self.logger.debug("Safety circuit 1 is activated so it will not move.")
            status_list[3] = 1
        if int(status[-5]):
            self.logger.debug("Safety switch 2 is pushed.")
            status_list[4] = 1
        if int(status[-6]):
            self.logger.debug("Safety circuit 2 is activated so it will not move.")
            status_list[5] = 1
        if int(status[-7]):
            self.logger.debug("Any of the safety switch is pushed.")
            status_list[6] = 1

        return status_list

    def apply_ui_to_task(self,execute_once):
        ip_address = self.ui.ip_address_lineedit_onrobot.text()
        port_number = self.ui.port_number_spinbox_onrobot.value()
        position = self.ui.position_spinbox_onrobot.value()
        self.parameters = {'ip_address': ip_address, 'port_number': port_number, 'position': position, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        ip_address = self.parameters['ip_address']
        port_number = self.parameters['port_number']
        position = self.parameters['position']
        self.ui.ip_address_lineedit_onrobot.setText(ip_address)
        self.ui.port_number_spinbox_onrobot.setValue(port_number)
        self.ui.position_spinbox_onrobot.setValue(position)

    def set_target_force(self, force_val):
        # Sets the target force for the gripper
        # Accepted values in the range 0 -> 140 N
        force = force_val * 10
        self.logger.info(f'Setting force value for RG2: {force}')
        result = self.onrobot_client.write_register(
            address=0, value=force, unit=65)

    def set_target_speed(self, speed_val):
        # Sets the target speed for the gripper
        # Accepted values in the range 10 -> 100 %
        result = self.onrobot_client.write_register(
            address=2, value=speed_val, unit=65)

    def set_target_width(self, width_val):
        """Writes the target width between
        the finger to be moved to and maintained.
        It must be provided in 1/10th millimeters.
        The valid range is 330 to 750 for the 2FG7.
        Please note that the target width should be provided
        corrected for any fingertip offset,
        as it is measured between the insides of the aluminum fingers.
        """
        width = width_val * 10
        self.logger.info(f'Sending target width to RG2: {width}')

        result = self.onrobot_client.write_register(address=1, value=width, unit=65)
        result = self.onrobot_client.write_register(address=2, value=1, unit=65)

    def execute(self, **kwargs):
        self.error = False
        if not self.connected:
            # self.logger.warn(f"OnRobot 2FG7 must be connected first!")
            self.connect()
            if not self.connected:
                self.logger.error(f"Onrobot RG2 must be connected first!")
                self.error = True
                return
        self.logger.info(f"Robot {self.robot_name} setting {self.type} gripper to: {self.parameters['position']}")
        self.set_target_width(self.parameters['position'])
        result = self.onrobot_client.write_registers(
            address=3, values=1, unit=65)
        while True:
            time.sleep(0.2)
            if not self.get_status()[0]:
                break