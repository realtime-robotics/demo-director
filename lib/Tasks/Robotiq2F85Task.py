#!/usr/bin/env python3

from ui.robotiq_2f85_task import Ui_Form
from lib.Tasks.Task import Task
import time, socket

class Robotiq2F85(Task):
    connection_data = {}
    connected = False

    def __init__(self,robot_name,parameters, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type= 'Robotiq 2F85', robot_name = robot_name, parameters= parameters)
        self.robotiq_socket = None
        self.has_ui = False
        self.connected = False

    def load_ui(self):
        self.ui = Ui_Form()
        self.ui.setupUi(self)
        self.ui.connect_button.clicked.connect(self.connect)
        self.has_ui = True

    def to_string(self):
        try:
            width = self.parameters['position']
        except:
            width = '0'
        string = f'{self.type}: {width}'
        return string

    def connect(self):
        if self.robot_name in self.connection_data.keys():
            self.robotiq_socket = self.connection_data[self.robot_name]
            self.connected = True
        else:
            try:
                # Setting up socket connection to UR on the Robotiq port
                self.robotiq_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
                self.robotiq_socket.settimeout(2.0)
                # Open the socket
                ip_address = self.parameters['ip_address']
                port = self.parameters['port_number']
                self.logger.info(f"Connecting to the Robotiq 2F85 for {self.robot_name} at {ip_address}:{port}")
                self.robotiq_socket.connect((ip_address,port))
                # Activate the gripper
                self.robotiq_socket.sendall(b'SET ACT 1\n')
                self.robotiq_socket.sendall(b'SET MOD 1\n')
                time.sleep(3.0)
                # Store the socket for all tasks to use
                self.connection_data.update({self.robot_name: self.robotiq_socket})
                self.connected = True
                if self.has_ui:
                    self.ui.connection_status_label.setText('CONNECTED')
            except Exception as e:
                self.logger.error(f"Robot [{self.robot_name}] failed to connect to the UR Robotiq 2F85 with error: {e}")
                self.connected = False
                if self.has_ui:
                    self.ui.connection_status_label.setText('DISCONNECTED')
        
    def apply_ui_to_task(self,execute_once):
        ip_address = self.ui.ip_address_lineedit.text()
        port_number = self.ui.port_number_spinbox.value()
        position = self.ui.position_spinbox.value()
        self.parameters = {'ip_address': ip_address, 'port_number': port_number, 'position': position, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        ip_address = self.parameters['ip_address']
        port_number = self.parameters['port_number']
        position = self.parameters['position']
        self.ui.ip_address_lineedit.setText(ip_address)
        self.ui.port_number_spinbox.setValue(port_number)
        self.ui.position_spinbox.setValue(position)

    def width_to_cmd(self,width):
        # Convert the mm distance between finger tips to the 0-255 integer sent to the controller
        # max finger tip distance is 85mm
        # integer 0 results in max opening (85mm)
        # integer 255 results in minimum opening (0mm)

        # y = mx + b
        # 0 = m(85) + b
        # 255 = m(0) + b
        # b = 255
        # 0 = m(85) + 255
        # -255/85 = m
        value = -(255/85 * width) + 255
        return int(value)
    
    def execute(self, **kwargs):
        self.error = False
        self.connect()
        if not self.connected:
            self.logger.error(f"Robotiq 2F85 must be connected first!")
            self.error = True
            return
        
        self.logger.info(f"Robot {self.robot_name} setting Robotiq 2F85 gripper to: {self.parameters['position']}")
        width = self.width_to_cmd(self.parameters['position'])
        self.robotiq_socket.sendall(b'SET POS %d\n'%(width))
        time.sleep(1.6)