#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIRemoveBoxes import ASCIIRemoveBoxes
from lib.appliance_client.appliance_client import ApplianceClient
from ui.dsm_remove_boxes_task import Ui_DSMRemoveBoxes
from lib.Tasks.Task import Task
import time

class DSMRemoveBoxesTask(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='RemoveBoxes',robot_name=robot_name, parameters=parameters, uuid=self.uuid)

    def load_ui(self):
        self.ui = Ui_DSMRemoveBoxes()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once): 
        names = self.ui.names_lineedit.text()
        self.parameters.update({'names':names})
        self.parameters.update({'execute_once':execute_once})

    def apply_task_to_ui(self):
        names = self.parameters['names']
        self.ui.names_lineedit.setText(names)

    def execute(self, **kwargs):
        if self.parameters['names'] == '':
            names = []
        else:
            names = self.parameters['names'].split(',')
            
        self.error = False
        message = ASCIIRemoveBoxes(box_names=names)
        try:
            handler = self.appliance_client.send_msg(message).wait(30.0)
        except Exception as e:
            self.logger.error(f"[{self.robot_name}] {message.get_topic()} ERROR: {e}")
            self.error = True
        else:
            self.logger.info(f'Removed DSM boxes: {names}')
