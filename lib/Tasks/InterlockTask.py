#!/usr/bin/env python3

from lib.Tasks.Task import Task
import time
import ui.interlock_task
from lib.InterlockManager import InterlockManager


class InterlockTask(Task):
    def __init__(self, robot_name, interlock_manager: InterlockManager, parameters={}, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type='Interlock', parameters=parameters, uuid=self.uuid)
        self.interlock_manager = interlock_manager
        self.parameters = parameters
        # correct for uncapitalized first letter in value if loaded from json
        if 'value' in self.parameters:
            self.parameters['value'] = self.parameters['value'].capitalize()
        self.robot_name = robot_name
        self.running = False
        self.stop = False
        self.pause = False

    def load_ui(self):
        self.ui = ui.interlock_task.Ui_InterlockTask()
        self.ui.setupUi(self)

        self.ui.add_interlock_id_button.clicked.connect(self.add_interlock)
        self.refresh_interlock_id_combobox()

    def refresh_interlock_id_combobox(self):
        self.ui.interlock_id_combobox.clear()
        self.ui.interlock_id_combobox.addItems(
            list(self.interlock_manager.interlocks.keys()))

    def add_interlock(self):
        id = self.ui.interlock_id_lineedit.text()
        if type(id) == None:
            return
        self.interlock_manager.interlocks[id] = False
        self.refresh_interlock_id_combobox()
        self.logger.info(f'Added Interlock: {id}')

    def apply_ui_to_task(self, execute_once):
        action = self.ui.interlock_action_combobox.currentText()
        id = self.ui.interlock_id_combobox.currentText()
        value = self.ui.interlock_value_combobox.currentText()
        self.parameters = {'action': action, 'id': id,
                           'value': value, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        action = self.parameters['action']
        id = self.parameters['id']
        value = self.parameters['value']
        self.ui.interlock_action_combobox.setCurrentText(action)
        self.ui.interlock_id_combobox.setCurrentText(id)
        self.ui.interlock_value_combobox.setCurrentText(value)

    def execute(self, **kwargs):
        self.running = True
        if self.parameters['action'] == 'Set':
            self.logger.info(
                f"Robot {self.robot_name} setting interlock {self.parameters['id']} to {self.parameters['value']}")
            self.interlock_manager.set_interlock(
                self.parameters['id'], self.parameters['value'])
        elif self.parameters['action'] == 'Wait':
            self.logger.info(
                f"Robot {self.robot_name} waiting for interlock {self.parameters['id']} to be {self.parameters['value']}")
            tic = time.time()
            while not (self.pause or self.stop):
                # The sleep exists otherwise the robot will seemingly skip the interlock task.
                # Comment out the sleep and then start and pause a task plan where one robot waits on an
                # interlock set by another
                if self.interlock_manager.interlocks[self.parameters['id']] == self.parameters['value']:
                    break
                time.sleep(0.05)
            toc = time.time()
            self.logger.info(
                f"Robot {self.robot_name} waited {toc-tic} seconds for interlock {self.parameters['id']} to be {self.parameters['value']}")
        self.stop = False
        self.pause = False
        self.running = False

    def stop_task(self):
        self.stop = True

    def pause_task(self):
        self.pause = True

    def to_string(self) -> str:
        return f'{self.type}: {self.parameters["action"]} [{self.parameters["id"]}] = [{self.parameters["value"]}]'
