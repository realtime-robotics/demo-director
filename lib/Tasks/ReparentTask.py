#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIReparent import ASCIIReparent
from lib.appliance_client.appliance_client import ApplianceClient
from ui.reparent_task import Ui_Reparent
from lib.Tasks.Task import Task
import time

class ReparentTask(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='Reparent',robot_name=robot_name, parameters=parameters, uuid=self.uuid)

    def load_ui(self):
        self.ui = Ui_Reparent()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once):
        name = self.ui.name_lineedit.text()
        parent_frame = self.ui.parent_frame_lineedit.text()
        self.parameters.update({'name':name})
        self.parameters.update({'parent_frame':parent_frame})
        self.parameters.update({'execute_once':execute_once})

    def apply_task_to_ui(self):
        name = self.parameters['name']
        parent_frame = self.parameters['parent_frame']

        self.ui.name_lineedit.setText(name)
        self.ui.parent_frame_lineedit.setText(parent_frame)

    def execute(self, **kwargs):
        self.error = False

        name = self.parameters['name']
        parent_frame = self.parameters['parent_frame']
        if parent_frame == '':
            parent_frame = None
        message = ASCIIReparent(box_name=name, parent_frame=parent_frame)
        try:
            handler = self.appliance_client.send_msg(message).wait(30.0)
        except Exception as e:
            self.error = True
            self.logger.error(f"[{self.robot_name}] {message.get_topic()} ERROR: {e}")
        else:
            self.logger.info(f'Reparented DSM box [{name}] successfully.')

    def to_string(self):
        string = f"{self.type}: {self.parameters['name']}"
        return string
