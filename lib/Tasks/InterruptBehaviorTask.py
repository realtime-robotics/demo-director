#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.interrupt_behavior_task
import time
from lib.appliance_client.messages.ASCIISetInterruptBehavior import ASCIISetInterruptBehavior
from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException
from lib.appliance_client.appliance_client import ApplianceClient


class InterruptBehaviorTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='SetInterruptBehavior',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.timeout = 5

    def load_ui(self):
        self.ui = ui.interrupt_behavior_task.Ui_InterruptBehaviorTask()
        self.ui.setupUi(self)

        self.ui.infinite_timeout_checkbox.stateChanged.connect(
            self.set_enabled_states)
        self.ui.infinite_replans_checkbox.stateChanged.connect(
            self.set_enabled_states)
        self.set_enabled_states()

    def set_enabled_states(self):
        if self.ui.infinite_timeout_checkbox.isChecked():
            self.ui.timeout_spinbox.setEnabled(False)
        else:
            self.ui.timeout_spinbox.setEnabled(True)

        if self.ui.infinite_replans_checkbox.isChecked():
            self.ui.replans_spinbox.setEnabled(False)
        else:
            self.ui.replans_spinbox.setEnabled(True)

    def apply_ui_to_task(self, execute_once):
        if self.ui.infinite_timeout_checkbox.isChecked():
            timeout = -1
        else:
            timeout = self.ui.timeout_spinbox.value()

        if self.ui.infinite_replans_checkbox.isChecked():
            replan_attempts = -1
        else:
            replan_attempts = self.ui.replans_spinbox.value()

        self.parameters = {
            'timeout': timeout, 'replan_attempts': replan_attempts, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        timeout = self.parameters['timeout']
        replan_attempts = self.parameters['replan_attempts']
        if timeout == -1:
            self.ui.timeout_spinbox.setValue(0.05)
            self.ui.infinite_timeout_checkbox.setChecked(True)
        else:
            self.ui.timeout_spinbox.setValue(timeout)
            self.ui.infinite_timeout_checkbox.setChecked(False)

        if replan_attempts == -1:
            self.ui.replans_spinbox.setValue(0)
            self.ui.infinite_replans_checkbox.setChecked(True)
        else:
            self.ui.replans_spinbox.setValue(replan_attempts)
            self.ui.infinite_replans_checkbox.setChecked(False)

    def execute(self, **kwargs):
        # send the ascii msg
        tic = time.time()
        handler = self.appliance_client.send_msg(
            ASCIISetInterruptBehavior(robot_name=self.robot_name,
                                      timeout=self.parameters['timeout'],
                                      max_replans=self.parameters['replan_attempts']
                                      ))
        
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()
