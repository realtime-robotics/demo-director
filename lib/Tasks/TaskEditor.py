#!/usr/bin/env python3

from PyQt5.QtWidgets import QDialog, QDialogButtonBox, QCheckBox, QVBoxLayout, QHBoxLayout, QLabel, QSpacerItem, QSizePolicy, QLineEdit
from PyQt5.QtCore import Qt
from lib.Utils import initialize_logger


class TaskEditor(QDialog):
    '''
        The Task editor works by receiving a task object, loading the UI elements,
        writing the parameter values to the UI elements, and then opening in a dialog.

        It is expected that the task object received is a copy of the task being edited, such that if
        the changes are rejected, the original task remains unchanged. If the changes are accepted, then
        the caller must overwrite the original parameters dictionary with the parameters from the editor.
    '''

    def __init__(self, task, parent=None):
        super().__init__(parent)
        self.logger = initialize_logger()
        self.setWindowTitle("Task Editor")
        self.setStyleSheet("font: 12pt Open Sans;")

        QBtn = QDialogButtonBox.Ok | QDialogButtonBox.Cancel
        self.buttonBox = QDialogButtonBox(QBtn)
        self.buttonBox.accepted.connect(self.accept)
        self.buttonBox.rejected.connect(self.reject)
        btns = self.buttonBox.buttons()
        btns[0].setDefault(True)
        btns[1].setDefault(False)

        task.load_ui()
        task.apply_task_to_ui()

        self.execute_once_checkbox = QCheckBox('Execute once in task list')
        if task.parameters['execute_once']:
            self.execute_once_checkbox.setChecked(True)

        uuid_label = QLabel('UUID:')
        uuid_label.setStyleSheet("font: 9pt")
        uuid_value = QLabel(f'{task.uuid}')
        uuid_value.setStyleSheet("font: 9pt")
        uuid_value.setTextInteractionFlags(Qt.TextSelectableByMouse)
        uuid_layout = QHBoxLayout()
        uuid_layout.addWidget(uuid_label)
        uuid_layout.addWidget(uuid_value)
        uuid_layout.addSpacerItem(QSpacerItem(
            20, 40, QSizePolicy.Expanding, QSizePolicy.Minimum))

        self.layout = QVBoxLayout()
        self.layout.addLayout(uuid_layout)
        self.layout.addWidget(task)
        self.layout.addWidget(self.execute_once_checkbox)
        self.layout.addWidget(self.buttonBox)
        self.setLayout(self.layout)

        self.logger.debug(
            f'Task editor initialized for task {task.type} with uuid {task.uuid}')
