#!/usr/bin/env python3

from lib.Utils import initialize_logger
from lib.Tasks.Task import Task
import ui.digitalio_task

logger = initialize_logger()
try:
    from pymodbus.client.sync import ModbusTcpClient as ModbusClient
except:
    logger.error(f'Failed to import pymodbus, DigitalIO task will not succeed. See README for installtion steps.')

class DigitalIO(Task):

    def __init__(self, robot_name, parameters={}, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type='Digital I/O', parameters=parameters, uuid=self.uuid)
        self.parameters = parameters
        self.robot_name = robot_name
        # The E1213 has 8 DI lines, 4 DO lines and 4 DIO lines. Depending on the configuration of the 4 DIO lines, this allows up to 12 DI lines or 8 DO lines
        # self.di_lines = ['DI-00', 'DI-01', 'DI-02', 'DI-03', 'DI-04', 'DI-05', 'DI-06', 'DI-07', 'DI-08', 'DI-09', 'DI-10', 'DI-11']
        # self.do_lines = ['DO-00', 'DO-01', 'DO-02', 'DO-03', 'DO-04', 'DO-05', 'DO-06', 'DO-07']
        self.di_lines = ['DI-00', 'DI-01', 'DI-02',
                         'DI-03', 'DI-04', 'DI-05', 'DI-06', 'DI-07']
        self.do_lines = ['DO-00', 'DO-01', 'DO-02', 'DO-03']
        self.connection_data = {}
        # Set the host IP address for the DIO module here:
        self.host = None
        # Modbus TCP port is 502 by default
        self.port = 502
        self.connected = False

    def load_ui(self):
        self.ui = ui.digitalio_task.Ui_DigitalIOTask()
        self.ui.setupUi(self)
        self.ui.digitalio_action_combobox.currentIndexChanged.connect(
            self.refresh_digitalio_id_combobox)
        self.refresh_digitalio_id_combobox()

    def connect(self):
        dio_hw_id = 'dio_01'
        if dio_hw_id in self.connection_data.keys():
            self.logger.info(f"{self.type} already connected.")
            self.modbus_client = self.connection_data[dio_hw_id]
            self.connected = True
        else:
            self.logger.info(f"Connecting to the {self.type} for {dio_hw_id}")
            self.modbus_client = ModbusClient(
                '192.168.0.61', port=502, stopbits=1, bytesize=8, parity='E', baudrate=115200, timeout=1)
            # Connecting to the Moxa IO module via modbus client
            self.modbus_client.connect()
            self.connection_data.update({dio_hw_id: self.modbus_client})
            self.connected = True
            self.logger.info(
                f"Successfully connected to the {self.type} for {dio_hw_id}")

    def refresh_digitalio_id_combobox(self):
        self.ui.digitalio_id_combobox.clear()
        action = self.ui.digitalio_action_combobox.currentText()
        id_list = []
        if action == 'Set':
            id_list = self.do_lines
        else:
            id_list = self.di_lines
        self.ui.digitalio_id_combobox.addItems(id_list)

    def apply_ui_to_task(self, execute_once):
        action = self.ui.digitalio_action_combobox.currentText()
        id = self.ui.digitalio_id_combobox.currentIndex()
        value = self.ui.digitalio_value_combobox.currentText()
        self.parameters = {'action': action, 'id': id,
                           'value': value, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        action = self.parameters['action']
        id = self.parameters['id']
        value = self.parameters['value']
        self.ui.digitalio_action_combobox.setCurrentText(action)
        self.ui.digitalio_id_combobox.setCurrentIndex(id)
        self.ui.digitalio_value_combobox.setCurrentText(value)

    def execute(self, **kwargs):
        action = self.parameters['action']
        id = self.parameters['id']
        value = self.parameters['value']

        output_setting = 0
        input_setting = False
        if value == 'On':
            output_setting = 1
            input_setting = True

        if not self.connected:
            self.connect()
            if not self.connected:
                self.logger.error(f"Digital I/O hardware module did not connect")
                self.error = True
                return
        if action == 'Set':
            self.logger.info(
                f"Robot {self.robot_name} setting DO-{id} to {output_setting}")
            #   Set digital output line 'id' to 'value' using modbus function block 5 "write_coil"

            result = self.modbus_client.write_coil(
                address=id, value=output_setting)

        elif action == 'Wait':
            self.logger.info(
                f"Robot {self.robot_name} waiting for DI-{id} to be {value}")
            # Read digital input line 'id' using modbus function block 2 "read_discrete_inputs"
            di_port_value = self.modbus_client.read_discrete_inputs(
                address=0, count=8)
            di_line_value = di_port_value.bits[id]
            if di_line_value == input_setting:
                self.logger.info(
                    f"Robot {self.robot_name} confirmed DI-{id} to be {value}")
            else:
                self.logger.warn(
                    f"Robot {self.robot_name} read signal DI-{id} as invalid: {value}")

        self.modbus_client.close()

    def to_string(self) -> str:
        return f"{self.type}: {self.parameters['action']} [{self.parameters['id']}] = [{self.parameters['value']}]"
