#!/usr/bin/env python3

from lib.Tasks.Task import Task
import time
import ui.clear_faults_task
from lib.appliance_client.messages.ASCIIClearFaults import ASCIIClearFaults
from lib.appliance_client.messages.ASCIIEnterOperationMode import ASCIIEnterOperationMode
from lib.appliance_client.appliance_client import ApplianceClient
from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException


class ClearFaultsTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='ClearFaults',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.timeout = 30

    def load_ui(self):
        self.ui = ui.clear_faults_task.Ui_ClearFaultsTask()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once):
        self.parameters = {'execute_once': execute_once}

    def apply_task_to_ui(self):
        print('apply')
        #acceleration_factor = self.parameters['acceleration_factor']
        #self.ui.accel_factor_spinbox.setValue(acceleration_factor)

    def execute(self, **kwargs):
     # send the ascii msg
        tic = time.time()
        handler = self.appliance_client.send_msg(
            ASCIIClearFaults(
                            ))
        
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()

        handler = self.appliance_client.send_msg(
            ASCIIEnterOperationMode(
                            ))
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()