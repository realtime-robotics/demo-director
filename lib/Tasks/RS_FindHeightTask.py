#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIRS_FindHeight import ASCIIRS_FindHeight
from lib.appliance_client.appliance_client import ApplianceClient
from lib.Utils import initialize_height_check_logger
from ui.rs_find_height_task import Ui_RS_FindHeight
from lib.GlobalVariables import GlobalVariables
from lib.Tasks.Task import Task
import time

class RS_FindHeightTask(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, global_variables: GlobalVariables, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='HeightCheck',robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.global_variables = global_variables
        self.height_check_logger = initialize_height_check_logger()

    def load_ui(self):
        self.ui = Ui_RS_FindHeight()
        self.ui.setupUi(self)
            
    def to_string(self):
        string = f"{self.type}: {self.parameters['name']}"
        return string

    def apply_ui_to_task(self, execute_once):
        filter_id = self.ui.id_lineedit.text()
        x = self.ui.x_lineedit.text()
        y = self.ui.y_lineedit.text()
        length = self.ui.length_lineedit.text()
        width = self.ui.width_lineedit.text()
        name = self.ui.name_lineedit.text()
        expected_height = self.ui.expected_height_lineedit.text()
        self.parameters.update({'filter_id':filter_id})
        self.parameters.update({'x':x})
        self.parameters.update({'y':y})
        self.parameters.update({'length':length})
        self.parameters.update({'width':width})
        self.parameters.update({'name':name})
        self.parameters.update({'expected_height': expected_height})
        self.parameters.update({'execute_once':execute_once})

    def apply_task_to_ui(self):
        filter_id = self.parameters['filter_id']
        x = self.parameters['x']
        y = self.parameters['y']
        length = self.parameters['length']
        width = self.parameters['width']
        name = self.parameters['name']
        expected_height = self.parameters['expected_height']
        self.ui.id_lineedit.setText(filter_id)
        self.ui.x_lineedit.setText(x)
        self.ui.y_lineedit.setText(y)
        self.ui.length_lineedit.setText(length)
        self.ui.width_lineedit.setText(width)
        self.ui.name_lineedit.setText(name)
        self.ui.expected_height_lineedit.setText(expected_height)

    def execute(self, **kwargs):
        try:
            filter_id = int(self.parameters['filter_id'])
        except:
            filter_id = self.parameters['filter_id']
        x = float(self.parameters['x'])
        y = float(self.parameters['y'])
        length = float(self.parameters['length'])
        width = float(self.parameters['width'])
        message = ASCIIRS_FindHeight(filter_id=filter_id,centroid=[x,y],length=length,width=width)

        name = self.parameters['name']
        expected_height = self.parameters['expected_height']
        self.logger.info(f"Getting height for: {name}")
        
        self.error = False
        try:
            tic = time.time()
            handler = self.appliance_client.send_msg(message).wait(30.0)
        except Exception as e:
            toc = time.time()
            self.logger.error(f"[{self.robot_name}] {message.get_topic()} ERROR: {e}")
            self.height_check_logger.info(f'{name},{toc-tic:0.4f},ERROR,{e}')
            self.error = True
        else:
            toc = time.time()
            # store the value returned by height check in a global variable
            height = handler.received[-1]['data']['height']
            self.global_variables.add_variable(f'{self.robot_name}_find_height',str(height))
            self.logger.info(f'[{self.robot_name}] RS_FindHeight returned: {height}')
            self.height_check_logger.info(f'{name},{toc-tic:0.4f},{height},{expected_height}')
