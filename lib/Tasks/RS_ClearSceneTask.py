#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIRS_ClearScene import ASCIIRS_ClearScene
from lib.appliance_client.appliance_client import ApplianceClient
from ui.rs_clear_scene_task import Ui_RS_ClearScene
from lib.Tasks.Task import Task

class RS_ClearSceneTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='RS_ClearScene', robot_name=robot_name, parameters=parameters)
        self.timeout = 30

    def load_ui(self):
        self.ui = Ui_RS_ClearScene()
        self.ui.setupUi(self)

    def to_string(self):
        try:
            filter_id = self.parameters['filter_id']
        except:
            filter_id = 'None'
        string = f'{self.type}: {filter_id}'
        return string

    def apply_ui_to_task(self, execute_once):
        filter_id = self.ui.filter_id_lineedit.text()
        self.parameters = {'filter_id': filter_id, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        filter_id = self.parameters['filter_id']
        self.ui.filter_id_lineedit.setText(filter_id)

    def execute(self, **kwargs):
        # send the ascii msg
        try:
            filter_id = int(self.parameters['filter_id'])
        except:
            filter_id = self.parameters['filter_id']

        self.logger.info(f'Calling RS_ClearScene for filter ID: {filter_id}')
        handler = self.appliance_client.send_msg(ASCIIRS_ClearScene(filter_id=filter_id))

        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            self.logger.error(f"RS_ClearScene failed for filter ID [{filter_id}] with error: {e}")
            self.error = True
        
        handler.close_handler()
