#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIUserLog import ASCIIUserLog
from lib.appliance_client.appliance_client import ApplianceClient
from ui.user_log_task import Ui_UserLog
from lib.Tasks.Task import Task
import time

class UserLogTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='UserLog',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.timeout = 30

    def load_ui(self):
        self.ui = Ui_UserLog()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once):
        message = self.ui.message_line_edit.text()
        self.parameters = {'message': message,
                           'execute_once': execute_once}

    def apply_task_to_ui(self):
        message = self.parameters['message']
        self.ui.message_line_edit.setText(message)

    def execute(self, **kwargs):
        # send the ascii msg
        tic = time.time()
        msg = ASCIIUserLog(self.parameters['message'])
        handler = self.appliance_client.send_msg(msg)
        
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            self.logger.error(f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()
