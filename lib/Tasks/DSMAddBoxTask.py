#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIAddBox import ASCIIAddBox
from lib.appliance_client.appliance_client import ApplianceClient
from ui.dsm_add_box_task import Ui_DSMAddBox
from lib.Tasks.Task import Task
import time

class DSMAddBoxTask(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='AddBox',robot_name=robot_name, parameters=parameters, uuid=self.uuid)

    def load_ui(self):
        self.ui = Ui_DSMAddBox()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once):
        name = self.ui.name_lineedit.text()
        x = self.ui.x_lineedit.text()
        y = self.ui.y_lineedit.text()
        z = self.ui.z_lineedit.text()
        rx = self.ui.rx_lineedit.text()
        ry = self.ui.ry_lineedit.text()
        rz = self.ui.rz_lineedit.text()
        length = self.ui.length_lineedit.text()
        width = self.ui.width_lineedit.text()
        height = self.ui.height_lineedit.text()
        parent_frame = self.ui.parent_frame_lineedit.text()
        self.parameters.update({'name':name})
        self.parameters.update({'x':x})
        self.parameters.update({'y':y})
        self.parameters.update({'z':z})
        self.parameters.update({'rx':rx})
        self.parameters.update({'ry':ry})
        self.parameters.update({'rz':rz})
        self.parameters.update({'length':length})
        self.parameters.update({'width':width})
        self.parameters.update({'height':height})
        self.parameters.update({'parent_frame':parent_frame})
        self.parameters.update({'execute_once':execute_once})

    def apply_task_to_ui(self):
        name = self.parameters['name']
        x = self.parameters['x']
        y= self.parameters['y']
        z= self.parameters['z']
        rx= self.parameters['rx']
        ry= self.parameters['ry']
        rz= self.parameters['rz']
        length = self.parameters['length']
        width = self.parameters['width']
        height = self.parameters['height']
        parent_frame = self.parameters['parent_frame']

        self.ui.name_lineedit.setText(name)
        self.ui.x_lineedit.setText(x)
        self.ui.y_lineedit.setText(y)
        self.ui.z_lineedit.setText(z)
        self.ui.rx_lineedit.setText(rx)
        self.ui.ry_lineedit.setText(ry)
        self.ui.rz_lineedit.setText(rz)
        self.ui.length_lineedit.setText(length)
        self.ui.width_lineedit.setText(width)
        self.ui.height_lineedit.setText(height)
        self.ui.parent_frame_lineedit.setText(parent_frame)

    def execute(self, **kwargs):
        self.error = False

        name = self.parameters['name']
        x = self.parameters['x']
        y= self.parameters['y']
        z= self.parameters['z']
        rx= self.parameters['rx']
        ry= self.parameters['ry']
        rz= self.parameters['rz']
        length = self.parameters['length']
        width = self.parameters['width']
        height = self.parameters['height']
        parent_frame = self.parameters['parent_frame']
        if parent_frame == '':
            parent_frame = None
        message = ASCIIAddBox(box_name=name,size=[length,width,height],offset=[x,y,z,rx,ry,rz],parent_frame=parent_frame)
        try:
            handler = self.appliance_client.send_msg(message).wait(30.0)
        except Exception as e:
            self.error = True
            self.logger.error(f"[{self.robot_name}] {message.get_topic()} ERROR: {e}")
        else:
            self.logger.info(f'Added DSM box [{name}] successfully.')

    def to_string(self):
        string = f"{self.type}: {self.parameters['name']}"
        return string
