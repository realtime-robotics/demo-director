#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.wait_task
import random
import time

class WaitTask(Task):
    def __init__(self, robot_name, parameters, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type='Wait', robot_name=robot_name,
                         parameters=parameters, uuid=self.uuid)
        
        self.stop = False
        self.noise_error = False

    def load_ui(self):
        self.ui = ui.wait_task.Ui_WaitTask()
        self.ui.setupUi(self)
        self.ui.min_noise_spinbox.valueChanged.connect(self.validate_noise_values)
        self.ui.max_noise_spinbox.valueChanged.connect(self.validate_noise_values)

    def validate_noise_values(self):
        error_string = ''
        min_noise = float(self.ui.min_noise_spinbox.value())
        max_noise = float(self.ui.max_noise_spinbox.value())
        self.noise_error = True
        if min_noise > max_noise:
            error_string = f'Min noise cannot be greater than Max noise!'
        elif max_noise < min_noise:
            error_string = f'Max noise cannot be less than Min noise!'
        elif min_noise == max_noise and max_noise > 0.0:
            error_string = f'Noise will not be random since Min and Max are equal!'
        else:
            self.noise_error = False
        self.ui.noise_error_label.setText(error_string)

    def apply_ui_to_task(self, execute_once):
        time = self.ui.wait_time_spinbox.value()
        loop_add_time = self.ui.loop_add_time.value()
        min_noise = float(self.ui.min_noise_spinbox.value())
        max_noise = float(self.ui.max_noise_spinbox.value())
        self.parameters = {'time': time, 'loop_add_time': loop_add_time}
        self.parameters.update({'min_noise':min_noise})
        self.parameters.update({'max_noise':max_noise})
        self.parameters.update({'execute_once': execute_once})

    def apply_task_to_ui(self):
        time = self.parameters['time']
        loop_add_time = self.parameters['loop_add_time']
        min_noise = self.parameters['min_noise']
        max_noise = self.parameters['max_noise']
        self.ui.wait_time_spinbox.setValue(time)
        self.ui.loop_add_time.setValue(loop_add_time)
        self.ui.min_noise_spinbox.setValue(min_noise)
        self.ui.max_noise_spinbox.setValue(max_noise)

    def execute(self, **kwargs):
        self.stop = False
        self.error = False

        noise = 0.0
        min_noise = self.parameters['min_noise']
        max_noise = self.parameters['max_noise']
        if max_noise > 0.0 and min_noise > 0.0:
            noise = random.uniform(min_noise,max_noise)
            self.logger.info(f'Adding noise to wait task: {noise:.2f}s')

        wait_time = self.parameters['time'] + (kwargs.get('loop_count') * self.parameters['loop_add_time'])
        wait_time += noise
        self.logger.info(f"Robot [{self.robot_name}] waiting for [{wait_time:.2f}] seconds")
        
        prev_time = time.time()
        while time.time() - prev_time < wait_time:
            time.sleep(0.05)
            if self.stop:
                self.error = True
                break

    def stop_task(self):
        self.stop = True

    def to_string(self):
        add_per_loop_str: str = ""
        if self.parameters["loop_add_time"] > 0:
            add_per_loop_str = f' + {self.parameters["loop_add_time"]}s/loop'
        
        noise_str: str = ""
        min_noise = self.parameters['min_noise']
        max_noise = self.parameters['max_noise']
        if max_noise > 0.0 and min_noise > 0.0:
            noise_str = f' + rand[{min_noise},{max_noise}]s'

        return f'{self.type} {self.parameters["time"]}s{add_per_loop_str}{noise_str}'