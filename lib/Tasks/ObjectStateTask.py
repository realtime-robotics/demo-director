#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.object_state_task
import time
from lib.appliance_client.messages.ASCIISetObjectState import ASCIISetObjectState
from lib.appliance_client.messages.ASCIIGetStatefulObjects import ASCIIGetStatefulObjects
from lib.appliance_client.messages.ASCIIGetObjectStates import ASCIIGetObjectStates
from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException, ApplianceClientException
from lib.appliance_client.appliance_client import ApplianceClient


class ObjectStateTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='SetObjectState',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.timeout = 5
        self.object_states_dict = self.get_object_states_dict()

    def load_ui(self):
        self.ui = ui.object_state_task.Ui_ObjectStateTask()
        self.ui.setupUi(self)

        self.ui.object_combobox.addItems(list(self.object_states_dict.keys()))
        object_name = self.ui.object_combobox.currentText()
        if self.object_states_dict.keys().__len__() > 0:
            states = self.object_states_dict[object_name]
            self.ui.state_combobox.addItems(states)
            self.refresh_objects()
            self.refresh_object_states()
        self.ui.object_combobox.activated.connect(self.refresh_object_states)

    def refresh_objects(self):
        object_names = list(self.object_states_dict.keys())
        self.ui.object_combobox.clear()
        self.ui.object_combobox.addItems(object_names)

    def refresh_object_states(self):
        object_name = self.ui.object_combobox.currentText()
        states = self.object_states_dict[object_name]
        self.ui.state_combobox.clear()
        self.ui.state_combobox.addItems(states)

    def apply_ui_to_task(self, execute_once):
        object_name = self.ui.object_combobox.currentText()
        state_name = self.ui.state_combobox.currentText()
        self.parameters = {'object': object_name,
                           'state': state_name, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        object_name = self.parameters['object']
        state_name = self.parameters['state']
        self.ui.object_combobox.setCurrentText(object_name)
        self.ui.state_combobox.setCurrentText(state_name)

    def execute(self, **kwargs):
        # send the ascii msg
        tic = time.time()
        handler = self.appliance_client.send_msg(
            ASCIISetObjectState(object_name=self.parameters['object'],
                                state_name=self.parameters['state']
                                ))
        
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()

    def get_object_states_dict(self) -> dict:
        object_states_dict = {}
        if not self.appliance_client._router._running:
            self.appliance_client.start()
        try:
            handler = self.appliance_client.send_msg(
                ASCIIGetStatefulObjects()).wait(self.timeout)
        except ApplianceClientException as e:
            self.logger.error(
                f"[{self.robot_name}] Appliance Client Exception ERROR: {e}")
            object_states_dict = {}
            return object_states_dict
        except MessageException as e:
            self.logger.error(
                f"[{self.robot_name}] Message Exception ERROR: {e}")
            object_states_dict = {}
            return object_states_dict
        object_states_list = list(
            dict(handler.received[-1])['data']['objects'])
        for object in object_states_list:
            handler = self.appliance_client.send_msg(
                ASCIIGetObjectStates(str(object)))
            try:
                handler.wait(self.timeout)
            except Exception as e:
                self.logger.error(
                    f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e}")
                object_states_dict = {}
                return object_states_dict
            object_states_dict[object] = list(
                dict(handler.received[-1])['data']['states'])
        handler.close_handler()
        return object_states_dict
