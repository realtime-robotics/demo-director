#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIGetRapidSenseStatus import ASCIIGetRapidSenseStatus
from lib.appliance_client.appliance_client import ApplianceClient
from ui.rs_get_status_task import Ui_rs_get_status
from lib.Tasks.Task import Task
import datetime, time

class GetRapidSenseStatusTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='GetRapidSenseStatus', robot_name=robot_name, parameters=parameters)
        self.timeout = 30

    def load_ui(self):
        self.ui = Ui_rs_get_status()
        self.ui.setupUi(self)

    def apply_ui_to_task(self, execute_once):
        time_stamp = self.ui.time_stamp_label.text()
        status = self.ui.status_label.text()
        self.parameters = {'time_stamp': time_stamp, 'status': status, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        time_stamp = self.parameters['time_stamp']
        status = self.parameters['status']
        self.ui.time_stamp_label.setText(time_stamp)
        self.ui.status_label.setText(status)

    def execute(self, **kwargs):
        # send the ascii msg
        self.logger.info(f'Getting RapidSense status.')
        send_time = time.time()
        handler = self.appliance_client.send_msg(ASCIIGetRapidSenseStatus())

        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            self.logger.error(f"GetRapidSenseStatus failed with error: {e}")
            self.error = True
        else:
            self.logger.info(f"RapidSense Status: {handler.received[-1]['data']}")
            version = handler.received[-1]['data']['rapidsense_version']
            mode = handler.received[-1]['data']['rapidsense_mode']
            # sensor_status = handler.received[-1]['data']['sensor_status']
            error_list = handler.received[-1]['data']['error_list']
            time_stamp = datetime.datetime.fromtimestamp(send_time).strftime('%Y-%m-%d %H:%M:%S.%f')
            
            self.parameters['time_stamp'] = str(time_stamp)
            self.parameters['status'] = str(handler.received[-1]['data'])

            # Try and apply the parameters to the UI elements. If the task is in an
            # editor, these elements will exist and the try will suceed
            try:
                self.apply_task_to_ui()
            except:
                pass # not an editor widget

            # Try and apply the response to the display widget. If the task is in
            # the task list, these elements will exist and the try will suceed.
            try:
                if len(error_list) == 0:
                    self.display_label.setText(f'{self.type}: {mode}')
                else:
                    self.display_label.setText(f'{self.type}: ERROR')
            except:
                pass # not a list widget
        
        handler.close_handler()
