#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.alternate_location_task
from lib.appliance_client.messages.ASCIISetAlternateLocation import ASCIISetAlternateLocation
from lib.appliance_client.appliance_client import ApplianceClient
import time
from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException


class AlternateLocationTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, presets_targets_dict, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='SetAlternateLocation',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.presets_targets_dict = presets_targets_dict
        self.timeout = 5

    def load_ui(self):
        self.ui = ui.alternate_location_task.Ui_AlternateLocationTask()
        self.ui.setupUi(self)

        # Loop over every preset, and create a list of all unique assigned targets
        all_targets = []
        for prest in self.presets_targets_dict:
            for target in self.presets_targets_dict[prest]:
                if target not in all_targets:
                    all_targets.append(str(target))
        all_targets.sort()
        self.ui.alternate_location_combobox.insertSeparator(2)
        self.ui.alternate_location_combobox.addItems(all_targets)

    def apply_ui_to_task(self, execute_once):
        selection = self.ui.alternate_location_combobox.currentText()
        if selection == 'None':
            enabled = 0
            self.parameters = {'enabled': enabled,
                               "mode": 0, "target": "", "complete_move": 0}
        elif selection == 'Partial Move':
            enabled = 1
            mode = 1
            complete_move = 1
            self.parameters = {'enabled': enabled, "mode": mode,
                               "target": "", "complete_move": int(complete_move)}
        else:
            enabled = 1
            mode = 0
            target = selection
            complete_move = self.ui.complete_move_checkbox.isChecked()
            self.parameters = {'enabled': enabled, "mode": mode,
                               "target": target, "complete_move": int(complete_move)}
        self.parameters.update({'execute_once': execute_once})

    def apply_task_to_ui(self):
        enabled = self.parameters['enabled']
        mode = self.parameters['mode']
        target = self.parameters['target']
        complete_move = self.parameters['complete_move']
        if not enabled:
            self.ui.alternate_location_combobox.setCurrentText('None')
        if enabled and mode == 1:
            self.ui.alternate_location_combobox.setCurrentText('Partial Move')
        if enabled and mode == 0:
            self.ui.alternate_location_combobox.setCurrentText(target)
        self.ui.complete_move_checkbox.setChecked(complete_move)

    def execute(self, **kwargs):
        if not self.parameters['enabled']:
            self.logger.info(
                F"Disabling alternate location behavior for [{self.robot_name}]")
        else:
            if self.parameters['mode'] == 0:
                self.logger.info(
                    F"Enabling alternate location behavior for [{self.robot_name}] to target [{self.parameters['target']}]")
            elif self.parameters['mode'] == 1:
                self.logger.info(
                    F"Enabling partial move behavior for [{self.robot_name}]")
                
        self.error = False
        # send the ascii msg
        tic = time.time()
        handler = self.appliance_client.send_msg(
            ASCIISetAlternateLocation(robot_name=self.robot_name,
                                      enabled=self.parameters['enabled'],
                                      mode=self.parameters['mode'],
                                      target_name=self.parameters['target'],
                                      # pose=self.parameters['pose'],
                                      complete_move=self.parameters['complete_move'],
                                      # timeout=self.parameters['timeout']
                                      ))
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()
