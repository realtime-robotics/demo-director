#!/usr/bin/env python3

from lib.appliance_client.appliance_client import ApplianceClient
from lib.PythonRESTCommander import PythonRESTCommander
from PyQt5.QtWidgets import QHBoxLayout, QLabel
from lib.Utils import initialize_logger
from typing import Optional, Any, Dict
from PyQt5.QtWidgets import QWidget
import uuid
import inspect


class Task(QWidget):
    tasks: Dict[str, Any] = {}

    def __init__(self, appliance_client: ApplianceClient = None, type='', robot_name='', parameters: Dict[str, Any] = {}, uuid: str = ''):
        super().__init__()
        self.uuid = uuid
        if self.uuid == '':
            self.uuid = self._generate_uuid()
        self.logger = initialize_logger()
        self.type = type
        self.parameters = parameters
        self.robot_name = robot_name
        self.ip = ''
        self.port = ''
        self.error = False
        self.running = False
        self.appliance_client = appliance_client
        Task.tasks[str(self.uuid)] = self

    def asdict(self):
        return {"uuid": self.uuid, "type": self.type, "parameters": self.parameters}

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, item):
        return getattr(self, item)

    def stop_task(self):
        return

    def pause_task(self):
        return

    def load_list_ui(self):
        self.layout = QHBoxLayout()
        self.setLayout(self.layout)
        self.display_label = QLabel(self.to_string())
        self.display_label.setStyleSheet(
            "background: rgb(232, 233, 235); border-radius: 5px; padding: 2px; margin: 4px; border: 1px solid grey; font-size: 6")
        self.layout.addWidget(self.display_label)

    def to_string(self) -> str:
        return self.type
    
    def _make_copy_or_clone(self, exclude_uuid):
        init_signature = inspect.signature(self.__init__)
        param_names = [p.name for p in init_signature.parameters.values() if p.name != 'self' and (not exclude_uuid or p.name != 'uuid')]
        args = {name: getattr(self, name) for name in param_names}
        return type(self)(**args)
    
    def copy(self):
        ''' Returns a new instance of the task with the same UUID. '''
        return self._make_copy_or_clone(exclude_uuid=False)
    
    def clone(self):
        ''' Returns a new instance of the task with a new UUID.'''
        return self._make_copy_or_clone(exclude_uuid=True)
    
    def _generate_uuid(self):
        return str(uuid.uuid4())
