#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIScanScene import ASCIIScanScene
from lib.appliance_client.appliance_client import ApplianceClient
from ui.scan_scene import Ui_ScanScene
from lib.Tasks.Task import Task

class ScanScene(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='ScanScene', robot_name=robot_name, parameters=parameters)
        self.timeout = 30

    def load_ui(self):
        self.ui = Ui_ScanScene()
        self.ui.setupUi(self)

    def to_string(self):
        try:
            scan_scene_type = self.parameters['scan_scene_type']
        except:
            scan_scene_type = 'None'
        string = f'{self.type}: {scan_scene_type}'
        return string

    def apply_ui_to_task(self, execute_once):
        scan_scene_type = self.ui.scan_scene_type.currentText()
        self.parameters = {'scan_scene_type': scan_scene_type, 'execute_once': execute_once}

    def apply_task_to_ui(self):
        scan_scene_type = self.parameters['scan_scene_type']
        self.ui.scan_scene_type.setCurrentText(scan_scene_type)

    def execute(self, **kwargs):
        # send the ascii msg
        self.logger.info(f"Calling ScanScene with mode: {self.parameters['scan_scene_type']}")
        handler = self.appliance_client.send_msg(ASCIIScanScene(type=self.parameters['scan_scene_type']))

        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            self.logger.error(f"ScanScene failed to set mode [{self.parameters['scan_scene_type']}] with error: {e}")
            self.error = True
        
        handler.close_handler()
