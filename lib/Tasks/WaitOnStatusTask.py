#!/usr/bin/env python3

from lib.Tasks.Task import Task
import time
import ui.wait_on_status_task


class WaitOnStatusTask(Task):
    '''
    Parameters:
        'task_uuid': uuid of task to wait on
        'tate': state to wait for
        'state_value': value of state to wait for
    '''

    def __init__(self, robot_name, parameters={}, uuid: str = ''):
        self.uuid = uuid
        super().__init__(type='WaitOnStatus', parameters=parameters, uuid=self.uuid)
        self.parameters = parameters
        self.capitalize_params()
        self.robot_name = robot_name
        self.running = False
        self.stop = False
        self.pause = False
        self.states = {'Move': ['Running', 'Planned', 'Complete'],
                       'SICKMoveToDropBin1': ['Running', 'Planned', 'Complete'],
                       'SICKMoveToDropBin2': ['Running', 'Planned', 'Complete']}

    def capitalize_params(self):
        if 'state' in self.parameters:
            self.parameters['state'] = self.parameters['state'].capitalize()
        if 'state_value' in self.parameters:
            self.parameters['state_value'] = self.parameters['state_value'].capitalize(
            )

    def load_ui(self):
        self.ui = ui.wait_on_status_task.Ui_WaitOnStatusTask()
        self.ui.setupUi(self)

        self.ui.uuid_check_button.clicked.connect(self._check_button_clicked)

    def check_uuid(self, uuid: str) -> str:
        '''Returns passed uuid if valid, returns empty str if invalid'''
        check_uuid = uuid
        if check_uuid not in Task.tasks:
            self.logger.error(f'UUID {check_uuid} is not valid')
            return ''
        self.logger.info(f'UUID {check_uuid} is valid')
        return check_uuid

    def get_states(self, task: Task) -> list[str]:
        '''Returns list of states for the passed task type'''
        if task.type not in self.states:
            self.logger.error(
                f"Task {task.type} is not part of the WaitOnStatusTask states")
            return []
        return self.states[task.type]

    def apply_ui_to_task(self, execute_once):
        self.parameters['task_uuid'] = self.ui.uuid.text()
        self.parameters['state'] = self.ui.state_name.currentText()
        self.parameters['state_value'] = self.ui.state_value.currentText()
        self.parameters['execute_once'] = execute_once

    def apply_task_to_ui(self):
        uuid = self.parameters['task_uuid']
        self.ui.uuid.setText(uuid)
        if self.check_uuid(uuid) == '':
            return
        # populate states
        states = self.get_states(Task.tasks[self.parameters['task_uuid']])
        for item in states:
            self.ui.state_name.addItem(item)
        self.ui.state_name.setCurrentText(self.parameters['state'])
        # populate state value
        boolean_values = ['True', 'False']
        for item in boolean_values:
            self.ui.state_value.addItem(item)
        self.ui.state_value.setCurrentText(self.parameters['state_value'])

    def execute(self, **kwargs):
        self.running = True
        self.logger.info(
            f"Robot {self.robot_name} waiting on task {self.parameters['task_uuid']} state {self.parameters['state']} to equal {self.parameters['state_value']}")

        # Only execute on a Move task
        uuid = self.parameters['task_uuid']
        if self.check_uuid(uuid) == '':
            return
        uuid_task_type = Task.tasks[self.parameters['task_uuid']].type
        if uuid_task_type != 'Move' and uuid_task_type != 'SICKMoveToDropBin1' and uuid_task_type != 'SICKMoveToDropBin2':
            return

        # Wait until state's value is equal to state_value
        if self.parameters['state'] == 'Running':
            tic = time.time()
            while not (self.pause or self.stop):
                if Task.tasks[self.parameters['task_uuid']].running == bool(self.parameters['state_value']):
                    break
                time.sleep(0.05)
            toc = time.time()
        elif self.parameters['state'] == 'Planned':
            tic = time.time()
            while not (self.pause or self.stop):
                if Task.tasks[self.parameters['task_uuid']].planned == bool(self.parameters['state_value']):
                    break
                time.sleep(0.05)
            toc = time.time()
            self.logger.info(
                f"Robot {self.robot_name} waited {toc-tic} seconds for task {self.parameters['task_uuid']} state {self.parameters['state']} to equal {self.parameters['state_value']}")
        elif self.parameters['state'] == 'Complete':
            tic = time.time()
            while not (self.pause or self.stop):
                if Task.tasks[self.parameters['task_uuid']].complete == bool(self.parameters['state_value']):
                    self.logger.debug('Wait for COMPLETE done')
                    break
                time.sleep(0.05)
            toc = time.time()
            self.logger.info(
                f"Robot {self.robot_name} waited {toc-tic} seconds for task {self.parameters['task_uuid']} state {self.parameters['state']} to equal {self.parameters['state_value']}")
        self.stop = False
        self.pause = False
        self.running = False

    def stop_task(self):
        self.stop = True

    def pause_task(self):
        self.pause = True

    def _check_button_clicked(self):
        self.parameters['task_uuid'] = self.check_uuid(self.ui.uuid.text())
        if self.parameters['task_uuid'] == '':
            self.clear()
            return
        states = self.get_states(Task.tasks[self.parameters['task_uuid']])
        if states == []:
            self.clear()
            return
        self.ui.state_name.clear()
        for item in states:
            self.ui.state_name.addItem(item)
        self.ui.state_value.clear()
        boolean_values = ['True', 'False']
        for item in boolean_values:
            self.ui.state_value.addItem(item)

    def clear(self):
        self.ui.uuid.clear()
        self.parameters['task_uuid'] = ''
        self.ui.state_name.clear()
        self.parameters['state'] = ''
        self.ui.state_value.clear()
        self.parameters['tate_value'] = ''
