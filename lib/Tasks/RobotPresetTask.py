#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.robot_preset_task
import time
from lib.appliance_client.messages.ASCIISetRobotPreset import ASCIISetRobotPreset
from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException
from lib.appliance_client.appliance_client import ApplianceClient


class RobotPresetTask(Task):
    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, presets_targets_dict, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client, type='SetRobotPreset',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.presets_targets_dict = presets_targets_dict
        self.timeout = 30

    def load_ui(self):
        self.ui = ui.robot_preset_task.Ui_RobotPresetTask()
        self.ui.setupUi(self)

        presets = self.presets_targets_dict.keys()
        self.ui.presets_combobox.addItems(presets)

    def apply_ui_to_task(self, execute_once):
        preset_name = self.ui.presets_combobox.currentText()
        self.parameters = {'preset_name': preset_name,
                           'execute_once': execute_once}

    def apply_task_to_ui(self):
        preset_name = self.parameters['preset_name']
        self.ui.presets_combobox.setCurrentText(preset_name)

    def execute(self, **kwargs):

        # send the ascii msg
        tic = time.time()
        handler = self.appliance_client.send_msg(
            ASCIISetRobotPreset(robot_name=self.robot_name,
                                preset_name=self.parameters['preset_name']
                                ))
        
        self.error = False
        try:
            handler.wait(self.timeout)
        except Exception as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e.error_message}")
            self.error = True
        else:
            self.logger.info(
                f'Robot {self.robot_name} {handler.original.get_topic()} COMPLETE in {time.time() - tic}s')
        handler.close_handler()
