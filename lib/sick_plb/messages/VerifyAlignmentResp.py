from .MsgResp import MsgResp
from ..ui.VerifyAlignmentRecv import Ui_VerifyAlignmentRecv
from typing import Dict, Any


class VerifyAlignmentResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "imageID": None,
            "cameraAlignmentAlias": None,
            "posError": None
        }

    def get_resp_type(self) -> str:
        return "SaveAlignmentResp"

    def setupUi(self):
        self.ui = Ui_VerifyAlignmentRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.imageId.setText(str(self.parameters["imageID"]))
        self.ui.cameraAlignmentAlias.setText(
            str(self.parameters["cameraAlignmentAlias"]))
        self.ui.posError.setText(str(self.parameters["posError"]))
