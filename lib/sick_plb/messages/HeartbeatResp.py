from dataclasses import dataclass
from ..ui.HeartbeatRecv import Ui_HeartbeatRecv
from .MsgResp import MsgResp
from typing import Dict, Any


@dataclass
class HeartbeatResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "state": None,
            "rotationConvention": None
        }

    def setupUi(self):
        self.ui = Ui_HeartbeatRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def get_resp_type(self) -> str:
        return "HeartbeatResp"

    def populate_ui(self) -> None:
        self.ui.result_output.setText(str(self.parameters["result"]))
        self.ui.state_output.setText(str(self.parameters["state"]))
        self.ui.rotation_output.setText(
            str(self.parameters["rotationConvention"]))
