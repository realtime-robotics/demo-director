from typing import Dict, Any
from .MsgResp import MsgResp
from ..ui.SetStateRecv import Ui_SetStateRecv


class StateResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "state": None,
        }

    def setupUi(self):
        self.ui = Ui_SetStateRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def get_resp_type(self) -> str:
        return "StateResp"

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.state.setText(str(self.parameters["state"]))
