from .MsgResp import MsgResp
from ..ui.PartResultRecv import Ui_PartResultRecv
from typing import Dict, Any
from dataclasses import dataclass

class PartResultResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": "",
            "binFillingRatio": "",
            "jobAlias": "",
            "cameraAlignmentAlias": "",
            "numValidPickPoses": "",
            "moreToAnalyze": "",
            "highestPointX": "",
            "highestPointY": "",
            "highestPointZ": "",
            "boundingBoxSizeX": "",
            "boundingBoxSizeY": "",
            "pickPoseAlias0": "",
            "gripperAlias0": "",
            "absoluteEntryToolFrame0X": "",
            "absoluteEntryToolFrame0Y": "",
            "absoluteEntryToolFrame0Z": "",
            "absoluteEntryToolFrame0R1": "",
            "absoluteEntryToolFrame0R2": "",
            "absoluteEntryToolFrame0R3": "",
            "absoluteApproachToolFrame0X": "",
            "absoluteApproachToolFrame0Y": "",
            "absoluteApproachToolFrame0Z": "",
            "absoluteApproachToolFrame0R1": "",
            "absoluteApproachToolFrame0R2": "",
            "absoluteApproachToolFrame0R3": "",
            "absoluteToolFrame0X": "",
            "absoluteToolFrame0Y": "",
            "absoluteToolFrame0Z": "",
            "absoluteToolFrame0R1": "",
            "absoluteToolFrame0R2": "",
            "absoluteToolFrame0R3": "",
            "absoluteClearToolFrame0X": "",
            "absoluteClearToolFrame0Y": "",
            "absoluteClearToolFrame0Z": "",
            "absoluteClearToolFrame0R1": "",
            "absoluteClearToolFrame0R2": "",
            "absoluteClearToolFrame0R3": "",
            "absoluteExitToolFrame0X": "",
            "absoluteExitToolFrame0Y": "",
            "absoluteExitToolFrame0Z": "",
            "absoluteExitToolFrame0R1": "",
            "absoluteExitToolFrame0R2": "",
            "absoluteExitToolFrame0R3": "",
            "relativeToolFrame0X": "",
            "relativeToolFrame0Y": "",
            "relativeToolFrame0Z": "",
            "relativeToolFrame0R1": "",
            "relativeToolFrame0R2": "",
            "relativeToolFrame0R3": "",
        }

    def get_resp_type(self) -> str:
        return "PartResultResp"

    def setupUi(self):
        self.ui = Ui_PartResultRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
            self.ui.result.setText(self.parameters["result"])
            self.ui.binFillingRatio.setText(self.parameters["binFillingRatio"])
            self.ui.jobAlias.setText(self.parameters["jobAlias"])
            self.ui.cameraAlignmentAlias.setText(self.parameters["cameraAlignmentAlias"])
            self.ui.numValidPickPoses.setText(self.parameters["numValidPickPoses"])
            self.ui.moreToAnalyze.setText(self.parameters["moreToAnalyze"])
            self.ui.highestPointX.setText(self.parameters["highestPointX"])
            self.ui.highestPointY.setText(self.parameters["highestPointY"])
            self.ui.highestPointZ.setText(self.parameters["highestPointZ"])
            self.ui.boundingBoxSizeX.setText(self.parameters["boundingBoxSizeX"])
            self.ui.boundingBoxSizeY.setText(self.parameters["boundingBoxSizeY"])
            self.ui.pickPoseAlias0.setText(self.parameters["pickPoseAlias0"])
            self.ui.gripperAlias0.setText(self.parameters["gripperAlias0"])
            self.ui.absoluteEntryToolFrame0X.setText(self.parameters["absoluteEntryToolFrame0X"])
            self.ui.absoluteEntryToolFrame0Y.setText(self.parameters["absoluteEntryToolFrame0Y"])
            self.ui.absoluteEntryToolFrame0Z.setText(self.parameters["absoluteEntryToolFrame0Z"])
            self.ui.absoluteEntryToolFrame0R1.setText(self.parameters["absoluteEntryToolFrame0R1"])
            self.ui.absoluteEntryToolFrame0R2.setText(self.parameters["absoluteEntryToolFrame0R2"])
            self.ui.absoluteEntryToolFrame0R3.setText(self.parameters["absoluteEntryToolFrame0R3"])
            self.ui.absoluteApproachToolFrame0X.setText(self.parameters["absoluteApproachToolFrame0X"])
            self.ui.absoluteApproachToolFrame0Y.setText(self.parameters["absoluteApproachToolFrame0Y"])
            self.ui.absoluteApproachToolFrame0Z.setText(self.parameters["absoluteApproachToolFrame0Z"])
            self.ui.absoluteApproachToolFrame0R1.setText(self.parameters["absoluteApproachToolFrame0R1"])
            self.ui.absoluteApproachToolFrame0R2.setText(self.parameters["absoluteApproachToolFrame0R2"])
            self.ui.absoluteApproachToolFrame0R3.setText(self.parameters["absoluteApproachToolFrame0R3"])
            self.ui.absoluteToolFrame0X.setText(self.parameters["absoluteToolFrame0X"])
            self.ui.absoluteToolFrame0Y.setText(self.parameters["absoluteToolFrame0Y"])
            self.ui.absoluteToolFrame0Z.setText(self.parameters["absoluteToolFrame0Z"])
            self.ui.absoluteToolFrame0R1.setText(self.parameters["absoluteToolFrame0R1"])
            self.ui.absoluteToolFrame0R2.setText(self.parameters["absoluteToolFrame0R2"])
            self.ui.absoluteToolFrame0R3.setText(self.parameters["absoluteToolFrame0R3"])
            self.ui.absoluteClearToolFrame0X.setText(self.parameters["absoluteClearToolFrame0X"])
            self.ui.absoluteClearToolFrame0Y.setText(self.parameters["absoluteClearToolFrame0Y"])
            self.ui.absoluteClearToolFrame0Z.setText(self.parameters["absoluteClearToolFrame0Z"])
            self.ui.absoluteClearToolFrame0R1.setText(self.parameters["absoluteClearToolFrame0R1"])
            self.ui.absoluteClearToolFrame0R2.setText(self.parameters["absoluteClearToolFrame0R2"])
            self.ui.absoluteClearToolFrame0R3.setText(self.parameters["absoluteClearToolFrame0R3"])
            self.ui.absoluteExitToolFrame0X.setText(self.parameters["absoluteExitToolFrame0X"])
            self.ui.absoluteExitToolFrame0Y.setText(self.parameters["absoluteExitToolFrame0Y"])
            self.ui.absoluteExitToolFrame0Z.setText(self.parameters["absoluteExitToolFrame0Z"])
            self.ui.absoluteExitToolFrame0R1.setText(self.parameters["absoluteExitToolFrame0R1"])
            self.ui.absoluteExitToolFrame0R2.setText(self.parameters["absoluteExitToolFrame0R2"])
            self.ui.absoluteExitToolFrame0R3.setText(self.parameters["absoluteExitToolFrame0R3"])
            self.ui.relativeToolFrame0X.setText(self.parameters["relativeToolFrame0X"])
            self.ui.relativeToolFrame0Y.setText(self.parameters["relativeToolFrame0Y"])
            self.ui.relativeToolFrame0Z.setText(self.parameters["relativeToolFrame0Z"])
            self.ui.relativeToolFrame0R1.setText(self.parameters["relativeToolFrame0R1"])
            self.ui.relativeToolFrame0R2.setText(self.parameters["relativeToolFrame0R2"])
            self.ui.relativeToolFrame0R3.setText(self.parameters["relativeToolFrame0R3"])
