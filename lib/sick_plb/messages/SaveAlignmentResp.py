from .MsgResp import MsgResp
from ..ui.SaveAlignmentRecv import Ui_SaveAlignmentRecv
from typing import Dict, Any


class SaveAlignmentResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "cameraAlignmentAlias": None
        }

    def get_resp_type(self) -> str:
        return "SaveAlignmentResp"

    def setupUi(self):
        self.ui = Ui_SaveAlignmentRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.cameraAlignmentAlias.setText(
            str(self.parameters["cameraAlignmentAlias"]))
