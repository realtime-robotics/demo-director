from typing import Any
from .Msg import Msg
from ..ui.VerifyAlignmentSend import Ui_VerifyAlignmentSend


class VerifyAlignmentMsg(Msg):

    def __init__(self, x: float = None, y: float = None, z: float = None):
        super().__init__()
        self.x = x
        self.y = y
        self.z = z

    def setupUi(self):
        self.ui = Ui_VerifyAlignmentSend()
        self.ui.setupUi(self)
        self.ui.x.setText(str(self.x))
        self.ui.y.setText(str(self.y))
        self.ui.z.setText(str(self.z))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()}, {self.x},{self.y},{self.z};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "VerifyAlignment"

    def get_resp_type(self) -> str:
        return "VerifyAlignmentResp"
