from typing import Any
from .Msg import Msg
from PyQt5.QtWidgets import QWidget
from ..ui.SetStateSend import Ui_SetStateSend


class SetStateMsg(Msg):

    def __init__(self, state: int = 1, camera_alias: int = 0):
        super().__init__()
        self.state = state
        self.camera_alias = camera_alias

    def setupUi(self):
        self.ui = Ui_SetStateSend()
        self.ui.setupUi(self)
        self.ui.state.setText(str(self.state))
        self.ui.cameraAlignmentAlias.setText(str(self.camera_alias))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.state},{self.camera_alias};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "SetState"

    def get_resp_type(self) -> str:
        return "StateResp"
    
    def ui_to_data(self) -> None:
        self.state = int(self.ui.state.text())
        self.camera_alias = int(self.ui.cameraAlignmentAlias.text())
