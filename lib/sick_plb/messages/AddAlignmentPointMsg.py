from dataclasses import dataclass
from typing import Any
from .Msg import Msg
from PyQt5.QtWidgets import QWidget
from ..ui.AddAlignmentPointSend import Ui_AddAlignmentPointSend


class AddAlignmentPointMsg(Msg):

    def __init__(self, x: float = 0.0, y: float = 0.0, z: float = 0.0):
        super().__init__()
        self.x = x
        self.y = y
        self.z = z

    def setupUi(self):
        self.ui = Ui_AddAlignmentPointSend()
        self.ui.setupUi(self)
        self.ui.x_lineedit.setText(str(self.x))
        self.ui.y_lineedit.setText(str(self.y))
        self.ui.z_lineedit.setText(str(self.z))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.x},{self.y},{self.z};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "AddAlignmentPoint"

    def get_resp_type(self) -> str:
        return "AddAlignmentPointResp"
