from .MsgResp import MsgResp
from ..ui.TriggerRecv import Ui_TriggerRecv
from typing import Dict, Any


class TriggerResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "imageId": None,
            "cameraAlignmentAlias": None
        }

    def get_resp_type(self) -> str:
        return "TriggerResp"

    def setupUi(self):
        self.ui = Ui_TriggerRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.imageId.setText(str(self.parameters["imageId"]))
        self.ui.cameraAlignmentAlias.setText(
            str(self.parameters["cameraAlignmentAlias"]))
