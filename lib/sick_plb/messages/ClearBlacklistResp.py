from .MsgResp import MsgResp
from ..ui.ClearBlacklistRecv import Ui_ClearBlacklistRecv
from typing import Dict, Any


class ClearBlacklistResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "jobAlias": None,
        }

    def get_resp_type(self) -> str:
        return "ClearBlacklistResp"

    def setupUi(self):
        self.ui = Ui_ClearBlacklistRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.jobAlias.setText(
            str(self.parameters["jobAlias"]))
