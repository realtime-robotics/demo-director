from .MsgResp import MsgResp
from ..ui.BinResultRecv import Ui_BinResultRecv
from typing import Dict, Any


class BinResultResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "imageId": None,
            "jobAlias": None,
            'cameraAlignmentAlias': None,
            'binX': None,
            'binY': None,
            'binZ': None,
            'binR1': None,
            'binR2': None,
            'binR3': None,
            'binFillingRatio': None
        }

    def get_resp_type(self) -> str:
        return "BinResultResp"

    def setupUi(self):
        self.ui = Ui_BinResultRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result.setText(str(self.parameters["result"]))
        self.ui.imageId.setText(str(self.parameters["imageId"]))
        self.ui.jobAlias.setText(
            str(self.parameters["jobAlias"]))
        self.ui.cameraAlignmentAlias.setText(
            str(self.parameters["cameraAlignmentAlias"]))
        self.ui.binX.setText(str(self.parameters['binX']))
        self.ui.binY.setText(str(self.parameters['binY']))
        self.ui.binZ.setText(str(self.parameters['binZ']))
        self.ui.binR1.setText(str(self.parameters['binR1']))
        self.ui.binR2.setText(str(self.parameters['binR2']))
        self.ui.binR3.setText(str(self.parameters['binR3']))
        self.ui.binFillingRatio.setText(
            str(self.parameters['binFillingRatio']))
