from __future__ import annotations

from abc import abstractmethod
from typing import Any
from PyQt5.QtWidgets import QWidget

# from ..view.exceptions import MessageException


class MsgResp(QWidget):
    @abstractmethod
    def get_resp_type(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def populate_ui(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def setupUi(self) -> None:
        raise NotImplementedError()
    
    def validate_resp(self, response: list) -> bool:
        # check that number of fields in response is correct
        if len(response) != (len(self.parameters) + 1):
            return False
        # check that first field in response is the correct type
        if response[0] != self.get_resp_type():
            return False
        else:
            return True
