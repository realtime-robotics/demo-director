from dataclasses import dataclass

@dataclass
class SetCameraParameterMsg:
    SetCameraParameterMsg : str = None
    result : int = None