from __future__ import annotations

from abc import abstractmethod
from typing import Any, Protocol
from PyQt5.QtWidgets import QWidget
from .MsgResp import MsgResp

# from ..view.exceptions import MessageException


class Msg(QWidget):
    @abstractmethod
    def to_msg(self) -> Any:
        raise NotImplementedError()

    @abstractmethod
    def get_type(self) -> str:
        raise NotImplementedError()

    @abstractmethod
    def setupUi(self) -> None:
        raise NotImplementedError()

    @abstractmethod
    def get_resp_type(self) -> str:
        raise NotImplementedError()
    
    @abstractmethod
    def ui_to_data(self) -> None:
        raise NotImplementedError()
