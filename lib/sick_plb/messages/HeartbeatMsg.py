from dataclasses import dataclass
from typing import Any
from .Msg import Msg
from .MsgResp import MsgResp
from PyQt5.QtWidgets import QWidget
from ..ui.HeartbeatSend import Ui_HeartbeatSend
from .HeartbeatResp import HeartbeatResp


class HeartbeatMsg(Msg):

    def __init__(self):
        super().__init__()

    def setupUi(self):
        self.ui = Ui_HeartbeatSend()
        self.ui.setupUi(self)

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "Heartbeat"

    def get_resp_type(self) -> str:
        return "HeartbeatResp"
    
    def ui_to_data(self) -> None:
        pass
