from dataclasses import dataclass
from typing import Any
from .Msg import Msg
from PyQt5.QtWidgets import QWidget
from ..ui.LocatePartSend import Ui_LocatePartSend


class LocatePartMsg(Msg):

    def __init__(self, jobAlias: str = 'QuestFR', binAlias: str = 'Camera1'):
        super().__init__()
        self.jobAlias = jobAlias
        self.binAlias = binAlias

    def setupUi(self):
        self.ui = Ui_LocatePartSend()
        self.ui.setupUi(self)
        self.ui.jobAlias.setText(str(self.jobAlias))
        self.ui.binAlias.setText(str(self.binAlias))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.jobAlias},{self.binAlias};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "LocatePart"

    def get_resp_type(self) -> str:
        return "PartResultResp"

    def ui_to_data(self) -> None:
        self.jobAlias = self.ui.jobAlias.text()
        self.binAlias = self.ui.binAlias.text()
