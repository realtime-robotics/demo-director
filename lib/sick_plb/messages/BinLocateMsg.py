from dataclasses import dataclass
from typing import Any
from .Msg import Msg
from PyQt5.QtWidgets import QWidget
from ..ui.BinLocateSend import Ui_BinLocateSend


class BinLocateMsg(Msg):

    def __init__(self, jobAlias: int = None):
        super().__init__()
        self.jobAlias = jobAlias

    def setupUi(self):
        self.ui = Ui_BinLocateSend()
        self.ui.setupUi(self)
        self.ui.jobAlias.setText(str(self.jobAlias))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.jobAlias};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "BinLocate"

    def get_resp_type(self) -> str:
        return "BinResultResp"
