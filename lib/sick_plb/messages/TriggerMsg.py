from typing import Any
from .Msg import Msg
from ..ui.TriggerSend import Ui_TriggerSend


class TriggerMsg(Msg):

    def __init__(self, image_id: int = 1, camera_alias: int = 0):
        super().__init__()
        self.image_id = image_id
        self.camera_alias = camera_alias

    def setupUi(self):
        self.ui = Ui_TriggerSend()
        self.ui.setupUi(self)
        self.ui.imageId.setText(str(self.image_id))
        self.ui.cameraAlias.setText(str(self.camera_alias))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.image_id},{self.camera_alias};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "Trigger"

    def get_resp_type(self) -> str:
        return "TriggerResp"
    
    def ui_to_data(self) -> None:
        self.image_id = int(self.ui.imageId.text())
        self.camera_alias = int(self.ui.cameraAlias.text())
