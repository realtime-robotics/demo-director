from .MsgResp import MsgResp
from ..ui.AddAlignmentPointRecv import Ui_AddAlignmentPointRecv
from typing import Dict, Any


class AddAlignmentPointResp(MsgResp):

    def __init__(self):
        super().__init__()
        self.parameters: Dict[str, Any] = {
            "result": None,
            "imageId": None,
            "cameraAlignmentAlias": None
        }

    def get_resp_type(self) -> str:
        return "AddAlignmentPointResp"

    def setupUi(self):
        self.ui = Ui_AddAlignmentPointRecv()
        self.ui.setupUi(self)
        self.populate_ui()

    def populate_ui(self) -> None:
        self.ui.result_output.setText(str(self.parameters["result"]))
        self.ui.image_id_output.setText(str(self.parameters["imageId"]))
        self.ui.cam_alias_output.setText(
            str(self.parameters["cameraAlignmentAlias"]))
