from typing import Any
from .Msg import Msg
from ..ui.SaveAlignmentSend import Ui_SaveAlignmentSend


class SaveAlignmentMsg(Msg):

    def __init__(self):
        super().__init__()

    def setupUi(self):
        self.ui = Ui_SaveAlignmentSend()
        self.ui.setupUi(self)

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "SaveAlignment"

    def get_resp_type(self) -> str:
        return "SaveAlignmentResp"
