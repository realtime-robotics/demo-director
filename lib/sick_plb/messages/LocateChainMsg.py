
from typing import Any
from .Msg import Msg
from PyQt5.QtWidgets import QWidget
from ..ui.LocateChainSend import Ui_LocateChainSend


class LocateChainMsg(Msg):

    def __init__(self, triggerImage: int = None, imageId: int = None, cameraAlias: int = None, locateBin: int = None, jobAliasChain: str = None):
        super().__init__()
        self.triggerImage = triggerImage
        self.imageId = imageId
        self.cameraAlias = cameraAlias
        self.locateBin = locateBin
        self.jobAliasChain = jobAliasChain

    def setupUi(self):
        self.ui = Ui_LocateChainSend()
        self.ui.setupUi(self)
        self.ui.triggerImage.setText(str(self.triggerImage))
        self.ui.imageId.setText(str(self.imageId))
        self.ui.cameraAlias.setText(str(self.cameraAlias))
        self.ui.locateBin.setText(str(self.locateBin))
        self.ui.jobAliasChain.setText(str(self.jobAliasChain))

    def to_msg(self) -> bytes:
        send_data = f'{self.get_type()},{self.jobAliasChain},{self.triggerImage}{self.imageId},{self.cameraAlias},{self.locateBin};'
        return bytes(send_data, 'utf-8')

    def get_type(self) -> str:
        return "LocateChain"

    def get_resp_type(self) -> str:
        return "PartResultResp"
