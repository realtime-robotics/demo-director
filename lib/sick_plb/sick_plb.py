from enum import Enum
import socket
import time
import inspect
import pkgutil
from typing import Dict, Optional
from dataclasses import dataclass, fields
from .messages.Msg import Msg
from .messages.MsgResp import MsgResp
from PyQt5 import QtWidgets
from . import messages
from .messages.AddAlignmentPointMsg import AddAlignmentPointMsg
from .messages.HeartbeatMsg import HeartbeatMsg
from .messages.HeartbeatResp import HeartbeatResp
from .messages.AddAlignmentPointResp import AddAlignmentPointResp
from .messages.SetStateMsg import SetStateMsg
from .messages.StateResp import StateResp
from .messages.TriggerMsg import TriggerMsg
from .messages.TriggerResp import TriggerResp
from .messages.PartResultResp import PartResultResp
from .messages.LocatePartMsg import LocatePartMsg
from .messages.BinLocateMsg import BinLocateMsg
from .messages.BinResultResp import BinResultResp
from .messages.ClearBlacklistMsg import ClearBlacklistMsg
from .messages.ClearBlacklistResp import ClearBlacklistResp
from .messages.LocateChainMsg import LocateChainMsg
from .messages.SaveAlignmentMsg import SaveAlignmentMsg
from .messages.SaveAlignmentResp import SaveAlignmentResp
from .messages.VerifyAlignmentMsg import VerifyAlignmentMsg
from .messages.VerifyAlignmentResp import VerifyAlignmentResp
import os
from .ui.sick_plb_camera import Ui_SICK_PLB_Camera
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal
from lib.Utils import initialize_logger


class states(Enum):
    RUN = 1
    CONFIGURE = 2
    ALIGN = 3
    ANALYZE = 4


class SickPLBCamera(QWidget):
    name_changed_signal = pyqtSignal(str)
    connection_data: Dict[str, socket.socket] = {}
    error_codes = {
        0: 'OK',
        -1: 'Unexpected error',
        3: 'License error',
        4: 'Save file error',
        21: 'Unkown job',
        22: 'Invalid job',
        31: 'Set state failed',
        32: 'PLB busy',
        41: 'No sensor connected',
        42: 'Invalid state',
        43: 'Overtrigger',
        44: 'Acquisition failed',
        51: 'Unknown camera',
        52: 'Invalid camera',
        101: 'No image',
        102: 'Invalid state',
        103: 'PLB busy',
        111: 'No part located',
        112: 'Part overlapped',
        113: 'Gripper collision',
        114: 'Bin empty',
        115: 'Unknown gripper',
        116: 'No enabled pick poses',
        121: 'Bin not found',
        122: 'Bin not defined',
        131: 'Alignment target not found',
        132: 'Failed to add alignment point',
        133: 'Too few points in alignment',
        134: 'Alignment calculation failed',
        135: 'Threshold succeeded',
        136: 'Too much scaling',
        137: 'Invalid z-axis direction',
        141: 'Invalid parameter name',
        142: 'Invalid parameter value',
        143: 'Invalid parameter change occasion',
        144: 'Read-only parameter',
        151: 'Invalid configuration option'
    }

    def __init__(self, ip: Optional[str] = '', port: Optional[int] = 6008, name: Optional[str] = ''):
        super().__init__()
        self.logger = initialize_logger()
        self.ip = ip
        self.port = port
        self.name = name
        self.socket: socket.socket = None
        self.connected = False
        self.setup_messages()
        self.load_camera_ui()

    def setup_messages(self):
        # Set up send/recv objects
        self.send_dict: Dict[str, Msg] = {
            'Heartbeat': HeartbeatMsg(),
            'AddAlignmentPoint': AddAlignmentPointMsg(),
            'SetState': SetStateMsg(),
            'Trigger': TriggerMsg(),
            'LocatePart': LocatePartMsg(),
            'BinLocate': BinLocateMsg(),
            'ClearBlacklist': ClearBlacklistMsg(),
            'LocateChain': LocateChainMsg(),
            'SaveAlignment': SaveAlignmentMsg(),
            'VerifyAlignment': VerifyAlignmentMsg()
        }
        self.recv_dict: Dict[str, MsgResp] = {
            'HeartbeatResp': HeartbeatResp(),
            'AddAlignmentPointResp': AddAlignmentPointResp(),
            'PartResultResp': PartResultResp(),
            'StateResp': StateResp(),
            'TriggerResp': TriggerResp(),
            'BinResultResp': BinResultResp(),
            'ClearBlacklistResp': ClearBlacklistResp(),
            'SaveAlignmentResp': SaveAlignmentResp(),
            'VerifyAlignmentResp': VerifyAlignmentResp()
        }
        # Add UI for each object
        for message in self.send_dict.values():
            message.setupUi()
        for resp in self.recv_dict.values():
            resp.setupUi()

    def load_camera_ui(self):
        self.ui = Ui_SICK_PLB_Camera()
        self.ui.setupUi(self)
        self.ui.name_lineedit.setText(self.name)
        self.ui.ip_lineedit.setText(self.ip)
        self.ui.port_lineedit.setText(str(self.port))
        self.ui.message_combobox.addItems(self.send_dict.keys())
        self.ui.message_combobox.currentIndexChanged.connect(
            self.load_message_ui)
        self.ui.send_recv_button.clicked.connect(self.send_recv_manual)
        self.ui.name_lineedit.textChanged.connect(self.update_name)
        self.ui.connect_button.clicked.connect(self.connect)
        self.ui.ip_lineedit.textChanged.connect(self.update_ip)
        self.ui.port_lineedit.textChanged.connect(self.update_port)
        self.load_message_ui()

    def load_message_ui(self):
        # clear send and receive layouts
        for i in reversed(range(self.ui.send_layout.count())):
            self.ui.send_layout.itemAt(i).widget().setParent(None)
        for i in reversed(range(self.ui.receive_layout.count())):
            self.ui.receive_layout.itemAt(i).widget().setParent(None)
        message_name = self.ui.message_combobox.currentText()
        message = self.send_dict[message_name]
        resp = self.recv_dict[message.get_resp_type()]
        self.ui.send_layout.addWidget(message)
        self.ui.receive_layout.addWidget(resp)

    def update_name(self):
        text = self.ui.name_lineedit.text()
        validated_text = self.validate_name_description(text)
        if text != validated_text:
            cursor_pose = self.ui.name_lineedit.cursorPosition()
            self.ui.name_lineedit.setText(validated_text)
            self.ui.name_lineedit.setCursorPosition(cursor_pose - 1)
        self.name = validated_text
        self.name_changed_signal.emit(validated_text)

    def update_ip(self):
        self.ip = self.ui.ip_lineedit.text()

    def update_port(self):
        self.port = self.ui.port_lineedit.text()

    def validate_name_description(self, text):
        invalid_characters = ["'", '"']
        for character in invalid_characters:
            if character in text:
                text = text.replace(character, "")
                self.logger.info(
                    f'Invalid character [{character}] found and removed.')
        return text

    def is_connected(self) -> bool:
        return self.socket is not None

    def connect(self):
        self.ui.status_label.setText('Connecting...')
        self.ui.status_label.setStyleSheet('color: black;')
        if self.ip in self.connection_data.keys():
            self.socket = self.connection_data[self.ip]
            address = self.socket.getsockname()
            self.logger.warning(
                f'Already connected to SICK PLB sensor at {address[0]}:{address[1]}')
            self.connected = True
            self.ui.status_label.setText('Already Connected!')
            self.ui.status_label.setStyleSheet('color: green;')
            return
        try:
            self.logger.info(
                f'Connecting to SICK PLB sensor at ip: {self.ip}, port: {self.port}...')
            self.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
            self.socket.settimeout(5)
            self.socket.connect((self.ip, int(self.port)))
            self.socket.settimeout(None)
            self.logger.info(
                f'Connected successfully to SICK PLB sensor at {self.ip}:{self.port}')
            self.connection_data[self.ip] = self.socket
            self.connected = True
            self.ui.status_label.setText('Connected!')
            self.ui.status_label.setStyleSheet('color: green;')
        except Exception as e:
            self.connected = False
            self.logger.error(
                f'Error connecting to SICK PLB sensor at {self.ip}:{self.port}: {e}')
            self.ui.status_label.setText('Error!')
            self.ui.status_label.setStyleSheet('color: red;')

    def disconnect(self, ip=None, port=None):
        self.ip = ip if ip is not None else self.ip
        self.port = port if port is not None else self.port
        if self.ip in self.connection_data.keys():
            self.socket = self.connection_data[self.ip]
            self.socket.close()
            self.logger.info(
                f'Closed connection to SICK PLB sensor at {self.ip}:{self.port}')
        else:
            self.logger.error(
                f'No connection to SICK PLB sensor at {self.ip}:{self.port}')

    def send_recv_msg(self, send_msg: Msg, recv_msg: MsgResp) -> Optional[MsgResp]:
        if not self.connected:
            self.logger.error(
                f'Not connected to SICK PLB sensor at {self.ip}:{self.port}')
            return None
        self.send_msg(send_msg)
        return self.recv_msg(recv_msg)

    def send_msg(self, send_msg: Msg):
        self.socket.sendall(send_msg.to_msg())

    def recv_msg(self, recv_msg: MsgResp) -> Optional[MsgResp]:
        recv_toc = time.time()
        recv_data = self.socket.recv(4096)
        recv_tic = time.time()
        self.logger.info(
            f'SICK PLB Received Message [{round(recv_tic-recv_toc,3)}s]: {recv_msg.get_resp_type()}')
        # parse the response
        resp = "".join(recv_data.decode().splitlines()).replace(" ", "")
        resp_list = resp.replace("\t", "").replace(";", "").split(',')
        # send the response to be validated
        if not recv_msg.validate_resp(resp_list):
            self.logger.error(f'Invalid response received: {resp}')
            return None
        # Populate Parameters (ignore first field in response)
        keys = list(recv_msg.parameters)
        for i, key in enumerate(keys):
            recv_msg.parameters[key] = resp_list[i + 1]
        recv_msg.populate_ui()
        # return the response object, which holds the data
        return recv_msg

    def send_recv_manual(self):
        message_name = self.ui.message_combobox.currentText()
        message = self.send_dict[message_name]
        recv_msg = self.recv_dict[message.get_resp_type()]
        message.ui_to_data()
        recv_msg = self.send_recv_msg(message, recv_msg)
        if recv_msg is None:
            return


########### NOT IMPLEMENTED YET ##############

    # def log(self, log_level, message : str) -> Optional[LogMsg]:
    #     command = 'Log'
    #     data = f'{command},{log_level},{message}#;'
    #     resp_list = self.send_recv_msg(data)
    #     log_msg = LogMsg()
    #     if not self.validate_resp(resp_list, log_msg):
    #         return None
    #     return self.populate(resp_list, log_msg)

    # def set_camera_parameter(self, parameter_name : str, parameter_value, camera_alias : int = 0) -> Optional[SetCameraParameterMsg]:
    #     command = 'SetCameraParameter'
    #     data = f'{command},{parameter_name},{parameter_value},{camera_alias};'
    #     resp_list = self.send_recv_msg(data)
    #     set_camera_parameter_msg = SetCameraParameterMsg()
    #     if not self.validate_resp(resp_list, set_camera_parameter_msg):
    #         return None
    #     return self.populate(resp_list, set_camera_parameter_msg)

    # def set_job_parameter(self, parameter_name : str, parameter_value, job_alias : str) -> Optional[SetJobParameterMsg]:
    #     command = 'SetJobParameter'
    #     data = f'{command},{parameter_name},{parameter_value},{job_alias};'
    #     resp_list = self.send_recv_msg(data)
    #     set_job_parameter_msg = SetJobParameterMsg()
    #     if not self.validate_resp(resp_list, set_job_parameter_msg):
    #         return None
    #     return self.populate(resp_list, set_job_parameter_msg)
