from lib.Tasks.Task import Task
from lib.sick_plb.messages.TriggerMsg import TriggerMsg
from lib.sick_plb.messages.SetStateMsg import SetStateMsg
from lib.sick_plb.messages.TriggerResp import TriggerResp
from lib.sick_plb.sick_plb import SickPLBCamera
from lib.sick_plb.Tasks.SICKLocatePartsTask import SICKLocatePartsTask
from lib.sick_plb.messages import StateResp
from PyQt5.QtWidgets import QCheckBox
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
import socket
import time
from threading import Thread


class SICKTriggerTask(TriggerMsg, Task):
    RESULT_DESC = ['TriggerResp',
                   'result',
                   'imageId',
                   'cameraAlignmentAlias']

    def __init__(self, robot_name, parameters={}, uuid: str = ''):
        self.uuid = uuid
        TriggerMsg.__init__(self)
        Task.__init__(self, type='SICKTrigger', robot_name=robot_name,
                      parameters=parameters, uuid=self.uuid)
        self.running = False
        self.complete = False

    def asdict(self):
        return {"uuid": self.uuid, "type": self.type, "parameters": self.parameters}

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, item):
        return getattr(self, item)

    def load_ui(self):
        self.setupUi()
        self.ui.queue_checkbox = QCheckBox('Queue')
        self.ui.verticalLayout.addWidget(
            self.ui.queue_checkbox, alignment=Qt.AlignmentFlag.AlignRight)

    def apply_ui_to_task(self, execute_once):
        self.parameters['image_id'] = int(self.ui.imageId.text())
        self.parameters['camera_alias'] = int(self.ui.cameraAlias.text())
        self.parameters['execute_once'] = execute_once
        self.parameters['queue'] = self.ui.queue_checkbox.isChecked()

    def apply_task_to_ui(self):
        self.ui.imageId.setText(str(self.parameters['image_id']))
        self.ui.cameraAlias.setText(str(self.parameters['camera_alias']))
        self.ui.queue_checkbox.setChecked(self.parameters['queue'])

    def execute(self, **kwargs):
        self.running = True
        self.complete = False
        if not SickPLBCamera.connection_data:
            self.logger.error('No SICK connection data')
            raise Exception('No SICK connection data')
        self.socket = next(iter(SickPLBCamera.connection_data.values()))

        run_state = 1
        self.logger.info(
            f'Executing SICKTriggerTask [{self.parameters["camera_alias"]}]...')
        send_msg_set_state = SetStateMsg(
            state=run_state, camera_alias=self.parameters['camera_alias'])
        toc = time.time()
        self.socket.sendall(send_msg_set_state.to_msg())
        recv_data = self.socket.recv(4096)
        self.logger.info(
            f'SICK PLB Received Message [{round(time.time()-toc,3)}s]: StateResp')

        # # if there are still parts avaialble, don't take a new picture
        # if len(SICKLocatePartsTask.locate_part_results) > 0 and (Robot.no_move_available_1 or Robot.no_move_available_2 or Robot.no_move_available_3):
        #     self.logger.info(
        #         f'{len(SICKLocatePartsTask.locate_part_results)} parts still available, not triggering camera')
        #     self.complete = True
        #     self.running = False
        #     return

        # set up message
        send_msg = TriggerMsg(
            image_id=self.parameters['image_id'], camera_alias=self.parameters['camera_alias'])

        # set up receive message
        recv_msg = TriggerResp()

        SICKLocatePartsTask.locate_part_results = []
        self.send_msg(send_msg)
        recv_thread = Thread(target=self.recv_msg,
                             name='TriggerRecv', args=(recv_msg,))
        recv_thread.start()
        if not self.parameters['queue']:
            Thread.join(recv_thread)

    def send_msg(self, send_msg: TriggerMsg):
        self.socket.sendall(send_msg.to_msg())

    def recv_msg(self, recv_msg: TriggerResp):
        recv_toc = time.time()
        recv_data = self.socket.recv(4096)
        recv_tic = time.time()
        self.logger.info(
            f'SICK PLB Received Message [{round(recv_tic-recv_toc,3)}s]: {recv_msg.get_resp_type()}')
        # parse the response
        resp = "".join(recv_data.decode().splitlines()).replace(" ", "")
        resp_list = resp.replace("\t", "").replace(";", "").split(',')
        # send the response to be validated
        if not recv_msg.validate_resp(resp_list):
            self.logger.error(f'Invalid Trigger response received: {resp}')
            raise Exception('Invalid Trigger response received')
        results = dict(zip(self.RESULT_DESC, resp_list))
        self.complete = True
        self.running = False

    def copy(self):
        return SICKTriggerTask(robot_name=self.robot_name, parameters=self.parameters, uuid=self.uuid)
