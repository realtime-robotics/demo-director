from lib.Tasks.Task import Task
import time
import random
from lib.appliance_client.appliance_client import ApplianceClient, Handler, Message
from lib.appliance_client.messages.ASCIICancelMove import ASCIICancelMove
from lib.appliance_client.messages.ASCIIMove import ASCIIMove
from lib.sick_plb.ui.MoveToDrop import Ui_MoveToDrop
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtCore import Qt


class SICKMoveToDropBin2(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, uuid: str = ''):
        self.uuid = uuid
        self.robot_name = robot_name
        super().__init__(appliance_client=appliance_client, type='SICKMoveToDropBin2', 
                         robot_name=self.robot_name, parameters=parameters, uuid=self.uuid)
        self.running = False
        self.timeout = None
        self.planned = False
        self.complete = False
        self.error = False

    def load_ui(self):
        self.ui = Ui_MoveToDrop()
        self.ui.setupUi(self)
        self.ui.queue_checkbox = QCheckBox('Queue')
        self.ui.verticalLayout.addWidget(
            self.ui.queue_checkbox, alignment=Qt.AlignmentFlag.AlignRight)
    
    def msg_callback(self, msg: Message):
        if Message.is_response(msg):
            self.logger.info(f'Response Received ({self.uuid}): {msg}')
            self.planned = True
        if Message.is_feedback(msg):
            self.logger.info(f'Feedback Received ({self.uuid}): {msg}')
            self.feedback += 1
        if Message.is_delayed_response(msg):
            self.logger.info(f'Delayed Response Received ({self.uuid}): {msg}')
            self.complete = True
        if Message.has_error(msg):
            self.logger.error(f'Error Received ({self.uuid}): {msg}')
            self.error = True

        
    def choose_target(self):
        target = ''
        rows = [6, 7]
        columns = ['A', 'B']

        row = random.choice(rows)
        column = random.choice(columns)
        
        robot_number = 7
        if '8' in self.robot_name:
            robot_number = 8

        target = 'RV' + str(robot_number) + '_gripper_with_part_bin2_' + str(row) + str(column)
        return target
    

    def execute(self, **kwargs):
        tic = time.time()
        self.running = True
        self.planned = False
        self.complete = False
        self.error = False

        self.logger.info(f"[{self.robot_name}] moving to random target with speed override [{kwargs.get('speed_override')}]")
        move_message = self.get_message(**kwargs)

        if not self.running:
            return
        try:
            handler = self.appliance_client.send_msg(move_message, self.msg_callback)
            if not self.parameters['queue']:
                handler.wait(self.timeout, wait_callbacks_timeout=3)
        except Exception as e:
            self.logger.error(f"[{self.robot_name}] {move_message.get_topic()} ERROR: {e}")
        else:
            self.logger.info(f'Robot {self.robot_name} {handler.original.get_topic()}  COMPLETE in {time.time() - tic}s')
        self.running = False


    def get_message(self, **kwargs) -> ASCIIMove:
        self.logger.debug('Getting ASCIIMove message...')
        tic = time.time()
        move_message = ASCIIMove(
            robot_name=self.robot_name,
            speed= 1 * kwargs.get('speed_override'),
            smoothing=150.0,
            move_type='roadmap',
            timeout=None,
            preset_name='GripperWithPart'
        )

        random_target = self.choose_target()
        self.logger.info(f"[{self.robot_name}] moving to random target [{random_target}]")
        move_message.target = random_target
        
        self.logger.debug(
            f'[{self.robot_name}] Move message: {move_message} get_message() time: {tic - time.time()}')
        
        return move_message

    def apply_ui_to_task(self, execute_once):
        self.parameters['execute_once'] = execute_once
        self.parameters['queue'] = self.ui.queue_checkbox.isChecked()

    def apply_task_to_ui(self):
        self.ui.queue_checkbox.setChecked(self.parameters['queue'])

    def copy(self):
        return SICKMoveToDropBin2(self.robot_name, self.parameters, self.appliance_client, uuid=self.uuid)

    def send_cancel_move(self, acceleration_factor):
        self.logger.info(
            f'[{self.robot_name}] cancelling Move command at acceleration factor {acceleration_factor}')
        handler = self.appliance_client.send_msg(ASCIICancelMove(
            robot_name=self.robot_name, acceleration_factor=acceleration_factor))
        handler.close_handler()
    
    def stop_task(self):
        self.send_cancel_move(0.8)
        self.running = False

    def pause_task(self):
        self.send_cancel_move(0.5)
        self.running = False
