#!/usr/bin/env python3

from lib.Tasks.Task import Task
import ui.move_task
import time
from lib.appliance_client.messages.ASCIIMove import ASCIIMove
from lib.appliance_client.messages.ASCIICancelMove import ASCIICancelMove
from lib.appliance_client.messages.ASCIICombinedMove import ASCIICombinedMove
from lib.appliance_client.appliance_client import ApplianceClient, Handler, Message
from lib.sick_plb.Tasks.SICKLocatePartsTask import SICKLocatePartsTask
from lib.Tasks.DigitalIOTask import DigitalIO
from lib.GlobalVariables import GlobalVariables
from lib.sick_plb.ui.CombinedMovePick import Ui_CombinedMovePick
from PyQt5.QtWidgets import QCheckBox
from PyQt5.QtCore import Qt
import lib.Robot as Robot
from math import hypot


class SICKCombinedPick1(Task):

    def __init__(self, robot_name, parameters, appliance_client: ApplianceClient, global_variables: GlobalVariables, uuid: str = ''):
        self.uuid = uuid
        super().__init__(appliance_client=appliance_client, type='SICKCombinedPick1',
                         robot_name=robot_name, parameters=parameters, uuid=self.uuid)
        self.global_variables = global_variables
        self.running = False
        self.timeout = None
        self.planned = False
        self.complete = False
        self.error = False
        self.feedback = 0
        self.digital_io = DigitalIO(robot_name=robot_name, parameters={
                                    "action": "Set", "id": 1, "value": "On", "execute_once": 0})

    def load_ui(self):
        self.ui = Ui_CombinedMovePick()
        self.ui.setupUi(self)
        self.ui.queue_checkbox = QCheckBox('Queue')
        self.ui.verticalLayout.addWidget(
            self.ui.queue_checkbox, alignment=Qt.AlignmentFlag.AlignRight)

    def msg_callback(self, msg: Message):
        if Message.is_response(msg):
            self.logger.info(
                f'{self.robot_name}: Response Received ({self.uuid}): {msg}')
            self.planned = True
        if Message.is_feedback(msg):
            if msg['data']['index'] == 0:
                self.logger.info(
                    f'{self.robot_name}: Index 0 of CombinedMove COMPLETE -> Triggering DIO{self.digital_io.parameters["id"]}')
                self.digital_io.execute()
            self.logger.info(
                f'{self.robot_name}: Feedback Received ({self.uuid}): {msg}')
            self.feedback += 1
        if Message.is_delayed_response(msg):
            self.logger.info(
                f'{self.robot_name}: Delayed Response Received ({self.uuid}): {msg}')
            self.complete = True
        if Message.has_error(msg):
            self.logger.error(
                f'{self.robot_name}: Error Received ({self.uuid}): {msg}')

    def execute(self, **kwargs):
        tic = time.time()
        self.running = True
        self.planned = False
        self.complete = False
        self.feedback = 0
        self.error = False
        self.logger.info(
            f"[{self.robot_name}] performing SICKCombinedPick1 with speed override [{kwargs.get('speed_override')}]")

        loop = True
        Robot.Robot.no_move_available_1 = False
        # start from the back of list (farthest from RV8)
        self.part_idx = len(SICKLocatePartsTask.locate_part_results) - 1
        while loop:
            # break if no parts left in list
            if self.part_idx < 0:
                Robot.Robot.no_move_available_1 = True
                break
            # check that the idx is at the back of the list
            if (len(SICKLocatePartsTask.locate_part_results) - 1) < self.part_idx:
                self.part_idx = len(
                    SICKLocatePartsTask.locate_part_results) - 1
            # Create the CombinedMove message
            combined_move_message = self.get_message(self.part_idx, **kwargs)

            if not self.running:
                return
            try:
                handler = self.appliance_client.send_msg(
                    combined_move_message, self.msg_callback)
                handler.wait(self.timeout, wait_callbacks_timeout=3)
                loop = False
            except Exception as e:
                self.logger.error(
                    f"[{self.robot_name}] {combined_move_message.get_topic()} ERROR: {e}")
                SICKLocatePartsTask.locate_part_results.insert(
                    self.part_idx, self.locate_part_result)
                self.part_idx -= 1

    def get_message(self, idx: int, **kwargs) -> ASCIICombinedMove:
        # Create the CombinedMove message
        combined_move_message = ASCIICombinedMove(
            robot_name=self.robot_name, preset_name='GripperWithPart')
        moves = []
        self.locate_part_result = SICKLocatePartsTask.locate_part_results.pop(
            idx)

        # approach move
        approach = {}
        approach['move_type'] = 'direct'
        pose_values = [float(self.locate_part_result['absoluteApproachToolFrame0X']), float(self.locate_part_result['absoluteApproachToolFrame0Y']),
                       float(self.locate_part_result['absoluteApproachToolFrame0Z']), float(
                           self.locate_part_result['absoluteApproachToolFrame0R1']),
                       float(self.locate_part_result['absoluteApproachToolFrame0R2']), float(self.locate_part_result['absoluteApproachToolFrame0R3'])]
        approach['pose'] = pose_values
        approach['speed'] = 1 * kwargs.get('speed_override')
        approach['smooothing'] = 100
        moves.append(approach)

        # pick move
        pick = {}
        pick['move_type'] = 'direct'
        pose_values = [float(self.locate_part_result['absoluteToolFrame0X']), float(self.locate_part_result['absoluteToolFrame0Y']),
                       float(self.locate_part_result['absoluteToolFrame0Z']), float(
                           self.locate_part_result['absoluteToolFrame0R1']),
                       float(self.locate_part_result['absoluteToolFrame0R2']), float(self.locate_part_result['absoluteToolFrame0R3'])]
        pick['pose'] = pose_values
        pick['speed'] = 1 * kwargs.get('speed_override')
        pick['collision_check'] = 'false'
        pick['interp'] = 1
        moves.append(pick)

        # clear move
        clear = {}
        clear['move_type'] = 'direct'
        pose_values = [float(self.locate_part_result['absoluteClearToolFrame0X']), float(self.locate_part_result['absoluteClearToolFrame0Y']),
                       float(self.locate_part_result['absoluteClearToolFrame0Z']), float(
                           self.locate_part_result['absoluteClearToolFrame0R1']),
                       float(self.locate_part_result['absoluteClearToolFrame0R2']), float(self.locate_part_result['absoluteClearToolFrame0R3'])]
        clear['pose'] = pose_values
        clear['speed'] = 1 * kwargs.get('speed_override')
        clear['collision_check'] = 'false'
        clear['interp'] = 1
        moves.append(clear)

        self.parameters['moves'] = moves
        combined_move_message.moves = moves

        return combined_move_message

    def stop_task(self):
        self.send_cancel_move(0.8)
        self.running = False

    def pause_task(self):
        self.send_cancel_move(0.5)
        self.running = False

    def send_cancel_move(self, acceleration_factor):
        self.logger.info(
            f'[{self.robot_name}] cancelling Move command at acceleration factor {acceleration_factor}')
        handler = self.appliance_client.send_msg(ASCIICancelMove(
            robot_name=self.robot_name, acceleration_factor=acceleration_factor))
        handler.close_handler()

    def apply_ui_to_task(self, execute_once):
        self.parameters['execute_once'] = execute_once

    def apply_task_to_ui(self):
        pass

    def copy(self):
        return SICKCombinedPick1(robot_name=self.robot_name, parameters=self.parameters, appliance_client=self.appliance_client, global_variables=self.global_variables, uuid=self.uuid)
