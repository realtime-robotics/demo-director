from lib.Tasks.Task import Task
from lib.sick_plb.messages.LocatePartMsg import LocatePartMsg
from lib.sick_plb.messages.PartResultResp import PartResultResp
from lib.sick_plb.sick_plb import SickPLBCamera
from lib.GlobalVariables import GlobalVariables
from PyQt5.QtWidgets import QCheckBox
from PyQt5 import QtWidgets
from PyQt5.QtCore import Qt
import socket
import time
from threading import Thread
import lib.Robot as Robot
from math import hypot


class SICKLocatePartsTask(LocatePartMsg, Task):
    RESULT_DESC = ['PartResultResp',
                   'result',
                   'binFillingRatio',
                   'jobAlias',
                   'cameraAlignmentAlias',
                   'numValidPickPoses',

                   'moreToAnalyze', 'highestPointX', 'highestPointY', 'highestPointZ',
                   'boundingBoxSizeX', 'boundingBoxSizeY',

                   'pickPoseAlias0', 'gripperAlias0',
                   'absoluteEntryToolFrame0X', 'absoluteEntryToolFrame0Y', 'absoluteEntryToolFrame0Z',
                   'absoluteEntryToolFrame0R1', 'absoluteEntryToolFrame0R2', 'absoluteEntryToolFrame0R3',
                   'absoluteApproachToolFrame0X', 'absoluteApproachToolFrame0Y', 'absoluteApproachToolFrame0Z',
                   'absoluteApproachToolFrame0R1', 'absoluteApproachToolFrame0R2', 'absoluteApproachToolFrame0R3',
                   'absoluteToolFrame0X', 'absoluteToolFrame0Y', 'absoluteToolFrame0Z',
                   'absoluteToolFrame0R1', 'absoluteToolFrame0R2', 'absoluteToolFrame0R3',
                   'absoluteClearToolFrame0X', 'absoluteClearToolFrame0Y', 'absoluteClearToolFrame0Z',
                   'absoluteClearToolFrame0R1', 'absoluteClearToolFrame0R2', 'absoluteClearToolFrame0R3',
                   'absoluteExitToolFrame0X', 'absoluteExitToolFrame0Y', 'absoluteExitToolFrame0Z',
                   'absoluteExitToolFrame0R1', 'absoluteExitToolFrame0R2', 'absoluteExitToolFrame0R3',
                   'relativeToolFrame0X', 'relativeToolFrame0Y', 'relativeToolFrame0Z',
                   'relativeToolFrame0R1', 'relativeToolFrame0R2', 'relativeToolFrame0R3']

    RESULTS_TO_ADD = ['absoluteApproachToolFrame0X', 'absoluteApproachToolFrame0Y', 'absoluteApproachToolFrame0Z',
                      'absoluteApproachToolFrame0R1', 'absoluteApproachToolFrame0R2', 'absoluteApproachToolFrame0R3',
                      'absoluteToolFrame0X', 'absoluteToolFrame0Y', 'absoluteToolFrame0Z',
                      'absoluteToolFrame0R1', 'absoluteToolFrame0R2', 'absoluteToolFrame0R3',
                      'absoluteClearToolFrame0X', 'absoluteClearToolFrame0Y', 'absoluteClearToolFrame0Z',
                      'absoluteClearToolFrame0R1', 'absoluteClearToolFrame0R2', 'absoluteClearToolFrame0R3',]
    PART_LIMIT = 10

    locate_part_results = []

    def __init__(self, robot_name, parameters={}, uuid: str = '', global_variables: GlobalVariables = None):
        self.uuid = uuid
        LocatePartMsg.__init__(self)
        Task.__init__(self, type='SICKLocateParts', robot_name=robot_name,
                      parameters=parameters, uuid=self.uuid)
        self.global_variables = global_variables
        self.running = False
        self.complete = False

    def asdict(self):
        return {"uuid": self.uuid, "type": self.type, "parameters": self.parameters}

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, item):
        return getattr(self, item)

    def load_ui(self):
        self.setupUi()
        self.ui.queue_checkbox = QCheckBox('Queue')
        self.ui.verticalLayout.addWidget(
            self.ui.queue_checkbox, alignment=Qt.AlignmentFlag.AlignRight)

    def apply_ui_to_task(self, execute_once):
        self.parameters['job_alias'] = self.ui.jobAlias.text()
        self.parameters['bin_alias'] = self.ui.binAlias.text()
        self.parameters['queue'] = self.ui.queue_checkbox.isChecked()
        self.parameters['execute_once'] = execute_once

    def apply_task_to_ui(self):
        self.ui.jobAlias.setText(str(self.parameters['job_alias']))
        self.ui.binAlias.setText(str(self.parameters['bin_alias']))
        self.ui.queue_checkbox.setChecked(self.parameters['queue'])

    def execute(self, **kwargs):
        self.running = True
        self.complete = False
        self.error = False
        if not SickPLBCamera.connection_data:
            self.logger.error('No SICK connection data')
            self.error = True
            raise Exception('No SICK connection data')
        self.socket = next(iter(SickPLBCamera.connection_data.values()))

        # if len(SICKLocatePartsTask.locate_part_results) > 0:
        #     self.logger.info('Parts still available, not locating parts...')
        #     self.complete = True
        #     self.running = False
        #     return

        Robot.Robot.no_parts_detected = False
        send_recv_all_thread = Thread(target=self.send_recv_all,
                                      name='LocatePartsRecv')
        send_recv_all_thread.start()
        if not self.parameters['queue']:
            Thread.join(send_recv_all_thread)

    def send_recv_all(self):
        # set up message
        send_msg = LocatePartMsg(
            jobAlias=self.parameters['job_alias'], binAlias=self.parameters['bin_alias'])

        # set up receive message
        recv_msg = PartResultResp()
        # self.clear_global_variables()
        self.more_to_analyze = True
        self.part_count = 0
        SICKLocatePartsTask.locate_part_results = []
        while (self.part_count < self.PART_LIMIT) and self.more_to_analyze:
            self.send_msg(send_msg)
            self.recv_msg(recv_msg)
            self.part_count += 1
        # check if there are no results
        if not SICKLocatePartsTask.locate_part_results:
            Robot.Robot.no_parts_detected = True
        else:
            # sort the list from closest -> farthest from RV8
            origin = [0, 0, 0, 0, 0, 0]
            if self.parameters['bin_alias'] == 'Camera1':
                origin = [-53.71, -407.104, 121.906, 0, 0, 0]
            if self.parameters['bin_alias'] == 'Camera2':
                origin = [-396.347, -50.328, 128.565, 0, 0, 0]
            SICKLocatePartsTask.locate_part_results = sorted(
                SICKLocatePartsTask.locate_part_results, key=lambda mag: hypot(float(mag['absoluteToolFrame0X']) - origin[0], float(mag['absoluteToolFrame0Y']) - origin[1]))
        self.complete = True
        self.running = False

    def send_msg(self, send_msg: LocatePartMsg):
        self.socket.sendall(send_msg.to_msg())

    def recv_msg(self, recv_msg: PartResultResp):
        recv_toc = time.time()
        recv_data = self.socket.recv(4096)
        recv_tic = time.time()
        self.logger.info(
            f'SICK PLB Received Message [{round(recv_tic-recv_toc,3)}s]: {recv_msg.get_resp_type()}')
        # parse the response
        resp = "".join(recv_data.decode().splitlines()).replace(" ", "")
        resp_list = resp.replace("\t", "").replace(";", "").split(',')
        # send the response to be validated
        if not recv_msg.validate_resp(resp_list):
            self.logger.error(f'Invalid Trigger response received: {resp}')
            raise Exception('Invalid Trigger response received')
        result = dict(zip(self.RESULT_DESC, resp_list))
        if result['result'] == '0':
            SICKLocatePartsTask.locate_part_results.append(result)
        else:
            self.logger.error(
                f'Result code not OK: {result["result"]}: {SickPLBCamera.error_codes[int(result["result"])]}')
            if (int(result["result"]) != 111) and (int(result["result"]) != 114) and (int(result["result"]) != 113):
                self.error = True
            self.more_to_analyze = False
        if result['moreToAnalyze'] == '0':
            self.more_to_analyze = False

    def add_result_to_global_variables(self, result: dict):
        for idx in range(len(self.RESULTS_TO_ADD)):
            self.global_variables.add_variable(
                name=f'Part{self.part_count}_{self.RESULTS_TO_ADD[idx]}', value=result[self.RESULTS_TO_ADD[idx]])
        self.logger.info(
            f'Added Part{self.part_count} data in global variables')

    def clear_global_variables(self):
        for num in range(self.PART_LIMIT):
            for idx in range(len(self.RESULTS_TO_ADD)):
                self.global_variables.add_variable(
                    name=f'Part{num}_{self.RESULTS_TO_ADD[idx]}', value='')
            self.logger.info(
                f'Cleared Part{num} data in global variables')

    def copy(self):
        return SICKLocatePartsTask(robot_name=self.robot_name, parameters=self.parameters, uuid=self.uuid, global_variables=self.global_variables)
