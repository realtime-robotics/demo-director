# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './lib/sick_plb/ui/SetStateRecv.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_SetStateRecv(object):
    def setupUi(self, SetStateRecv):
        SetStateRecv.setObjectName("SetStateRecv")
        SetStateRecv.resize(312, 114)
        self.gridLayout = QtWidgets.QGridLayout(SetStateRecv)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout_6 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_6.setObjectName("horizontalLayout_6")
        self.image_id_label = QtWidgets.QLabel(SetStateRecv)
        self.image_id_label.setMinimumSize(QtCore.QSize(140, 0))
        self.image_id_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.image_id_label.setObjectName("image_id_label")
        self.horizontalLayout_6.addWidget(self.image_id_label)
        self.state = QtWidgets.QLabel(SetStateRecv)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.state.sizePolicy().hasHeightForWidth())
        self.state.setSizePolicy(sizePolicy)
        self.state.setMinimumSize(QtCore.QSize(100, 20))
        self.state.setStyleSheet("background-color: rgb(220, 220, 220);")
        self.state.setFrameShape(QtWidgets.QFrame.Box)
        self.state.setText("")
        self.state.setObjectName("state")
        self.horizontalLayout_6.addWidget(self.state)
        self.gridLayout.addLayout(self.horizontalLayout_6, 1, 1, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem, 2, 1, 1, 1)
        self.horizontalLayout_4 = QtWidgets.QHBoxLayout()
        self.horizontalLayout_4.setObjectName("horizontalLayout_4")
        self.result_label = QtWidgets.QLabel(SetStateRecv)
        self.result_label.setMinimumSize(QtCore.QSize(140, 0))
        self.result_label.setAlignment(QtCore.Qt.AlignRight|QtCore.Qt.AlignTrailing|QtCore.Qt.AlignVCenter)
        self.result_label.setObjectName("result_label")
        self.horizontalLayout_4.addWidget(self.result_label)
        self.result = QtWidgets.QLabel(SetStateRecv)
        sizePolicy = QtWidgets.QSizePolicy(QtWidgets.QSizePolicy.Preferred, QtWidgets.QSizePolicy.Fixed)
        sizePolicy.setHorizontalStretch(0)
        sizePolicy.setVerticalStretch(0)
        sizePolicy.setHeightForWidth(self.result.sizePolicy().hasHeightForWidth())
        self.result.setSizePolicy(sizePolicy)
        self.result.setMinimumSize(QtCore.QSize(100, 20))
        self.result.setStyleSheet("background-color: rgb(220, 220, 220);")
        self.result.setFrameShape(QtWidgets.QFrame.Box)
        self.result.setText("")
        self.result.setObjectName("result")
        self.horizontalLayout_4.addWidget(self.result)
        self.gridLayout.addLayout(self.horizontalLayout_4, 0, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(40, 20, QtWidgets.QSizePolicy.Expanding, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem1, 1, 0, 1, 1)

        self.retranslateUi(SetStateRecv)
        QtCore.QMetaObject.connectSlotsByName(SetStateRecv)

    def retranslateUi(self, SetStateRecv):
        _translate = QtCore.QCoreApplication.translate
        SetStateRecv.setWindowTitle(_translate("SetStateRecv", "Form"))
        self.image_id_label.setText(_translate("SetStateRecv", "state:"))
        self.result_label.setText(_translate("SetStateRecv", "result:"))
