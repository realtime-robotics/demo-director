# -*- coding: utf-8 -*-

# Form implementation generated from reading ui file './lib/sick_plb/ui/BinLocateSend.ui'
#
# Created by: PyQt5 UI code generator 5.15.6
#
# WARNING: Any manual changes made to this file will be lost when pyuic5 is
# run again.  Do not edit this file unless you know what you are doing.


from PyQt5 import QtCore, QtGui, QtWidgets


class Ui_BinLocateSend(object):
    def setupUi(self, BinLocateSend):
        BinLocateSend.setObjectName("BinLocateSend")
        BinLocateSend.resize(374, 132)
        self.gridLayout = QtWidgets.QGridLayout(BinLocateSend)
        self.gridLayout.setObjectName("gridLayout")
        self.horizontalLayout = QtWidgets.QHBoxLayout()
        self.horizontalLayout.setObjectName("horizontalLayout")
        self.jobAlias_label = QtWidgets.QLabel(BinLocateSend)
        self.jobAlias_label.setObjectName("jobAlias_label")
        self.horizontalLayout.addWidget(self.jobAlias_label)
        self.jobAlias = QtWidgets.QLineEdit(BinLocateSend)
        self.jobAlias.setMinimumSize(QtCore.QSize(60, 0))
        self.jobAlias.setObjectName("jobAlias")
        self.horizontalLayout.addWidget(self.jobAlias)
        self.gridLayout.addLayout(self.horizontalLayout, 0, 0, 1, 1)
        spacerItem = QtWidgets.QSpacerItem(10, 20, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Minimum)
        self.gridLayout.addItem(spacerItem, 1, 1, 1, 1)
        spacerItem1 = QtWidgets.QSpacerItem(20, 40, QtWidgets.QSizePolicy.Minimum, QtWidgets.QSizePolicy.Expanding)
        self.gridLayout.addItem(spacerItem1, 2, 0, 1, 1)

        self.retranslateUi(BinLocateSend)
        QtCore.QMetaObject.connectSlotsByName(BinLocateSend)

    def retranslateUi(self, BinLocateSend):
        _translate = QtCore.QCoreApplication.translate
        BinLocateSend.setWindowTitle(_translate("BinLocateSend", "Form"))
        self.jobAlias_label.setText(_translate("BinLocateSend", "jobAlias;"))
