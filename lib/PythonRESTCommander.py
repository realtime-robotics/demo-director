#!/usr/bin/env python3

from lib.Utils import initialize_logger
import requests

class ApiError(Exception):
    """An API Error Exception"""
    def __init__(self, status):
        self.status = status
    def __str__(self):
        # return "APIError: status={}".format(self.status)
        return f'APIError: status={self.status}'

class PythonRESTCommander():

    def __init__(self,ip_adr):
        self.ip_adr = ip_adr
        self.logger = initialize_logger()

    def send_get_request(self,extension):
        '''
        This function sends a get request to the passed URL extension and returns the response in JSON form

        Parameters:
            extension (str): the suffix, after http://<ip_address>, of the URL
        
        Returns:
            resp.json: The REST API's response in JSON form
        '''
        url = f'http://{self.ip_adr}{extension}'
        resp = requests.get(url)

        if resp.status_code != 200:
            # This means something went wrong.
            raise ApiError(f'GET {url} {resp.status_code}')
        
        return resp.json()

    def get_state(self):
        '''
        This function gets the state of the RTR runtime controller.
            
        Returns:
            state (string): The state of the Realtime Controller    
        '''
        state_url = '/api/appliance/state'
        state = self.send_get_request(state_url)
        return state['state']

    def get_rapidplan_version(self):
        '''
        This function returns the current RapidPlan version.
            
        Returns:
            version (string): The semantic version (X.X.X) returned by the appliance.    
        '''
        version_url = '/api/version'
        rapidplan_version = self.send_get_request(version_url)
        return rapidplan_version['version']

    def get_installed_project_names(self):
        '''
        This function returns a list of the Projects installed on the Realtime Controller
            
        Returns:
            project_list (list of strings): A list of strings where each string is the name of a project installed on the Controller    
        '''
        project_names = '/api/projects/names'
        project_list = self.send_get_request(project_names)
        return project_list

    def get_loaded_project(self):
        '''
        This function returns the name of the loaded project or None.
            
        Returns:
            project_name (string): A string indicating the name of the loaded project, or None if none is loaded.    
        '''
        load_state = '/api/projects/load_state'
        load_state_response = self.send_get_request(load_state)
        if load_state_response['project'] != "":
            return load_state_response['project']
        return None

    def get_robot_names(self,project_name):
        '''
        This function returns a list of the robots in a given project.
            
        Returns:
            robot_list (list of strings): A list of strings where each string is the name of a robot in the specified project.    
        '''
        robot_names = f'/api/projects/{project_name}/robot_names'
        robot_list = self.send_get_request(robot_names)
        return robot_list['robots']

    def get_robot_presets(self,project_name,robot_name):
        '''
        This function returns a list of the targets in the robot's current preset.
            
        Returns:
            target_list (list of strings): A list of strings where each string is the name of a target in the robot's current preset.   
        '''
        robot_info = f'/api/projects/{project_name}/robots/{robot_name}/'
        robot_info_resp = self.send_get_request(robot_info)
        preset_list = []
        for preset in robot_info_resp['robot_presets']:
            name = preset['name']
            preset_list.append(name)
        
        return preset_list
