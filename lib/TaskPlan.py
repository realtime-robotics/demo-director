#!/usr/bin/env python3

from lib.InterlockManager import InterlockManager
from lib.GlobalVariables import GlobalVariables
from ui.task_plan_tab import Ui_TaskPlanTab
from lib.Utils import initialize_logger
from PyQt5.QtWidgets import QWidget
from PyQt5.QtCore import pyqtSignal
from typing import List, Dict
from threading import Thread
from lib.Robot import Robot
import uuid

class TaskPlan(QWidget):
    task_plan_done_signal = pyqtSignal(str)
    title_changed_signal = pyqtSignal(str)
    description_changed_signal = pyqtSignal(str)
    unsaved_changes = pyqtSignal(bool)
    # Use this signal to enable/disable play/pause buttons
    running_state = pyqtSignal(bool, str)
    switch_task_plans_request_sig = pyqtSignal(str)
    stop_task_plans_request_sig = pyqtSignal(str)

    def __init__(self,
                 title='New Task Plan',
                 description='',
                 loop=False,
                 sync_loop_start_time=False,
                 ip_address: str = '',
                 robots: List[Robot] = [],
                 interlock_manager: InterlockManager = InterlockManager(),
                 global_variables: GlobalVariables = None):
        super().__init__()
        self.global_variables = global_variables
        self.loop = loop
        self.ip_address = ip_address
        # needs to be a dictionary so we can associate which robot is running in the thread
        self.threads: Dict[str, Thread] = {}
        # need to append any executes to this array so we don't get errors on closin for unstarted threads
        self.execute_threads: List[Thread] = []
        self.pause = False
        self.stop = False
        self.title = title
        self.uuid = uuid.uuid4()
        self.logger = initialize_logger()
        self.description = description
        self.interlock_manager = interlock_manager
        self.sync_loop_start_time = sync_loop_start_time
        self.ui = Ui_TaskPlanTab()
        self.ui.setupUi(self)

        # Initialize empty list of robots and add all robots passed in
        self.robots: List[Robot] = []
        for robot in robots:
            self.add_robot(robot)
        self.connect_robot_signals()

        self.ui.loop_task_plan_checkbox.stateChanged.connect(self.loop_task_plan_update)
        self.ui.sync_loop_start_checkbox.stateChanged.connect(self.sync_loop_start_time_update)
        if loop:
            self.ui.loop_task_plan_checkbox.setChecked(True)
        if sync_loop_start_time:
            self.ui.sync_loop_start_checkbox.setChecked(True)

        self.sync_loop_start_time_update()
        self.loop_task_plan_update()

        self.running = False
        self.done_count = 0
        self.sync_loop_start_done_count = 0
        self.ui.task_plan_title_lineedit.setText(self.title)
        self.ui.task_plan_description_lineedit.setText(self.description)
        self.ui.task_plan_title_lineedit.textChanged.connect(self.update_title)
        self.ui.task_plan_description_lineedit.textChanged.connect(self.update_description)
        self.ui.robots_tab_widget.currentChanged.connect(self.robot_tab_changed)
        self.ui.play_task_plan.clicked.connect(self.execute)
        self.ui.pause_task_plan.clicked.connect(self.pause_robots)
        self.ui.stop_task_plan.clicked.connect(self.stop_robots)

    def asdict(self):
        task_plan_dict = {}
        task_plan_dict.update({"title": self.title})
        task_plan_dict.update({"description": self.description})
        task_plan_dict.update({"loop": int(self.loop)})
        task_plan_dict.update(
            {"sync_loop_start_time": int(self.sync_loop_start_time)})
        task_plan_dict.update({"interlocks": self.interlock_manager.asdict()})
        task_plan_dict.update({"robots": self.robots})
        return task_plan_dict

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, item):
        return getattr(self, item)

    def loop_task_plan_update(self):
        value = self.ui.loop_task_plan_checkbox.isChecked()
        self.logger.info(f'Setting loop start time to: {value}')

        if value:
            self.loop = True
            self.ui.sync_loop_start_checkbox.setEnabled(True)
        else:
            self.loop = False
            self.ui.sync_loop_start_checkbox.setChecked(False)
            self.ui.sync_loop_start_checkbox.setEnabled(False)

        for robot in self.robots:
            robot.loop = self.loop
        self.unsaved_changes.emit(True)

    def sync_loop_start_time_update(self):
        value = self.ui.sync_loop_start_checkbox.isChecked()
        self.logger.info(f'Setting sync loop start time to: {value}')
        self.sync_loop_start_time = False
        if value:
            self.sync_loop_start_time = True
        for robot in self.robots:
            robot.sync_loop_start_time = self.sync_loop_start_time
        self.unsaved_changes.emit(True)

    def validate_title_description(self, text):
        invalid_characters = ["'", '"']
        for character in invalid_characters:
            if character in text:
                text = text.replace(character, "")
                self.logger.info(
                    f'Invalid character [{character}] found and removed.')
        return text

    def update_title(self):
        text = self.ui.task_plan_title_lineedit.text()
        validated_text = self.validate_title_description(text)
        if text != validated_text:
            cursor_pose = self.ui.task_plan_title_lineedit.cursorPosition()
            self.ui.task_plan_title_lineedit.setText(validated_text)
            self.ui.task_plan_title_lineedit.setCursorPosition(cursor_pose - 1)
        self.title = validated_text
        self.title_changed_signal.emit(validated_text)

    def update_description(self):
        text = self.ui.task_plan_description_lineedit.text()
        validated_text = self.validate_title_description(text)
        if text != validated_text:
            cursor_pose = self.ui.task_plan_description_lineedit.cursorPosition()
            self.ui.task_plan_description_lineedit.setText(validated_text)
            self.ui.task_plan_description_lineedit.setCursorPosition(
                cursor_pose - 1)
        self.description = validated_text
        self.description_changed_signal.emit(validated_text)

    def robot_tab_changed(self):
        for robot in self.robots:
            robot.interlock_task_editor.refresh_interlock_id_combobox()

    def pause_robots(self):
        self.pause = True
        # command pause on all robots
        for robot in self.robots:
            robot.pause_tasklist()
        # wait until robot threads stop
        for thread in self.threads.values():
            thread.join()
        # wait until task plan threads stop
        for thread in self.execute_threads:
            thread.join()
        self.pause = False

    def stop_robots(self):
        self.logger.debug(f'Stopping robots from task plan [{self.title}]')
        self.stop = True
        # command stop on all robots
        for robot in self.robots:
            robot.stop_tasklist(requester=self.title)
        # wait until robot threads stop
        for thread in self.threads.values():
            thread.join()
        self.logger.debug(
            f'Robot threads have stopped on task plan [{self.title}]')
        # wait until task plan threads stop
        for thread in self.execute_threads:
            thread.join()
        self.logger.debug(
            f'Task plan threads have stopped on task plan [{self.title}]')
        # reset loop counts in robot threads
        for robot in self.robots:
            robot.loop_count = 0
        self.set_running_state(False)

    def connect_robot_signals(self):
        # Since robots are not added in the constructor of the task plan, there
        # needs to be a method to connect robot signals to the task plan methods
        # after they have been added
        for robot in self.robots:
            robot.unsaved_changes.connect(self.relay_unsaved_changes)
            robot.done_signal.connect(self.robot_task_list_done)
            robot.switch_task_plans_request.connect(self.switch_task_plans_request)
            robot.stop_task_plans_request.connect(self.stop_task_plans_request)
            robot.running_state.connect(self.set_running_state)
        
    def switch_task_plans_request(self):
        self.stop = True
        self.logger.info(
            f'Switch task plans request from [{self.title}] emitted')
        self.switch_task_plans_request_sig.emit(self.title)

    def stop_task_plans_request(self):
        self.stop = True
        self.logger.info(f'Stop task plans request from [{self.title}] emitted')
        self.stop_task_plans_request_sig.emit(self.title)

    def robot_task_list_done(self, name):
        # Only restart the robot's task list if the 'loop' checkbox is checked
        # and the 'sync loop start time' checkbox is NOT checked. Syncing a loop
        # start time is handled elsewhere (currently in the execute_in_thread method)
        if not self.loop or self.sync_loop_start_time or self.stop or self.pause:
            return

        # Find the corresponding robot in the robots list and restart it's task list
        # TODO: there must be a better way than looping over all robots. Maybe instead of a string being
        #       emitted over the signal, the robot class can be emitted that way we can just index into the list
        for robot in self.robots:
            if robot.robot_name == name:
                self.logger.info(f"Restarting robot [{name}]'s tasklist.")
                # if robot's thread is not alive, kill it
                if robot.robot_name in self.threads:
                    # need to delete the thread if it isn't running, python doesn't allow restart
                    if not self.threads[robot.robot_name].is_alive():
                        self.threads.pop(robot.robot_name)
                # otherwise, start a thread for the robot
                self.threads[robot.robot_name] = Thread(target=robot.execute_task_list, name=robot.robot_name)
                self.threads[robot.robot_name].start()
                break

    # Only call this from execute function
    def execute_in_thread(self):
        self.set_running_state(True)
        # clear out any old robot threads if they exist
        for robot in self.robots:
            if robot.robot_name in self.threads:
                self.threads.pop(robot.robot_name)

        # start robots
        for robot in self.robots:
            Robot.no_move_available_3 = True
            # skip if robot task list is empty
            if not robot.ui.task_list_widget.count():
                continue
            # if robot's thread is not alive, kill it
            if robot.robot_name in self.threads:
                # need to delete the thread if it isn't running, python doesn't allow restart
                if not self.threads[robot.robot_name].is_alive():
                    self.threads.pop(robot.robot_name)
            # otherwise, start a thread for the robot
            else:
                self.threads[robot.robot_name] = Thread(
                    target=robot.execute_task_list, name=robot.robot_name)
                self.threads[robot.robot_name].start()

        # If the sync start times checkbox is checked, wait until all threads finish and then call
        # execute again to restart all robots simultaneously
        if self.sync_loop_start_time:
            for thread in self.threads.values():
                thread.join()
            self.logger.info(
                f"Task Plan [{self.title}] start time is now in sync.")
            if not (self.stop or self.pause):
                self.execute()
                return

        self.set_running_state(False)

    def execute(self):
        for thread in self.threads.values():
            if thread.is_alive():
                self.logger.warn(
                    f"Active task plan thread detected, so task plan [{self.title}] will not execute!")
                return
        for robot in self.robots:
            if robot.running:
                self.logger.warn(f"Robot [{robot.robot_name}] is already running, so task plan [{self.title}] will not execute!")
                return

        # Reset the stop and pause flags
        self.stop = False
        self.pause = False

        # Start a thread so we can handle looping/syncing of robot threads in this execute
        self.logger.info(f"**** Executing task plan [{self.title}]. ****")
        self.execute_threads.append(
            Thread(target=self.execute_in_thread, name='task_plan'))
        self.execute_threads[-1].start()

    def relay_unsaved_changes(self, is_unsaved):
        self.unsaved_changes.emit(is_unsaved)

    def set_running_state(self, state: bool):
        self.running = state
        self.running_state.emit(state, str(self.uuid))

    def add_robot(self, robot: Robot = None):
        if robot is None:
            robot = Robot(f'Robot {len(self.robots) + 1}',
                          self.ip_address, global_variables=self.global_variables)
        robot.interlock_manager = self.interlock_manager
        robot.update_interlock_tasks()
        robot.global_variables = self.global_variables
        robot.get_tasks()
        robot.refresh_task_display()
        self.logger.info(
            f"Adding robot [{robot.robot_name}] to task plan [{self.title}].")
        self.robots.append(robot)
        self.ui.robots_tab_widget.addTab(robot, robot.robot_name)

    def clone(self):
        new_robots: List[Robot] = []
        new_interlock_manager = self.interlock_manager.clone(
        )
        for robot in self.robots:
            new_robots.append(robot.clone())
        return TaskPlan(title=self.title, description=self.description, loop=self.loop,
                        sync_loop_start_time=self.sync_loop_start_time, ip_address=self.ip_address, robots=new_robots, interlock_manager=new_interlock_manager)

    def is_stopped(self):
        return self.stop
