from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QWidget, QLabel, QDialog, QVBoxLayout, QProgressBar, QAction
from PyQt5.QtWidgets import QWidget, QFileDialog, QMessageBox, QApplication
from ui.LogReplayWindow import Ui_LogReplayWindow
from lib.LogManager import LogManager
from lib.Utils import FilePaths, initialize_logger, AboutDialog, set_logging_level
from PyQt5.QtGui import QIcon, QFont, QFontDatabase, QKeyEvent, QPixmap
from PyQt5 import QtWidgets, QtCore
from typing import Optional
import sys, os, argparse
import logging, json
import faulthandler


class LogReplayUtility(QMainWindow):
    '''
    This class creates the window
    Parameters:
            screen_resolution (QRect): The screen resolution of the monitor the application is launched in
    '''
    def __init__(self):
        #Start Logger
        self.logger = initialize_logger()
        self.logger.info(
            f'Launching Log Replay Utility')
            
        faulthandler.enable()
        super().__init__()
        self.ui = Ui_LogReplayWindow()
        self.ui.setupUi(self)
        
        self.file_paths = FilePaths()

        # Load the local fonts to application
        self.font_database = QFontDatabase()
        for filename in os.listdir(self.file_paths.font_path):
            f = os.path.join(self.file_paths.font_path, filename)
            if os.path.isfile(f):
                self.font_database.addApplicationFont(f)
        self.font = QFont("Open Sans")
        self.setFont(self.font)

        # Add a title and task bar icon
        self.window_name: str = "Log Replay Manager"
        self.setWindowTitle(self.window_name)


         # Create the two widgets displayed to the user and add them to the stacked layout   
        self.stackedWidget = QStackedWidget()
        self.log_manager = LogManager()
        self.stackedWidget.addWidget(self.log_manager)
        self.setCentralWidget(self.stackedWidget)

        # Connect menubar actions to appropriate methods
        self.ui.action_load.triggered.connect(self.log_manager.get_log)
        self.ui.action_close_application.triggered.connect(self.shutdown)

    def shutdown(self):
        self.close()

