#!/usr/bin/env python3

from lib.appliance_client.messages.ASCIIGetRobotPresets import ASCIIGetRobotPresets
from lib.Tasks.RS_FindSceneDeviationsTask import RS_FindSceneDeviationsTask
from lib.appliance_client.messages.ASCIIGetTargets import ASCIIGetTargets
from lib.Tasks.RS_GetRapidSenseStatusTask import GetRapidSenseStatusTask
from lib.Tasks.RS_AddExpectedObjectTask import RS_AddExpectedObjectTask
from lib.sick_plb.Tasks.SICKLocatePartsTask import SICKLocatePartsTask
from lib.sick_plb.Tasks.SICKMoveToDropBin2 import SICKMoveToDropBin2
from lib.sick_plb.Tasks.SICKMoveToDropBin1 import SICKMoveToDropBin1
from lib.sick_plb.Tasks.SICKCombinedPick1 import SICKCombinedPick1
from lib.sick_plb.Tasks.SICKCombinedPick2 import SICKCombinedPick2
from lib.sick_plb.Tasks.SICKCombinedPick3 import SICKCombinedPick3
from PyQt5.QtWidgets import QWidget, QListWidgetItem, QMessageBox
from lib.Tasks.AlternateLocationTask import AlternateLocationTask
from lib.Tasks.InterruptBehaviorTask import InterruptBehaviorTask
from lib.appliance_client.appliance_client import ApplianceClient
from lib.sick_plb.Tasks.SICKTriggerTask import SICKTriggerTask
from lib.Tasks.DSMRemoveBoxesTask import DSMRemoveBoxesTask
from lib.Tasks.RS_FindHeightTask import RS_FindHeightTask
from lib.Tasks.RS_ClearSceneTask import RS_ClearSceneTask
from lib.Tasks.WaitOnStatusTask import WaitOnStatusTask
from lib.Utils import initialize_logger, MessageDialog
from lib.Tasks.ObjectStateTask import ObjectStateTask
from lib.Tasks.RobotPresetTask import RobotPresetTask
from lib.Tasks.CancelMoveTask import CancelMoveTask
from lib.Tasks.ClearFaultsTask import ClearFaultsTask
from lib.CycleTimeDialog import CycleDetailsDialog
from lib.Tasks.Robotiq2F85Task import Robotiq2F85
from lib.Tasks.InterlockTask import InterlockTask
from lib.Tasks.DSMAddBoxTask import DSMAddBoxTask
from lib.Tasks.ReparentTask import ReparentTask
from lib.InterlockManager import InterlockManager
from lib.GlobalVariables import GlobalVariables
from lib.Tasks.OnRobotRG2Task import OnRobotRG2
from lib.Tasks.DigitalIOTask import DigitalIO
from lib.Tasks.ScanSceneTask import ScanScene
from lib.Tasks.UserLogTask import UserLogTask
from lib.Tasks.TaskEditor import TaskEditor
from PyQt5.QtGui import QKeyEvent, QColor
from lib.Tasks.WaitTask import WaitTask
from lib.Tasks.MoveTask import MoveTask
from PyQt5.QtCore import pyqtSignal, Qt
from ui.robot_tab import Ui_RobotTab
from PyQt5.QtGui import QKeyEvent
from lib.Tasks.Task import Task
from threading import Thread
from typing import Any, Dict
import time, uuid

class TaskColors():
    SUCCESS = QColor('#acffa8')
    ERROR = QColor('#ffa8a8')
    RUNNING = QColor('#a7cbf2')
    IDLE = QColor('#ffffff')

class Robot(QWidget):
    done_signal = pyqtSignal(str)
    sync_loop_start_signal = pyqtSignal(str)
    task_list_index_signal = pyqtSignal(int)
    pause_signal = pyqtSignal()
    reset_signal = pyqtSignal()
    unsaved_changes = pyqtSignal(bool)
    key_pressed = pyqtSignal(QKeyEvent)
    running_state = pyqtSignal(bool, str)
    switch_task_plans_request = pyqtSignal()
    stop_task_plans_request = pyqtSignal()

    # pack expo flags
    no_parts_detected = False
    no_move_available_1 = False
    no_move_available_2 = False
    no_move_available_3 = False

    def __init__(self,
                 robot_name: str = 'New Robot',
                 ip_address: str = 'localhost:80',
                 global_variables: GlobalVariables = None,
                 ):

        super().__init__()
        self.logger = initialize_logger()
        self.global_variables = global_variables
        # Setup the UI
        self.ui = Ui_RobotTab()
        self.ui.setupUi(self)
        self.robot_name = robot_name
        self.uuid = uuid.uuid4()
        self.ip_address = ip_address
        self.ip = ''
        self.port = ''
        self.ip, self.port = self.ip_address.split(':') if \
            ":" in self.ip_address else \
            (self.ip_address, '9999')
        self.appliance_client = ApplianceClient(
            self.ip, 9999, client_pool_size=1)
        self.appliance_client.start()
        self.loop: bool = True
        self.loop_count = 0
        self.sync_loop_start_time: bool = False
        self.interlock_manager: InterlockManager = InterlockManager()
        self.presets_targets_dict = self.get_presets_targets_dict()
        self.task_list_thread = Thread
        self.cycle_details_dialog = CycleDetailsDialog(self.robot_name,parent=self)
        
        # Available plugins: 'sick_plb', 'moxa', 'onrobot', 'robotiq'
        self.plugin_names = ['onrobot','robotiq']

        # Task execution flags
        self.idx = 0  # index of current running task in the task list
        self.running = False
        self.pause = False
        self.stop = False
        self.loop = False
        self.current_task: Any = Task()
        self.speed_override = 1.0

        # Cut, copy, paste memory
        self.copied_task = None

        # Connect UI
        self.ui.set_list_index_pushbutton.clicked.connect(self.set_list_index)
        self.ui.add_to_task_list_button.clicked.connect(self.ui_add_task)
        self.ui.task_type_combobox.currentIndexChanged.connect(self.refresh_task_display)
        self.ui.task_list_up_button.clicked.connect(self.move_task_up)
        self.ui.task_list_top_button.clicked.connect(self.move_task_to_top)
        self.ui.task_list_down_button.clicked.connect(self.move_task_down)
        self.ui.task_list_bottom_button.clicked.connect(self.move_task_to_bottom)
        self.ui.task_list_edit_button.clicked.connect(self.edit_task)
        self.ui.task_list_duplicate_button.clicked.connect(self.duplicate_task)
        self.ui.task_list_delete_button.clicked.connect(self.delete_task)
        self.ui.clear_task_list_button.clicked.connect(self.clear_task_list)
        self.ui.unselect_task_list_button.clicked.connect(self.unselect_task_widget)
        self.ui.execute_task_button.clicked.connect(self.task_editor_execute)
        self.task_list_index_signal.connect(self.update_task_list_index)
        self.ui.task_list_widget.doubleClicked.connect(self.edit_task)
        self.ui.play_button.clicked.connect(self.play_tasklist)
        self.ui.pause_button.clicked.connect(self.pause_tasklist)
        self.ui.step_forward_button.clicked.connect(self.step_forward)
        self.ui.step_back_button.clicked.connect(self.step_backward)
        self.ui.details_button.clicked.connect(self.open_details)

    def asdict(self):
        # This has to be put in a try/catch because for some reason the task_list_widget does not exist when
        # __init__ is called which seems to call asdict
        try:
            # loop over task_list_widget and create a list of tasks like we had before
            task_list = []
            count = self.ui.task_list_widget.count()
            for idx in range(count):
                item = self.ui.task_list_widget.item(idx)
                task = self.ui.task_list_widget.itemWidget(item)
                task_list.append(task)
            return {"robot_name": self.robot_name, "task_list": task_list}
        except AttributeError:
            self.logger.info(
                f'Failed to convert {self.robot_name} to dictionary.')

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())

    def __getitem__(self, item):
        return getattr(self, item)

    def refresh_task_display(self):
        for i in reversed(range(self.ui.task_layout.count())):
            self.ui.task_layout.itemAt(i).widget().setParent(None)

        task_selected = self.ui.task_type_combobox.currentText()
        self.logger.info(f'Displaying task widget [{task_selected}] for robot [{self.robot_name}]')

        if task_selected == 'Move':
            self.ui.task_layout.addWidget(self.move_task_editor)
        elif task_selected == 'Wait':
            self.ui.task_layout.addWidget(self.wait_task_editor)
        elif task_selected == "SetAlternateLocation":
            self.ui.task_layout.addWidget(self.alternate_location_task_editor)
        elif task_selected == "SetInterruptBehavior":
            self.ui.task_layout.addWidget(self.interrupt_behavior_task_editor)
        elif task_selected == 'SetObjectState':
            self.ui.task_layout.addWidget(self.object_state_task_editor)
        elif task_selected == 'SetRobotPreset':
            self.ui.task_layout.addWidget(self.robot_preset_task_editor)
        elif task_selected == "CancelMove":
            self.ui.task_layout.addWidget(self.cancel_move_task_editor)
        elif task_selected == "ClearFaults":
            self.ui.task_layout.addWidget(self.clear_faults_task_editor)
        elif task_selected == "Interlock":
            self.interlock_task_editor.refresh_interlock_id_combobox()
            self.ui.task_layout.addWidget(self.interlock_task_editor)
        elif task_selected == "WaitOnStatus":
            self.ui.task_layout.addWidget(self.wait_on_status_task_editor)
        elif task_selected == 'AddBox':
            self.ui.task_layout.addWidget(self.dsm_add_box_task_editor)
        elif task_selected == 'RemoveBoxes':
            self.ui.task_layout.addWidget(self.dsm_remove_boxes_task_editor)
        elif task_selected == 'Reparent':
            self.ui.task_layout.addWidget(self.reparent_task_editor)
        elif task_selected == 'ScanScene':
            self.ui.task_layout.addWidget(self.scan_scene_task_editor)
        elif task_selected == 'RS_ClearScene':
            self.ui.task_layout.addWidget(self.rs_clear_scene_task_editor)
        elif task_selected == 'RS_FindSceneDeviations':
            self.ui.task_layout.addWidget(self.rs_find_scene_deviations_task_editor)
        elif task_selected == 'RS_AddExpectedObject':
            self.ui.task_layout.addWidget(self.rs_add_expected_object_task_editor)
        elif task_selected == 'GetRapidSenseStatus':
            self.ui.task_layout.addWidget(self.get_rapidsense_status_task_editor)
        elif task_selected == 'RS_FindHeight':
            self.ui.task_layout.addWidget(self.height_check_task_editor)
        elif task_selected == 'UserLog':
            self.ui.task_layout.addWidget(self.user_log_task_editor)

        # Gripper plugins
        elif task_selected == "Robotiq 2F85":
            self.ui.task_layout.addWidget(self.robotiq_2f85_task_editor)
        elif task_selected == "OnRobot RG2":
            self.ui.task_layout.addWidget(self.onrobot_rg2_task_editor)
        
        # Digital IO plugin
        elif task_selected == "Digital I/O":
            self.digitalio_task_editor.refresh_digitalio_id_combobox()
            self.ui.task_layout.addWidget(self.digitalio_task_editor)

        # Sick PLB plugin
        elif task_selected == "SICKTrigger":
            self.ui.task_layout.addWidget(self.sick_trigger_task_editor)
        elif task_selected == "SICKLocateParts":
            self.ui.task_layout.addWidget(self.sick_locate_parts_editor)
        elif task_selected == "SICKCombinedPick1":
            self.ui.task_layout.addWidget(self.sick_combined_pick1_editor)
        elif task_selected == "SICKCombinedPick2":
            self.ui.task_layout.addWidget(self.sick_combined_pick2_editor)
        elif task_selected == "SICKCombinedPick3":
            self.ui.task_layout.addWidget(self.sick_combined_pick3_editor)
        elif task_selected == "SICKMoveToDropBin1":
            self.ui.task_layout.addWidget(self.sick_move_to_drop1_task_editor)
        elif task_selected == "SICKMoveToDropBin2":
            self.ui.task_layout.addWidget(self.sick_move_to_drop2_task_editor)

    def get_tasks(self):
        # Create an editor GUI for each task and add all the items to the combobox
        # Move task widget
        self.move_task_editor = MoveTask(self.robot_name, {}, self.appliance_client, self.presets_targets_dict, self.global_variables)
        self.move_task_editor.load_ui()
        # Wait task widget
        self.wait_task_editor = WaitTask(self.robot_name, {})
        self.wait_task_editor.load_ui()
        # Alternate location task widget
        self.alternate_location_task_editor = AlternateLocationTask(
            self.robot_name, {}, self.appliance_client, self.presets_targets_dict)
        self.alternate_location_task_editor.load_ui()
        # SetObjectState task widget
        self.object_state_task_editor = ObjectStateTask(
            self.robot_name, {}, self.appliance_client)
        self.object_state_task_editor.load_ui()
        # Set Robot Preset task widget
        self.robot_preset_task_editor = RobotPresetTask(
            self.robot_name, {}, self.appliance_client, self.presets_targets_dict)
        self.robot_preset_task_editor.load_ui()
        # CancelMove task widget
        self.cancel_move_task_editor = CancelMoveTask(
            self.robot_name, {}, self.appliance_client)
        self.cancel_move_task_editor.load_ui()
        # ClearFaults task widget
        self.clear_faults_task_editor = ClearFaultsTask(
            self.robot_name, {}, self.appliance_client)
        self.clear_faults_task_editor.load_ui()
        # InterruptBehavior task widget
        self.interrupt_behavior_task_editor = InterruptBehaviorTask(
            self.robot_name, {}, self.appliance_client)
        self.interrupt_behavior_task_editor.load_ui()
        # Interlock task widget
        self.interlock_task_editor = InterlockTask(
            self.robot_name, self.interlock_manager)
        self.interlock_task_editor.load_ui()
        # WaitOnStatus task widget
        self.wait_on_status_task_editor = WaitOnStatusTask(
            self.robot_name)
        self.wait_on_status_task_editor.load_ui()
        # DSM AddBox task widget
        self.dsm_add_box_task_editor = DSMAddBoxTask(self.robot_name,{},self.appliance_client)
        self.dsm_add_box_task_editor.load_ui()
        # DSM RemoveBoxes task widget
        self.dsm_remove_boxes_task_editor = DSMRemoveBoxesTask(self.robot_name,{},self.appliance_client)
        self.dsm_remove_boxes_task_editor.load_ui()
        # Reparent task widget
        self.reparent_task_editor = ReparentTask(self.robot_name,{},self.appliance_client)
        self.reparent_task_editor.load_ui()
        # RapidSense ScanScene task widget
        self.scan_scene_task_editor = ScanScene(self.robot_name, {}, self.appliance_client)
        self.scan_scene_task_editor.load_ui()
        # RapidSense ClearScene task widget
        self.rs_clear_scene_task_editor = RS_ClearSceneTask(self.robot_name, {}, self.appliance_client)
        self.rs_clear_scene_task_editor.load_ui()
        # RapidSense FindSceneDeviations task widget
        self.rs_find_scene_deviations_task_editor = RS_FindSceneDeviationsTask(self.robot_name, {}, self.appliance_client)
        self.rs_find_scene_deviations_task_editor.load_ui()
        # RapidSense AddExpectedObject task widget
        self.rs_add_expected_object_task_editor = RS_AddExpectedObjectTask(self.robot_name, {}, self.appliance_client)
        self.rs_add_expected_object_task_editor.load_ui()
        # RapidSense GetRapidSenseStatus task widget
        self.get_rapidsense_status_task_editor = GetRapidSenseStatusTask(self.robot_name, {}, self.appliance_client)
        self.get_rapidsense_status_task_editor.load_ui()
        # RapidSense RS_FindHeight task widget
        self.height_check_task_editor = RS_FindHeightTask(self.robot_name,{},self.appliance_client,self.global_variables)
        self.height_check_task_editor.load_ui()
        # UserLog task widget
        self.user_log_task_editor = UserLogTask(self.robot_name, {}, self.appliance_client)
        self.user_log_task_editor.load_ui()

        # Add the type strings to the combo box
        self.ui.task_type_combobox.addItems(['Move', 'SetInterruptBehavior', 'SetAlternateLocation',
                                             'SetRobotPreset', 'SetObjectState', 'ClearFaults','CancelMove', 'Wait',
                                             'Interlock', 'WaitOnStatus', 'AddBox', 'RemoveBoxes', 'Reparent',
                                             'UserLog', 'ScanScene'])
                                            # 'RS_FindHeight', 'RS_ClearScene', 'RS_FindSceneDeviations',
                                            # 'RS_AddExpectedObject', 'GetRapidSenseStatus', 

        # Plugin additions
        if 'onrobot' in self.plugin_names:
            # OnRobot RG2 task widget
            self.onrobot_rg2_task_editor = OnRobotRG2(self.robot_name, parameters={})
            self.onrobot_rg2_task_editor.load_ui()
            idx = self.ui.task_type_combobox.count()
            self.ui.task_type_combobox.insertSeparator(idx)
            self.ui.task_type_combobox.addItems(['OnRobot RG2'])
        if 'robotiq' in self.plugin_names:
            # Robotiq 2F85 task widget
            self.robotiq_2f85_task_editor = Robotiq2F85(self.robot_name, parameters={})
            self.robotiq_2f85_task_editor.load_ui()
            idx = self.ui.task_type_combobox.count()
            self.ui.task_type_combobox.insertSeparator(idx)
            self.ui.task_type_combobox.addItems(['Robotiq 2F85'])
        if 'moxa' in self.plugin_names:
            # DigitalIO task widget
            self.digitalio_task_editor = DigitalIO(self.robot_name, self.interlock_manager)
            self.digitalio_task_editor.load_ui()
            idx = self.ui.task_type_combobox.count()
            self.ui.task_type_combobox.insertSeparator(idx)
            self.ui.task_type_combobox.addItems(['Digital I/O'])
        if 'sick_plb' in self.plugin_names:
            # SICKTrigger task widget
            self.sick_trigger_task_editor = SICKTriggerTask(
                self.robot_name)
            self.sick_trigger_task_editor.load_ui()
            # SICKLocateParts task widget
            self.sick_locate_parts_editor = SICKLocatePartsTask(
                self.robot_name, global_variables=self.global_variables)
            self.sick_locate_parts_editor.load_ui()
            # SICKCombinedPick1 task widget
            self.sick_combined_pick1_editor = SICKCombinedPick1(
                self.robot_name, parameters={}, global_variables=self.global_variables, appliance_client=self.appliance_client)
            self.sick_combined_pick1_editor.load_ui()
            # SICKCombinedPick2 task widget
            self.sick_combined_pick2_editor = SICKCombinedPick2(
                self.robot_name, parameters={}, global_variables=self.global_variables, appliance_client=self.appliance_client)
            self.sick_combined_pick2_editor.load_ui()
            # SICKCombinedPick3 task widget
            self.sick_combined_pick3_editor = SICKCombinedPick3(
                self.robot_name, parameters={}, global_variables=self.global_variables, appliance_client=self.appliance_client)
            self.sick_combined_pick3_editor.load_ui()
            # SICKMoveToDropBin1 task widget
            self.sick_move_to_drop1_task_editor = SICKMoveToDropBin1(
                self.robot_name, parameters={}, appliance_client=self.appliance_client)
            self.sick_move_to_drop1_task_editor.load_ui()
            # SICKMoveToDropBin2 task widget
            self.sick_move_to_drop2_task_editor = SICKMoveToDropBin2(
                self.robot_name, parameters={}, appliance_client=self.appliance_client)
            self.sick_move_to_drop2_task_editor.load_ui()
            idx = self.ui.task_type_combobox.count()
            self.ui.task_type_combobox.insertSeparator(idx)
            self.ui.task_type_combobox.addItems(['SICKTrigger','SICKLocateParts','SICKCombinedPick1','SICKCombinedPick2',
                                                 'SICKCombinedPick3','SICKMoveToDropBin1','SICKMoveToDropBin2'])

    def update_task_list_index(self, idx):
        # You need to update the list index as the task_list_loop runs using a signal because it is running in a thread
        # You will see the error:
        #   QObject::connect: Cannot queue arguments of type 'QItemSelection'
        #   (Make sure 'QItemSelection' is registered using qRegisterMetaType().)
        # if you set the index directly in task_list_loop
        self.ui.task_list_widget.setCurrentRow(idx)

    def reset_list_color(self):
        self.logger.debug(f'Resetting list color to white.')
        count = self.ui.task_list_widget.count()
        for idx in range(count):
            item = self.ui.task_list_widget.item(idx)
            item.setBackground(TaskColors().IDLE)
    
    def execute_task(self):
        item = self.ui.task_list_widget.item(self.idx)
        self.current_task = self.ui.task_list_widget.itemWidget(item)
        item.setBackground(QColor("#a7cbf2"))  # Light Blue
        if self.stop:
            self.set_state_running(False)
            self.logger.info(
                f'Robot [{self.robot_name}] stopped.')
            self.stop = False
            item.setBackground(Qt.white)  # White
            self.idx = 0
            return
        if self.pause:
            self.set_state_running(False)
            self.logger.info(
                f'Robot [{self.robot_name}] paused.')
            self.pause = False
            item.setBackground(QColor("#a7cbf2"))  # Light Blue
            return
        # Skip execution if needed
        if self.current_task.parameters['execute_once'] and self.loop_count >= 1:
            self.logger.info(f'Skipping {self.current_task.type} task for {self.robot_name}, only executing once.')
        # Execute task
        else:
            self.current_task.execute(speed_override=self.speed_override,
                                      loop_count=self.loop_count, pause=self.pause, stop=self.stop)
            # if there is an error in the task, stop the robots
            if self.current_task.error:
                if self.ui.task_failure_remedy.currentText() == 'Stop on Failure':
                    self.stop = True # the behavior for failed tasks should be configurable: continue, stop, retry
                    self.logger.error(f'Robot [{self.robot_name}] stopped due to error in task.')
                    self.stop_task_plans_request.emit()
                item.setBackground(TaskColors().ERROR)
                return
            else:
                item.setBackground(TaskColors().SUCCESS)
            
            # switch task plans if there are no parts left
            if self.current_task.type == 'SICKLocateParts':
                self.logger.debug(
                    f'Robot flag states for [{self.robot_name}]:\n\tNo parts detected: {Robot.no_parts_detected}')
                if Robot.no_parts_detected:
                    self.logger.debug(
                        f'No parts detected. Sending switch task plans request from [{self.robot_name}]')
                    self.switch_task_plans_request.emit()
                    self.stop = True
                    Robot.no_parts_detected = False
                    return

            if self.current_task.type == 'SICKCombinePick1' or self.current_task.type == 'SICKCombinePick2' or self.current_task.type == 'SICKCombinePick3':
                self.logger.debug(
                    f'Robot no move available check for [{self.robot_name}]:\n\tNo move available 1: {Robot.no_move_available_1}\n\tNo move available 2: {Robot.no_move_available_2}\n\tNo move available 3: {Robot.no_move_available_3}')
                if Robot.no_move_available_1 and Robot.no_move_available_2 and Robot.no_move_available_3:
                    self.logger.debug(
                        'No moves available. Sending switch task plans request from Robot.py')
                    Robot.no_move_available_1 = False
                    Robot.no_move_available_2 = False
                    Robot.no_move_available_3 = False
                    self.switch_task_plans_request.emit()
                    self.stop = True
                    return

    def execute_task_list(self):
        self.set_state_running(True)
        self.reset_signal.emit()  # Check if this is really needed
        self.reset_list_color()
        if self.idx < 0 or self.idx >= self.ui.task_list_widget.count():
            self.idx = 0
        tic = time.time()

        self.stop_count_1 = 0
        self.stop_count_2 = 0

        # While the next list index is not out of range
        while self.idx < self.ui.task_list_widget.count():
            self.execute_task()
            if self.stop or self.pause:
                break
            item = self.ui.task_list_widget.item(self.idx)
            self.idx = self.ui.task_list_widget.row(item) + 1
        toc = time.time()
        loop_time = '%0.3f' % (toc - tic)
        self.ui.cycle_time_label.setText(f'{loop_time} s')

        # Reset the task list index only if the list completed on it's own
        if not self.pause and not self.stop:
            self.idx = 0

        # Update the cycle count and cycle times
        self.cycle_details_dialog.add_cycle_time(loop_time)
        self.loop_count += 1
        # self.pause = False
        # self.stop = False
        self.logger.info(f'Robot [{self.robot_name}] completed task list in {loop_time} seconds.')
        self.done_signal.emit(self.robot_name)
        # catch runaway scenario where task list is finishing very fast
        if float(loop_time) < 0.1:
            time.sleep(0.1)
        self.set_state_running(False)
        # self.pause = False
        # self.stop = False
    
    def set_list_index(self):
        # Clear current list index color
        if self.idx > 0 and self.ui.task_list_widget.count() > 0:
            item = self.ui.task_list_widget.item(self.idx)
            item.setBackground(Qt.white)  # White

        # Set new list index to blue
        row = self.ui.task_list_widget.currentRow()
        if row < 0:
            row = 0
        self.idx = row
        item = self.ui.task_list_widget.item(self.idx)
        item.setBackground(QColor("#a7cbf2"))  # Light Blue
        self.unselect_task_widget()

    def step_forward(self):
        # Don't start the robot if it is already running
        if self.running:
            self.logger.warning(f"Robot [{self.robot_name}] is already running, so [task list step forward] will not execute!")
            return
        self.set_state_running(True)
        # Reset the index if it is out of bounds
        if self.idx < 0 or self.idx >= self.ui.task_list_widget.count():
            self.idx = 0
        # Execute the task
        self.execute_task()
        item = self.ui.task_list_widget.item(self.idx)
        self.idx = self.ui.task_list_widget.row(item) + 1
        if self.idx >= self.ui.task_list_widget.count():
            self.idx = 0
        item = self.ui.task_list_widget.item(self.idx)
        if item is not None:
            item.setBackground(QColor("#a7cbf2"))  # Light Blue
        self.set_state_running(False)

    def step_backward(self):
        if self.running:
            self.logger.warning(
                f"Robot [{self.robot_name}] is already running, so [task list step backward] will not execute!")
            return
        self.set_state_running(True)
        if self.idx < 0 or self.idx >= self.ui.task_list_widget.count():
            self.idx = 0
        # Reset color of current index to white
        item = self.ui.task_list_widget.item(self.idx)
        item.setBackground(Qt.white)
        # Step back and set step to active
        if self.idx == 0:
            self.idx = self.ui.task_list_widget.count() - 1
        else:
            self.idx -= 1
        self.execute_task()
        item = self.ui.task_list_widget.item(self.idx)
        item.setBackground(QColor("#a7cbf2"))   # Light Blue
        self.set_state_running(False)

    def play_tasklist(self):
        # Don't start another task list thread if one is already running
        if self.running:
            self.logger.warning(f"Robot [{self.robot_name}] is already running, so [task list play] will not execute!")
            return
        self.execute_task_list()

    def pause_tasklist(self):
        self.logger.info(f"Pausing task list for robot [{self.robot_name}].")
        self.pause = True
        self.current_task.pause_task()
        # reset for case where loop is not executing
        if not self.running:
            self.pause = False

    def stop_tasklist(self,requester=None):
        if requester:
            self.logger.info(f"[{requester}] is stopping task list for robot [{self.robot_name}].")
        else:
            self.logger.info(f"Stopping task list for robot [{self.robot_name}].")
        
        # If the stop button is pressed more than once, reset the task list to index 0
        if not self.running:
            self.logger.info(f'Resetting list index.')
            self.reset_list_color()
            self.stop = False
            self.idx = 0
            return

        self.stop = True
        self.current_task.stop_task()
        # reset for case where loop is not executing

    def move_task_to_top(self):
        # If there are 0 or 1 rows, exit
        row_count = self.ui.task_list_widget.count()
        if row_count <= 1:
            return
        row = self.ui.task_list_widget.currentRow()
        # If the selected row is already at index 0, exit
        if row <= 0 or row > row_count - 1:
            return
        
        selected_item = self.ui.task_list_widget.item(row)
        task = self.ui.task_list_widget.itemWidget(selected_item)
        self.logger.debug(f' Moving task {task.type} ({task.uuid}) to top of list.')

        item = self.ui.task_list_widget.takeItem(row)
        self.ui.task_list_widget.insertItem(0, item)

        task = task.copy()
        task.load_list_ui()
        self.ui.task_list_widget.setItemWidget(item, task)
        self.update_task_list_index(0)

        self.unsaved_changes.emit(True)

    def move_task_up(self):
        # If there are 0 or 1 rows, exit
        row_count = self.ui.task_list_widget.count()
        if row_count <= 1:
            return
        row = self.ui.task_list_widget.currentRow()
        # If the selected row is already at index 0, exit
        if row <= 0 or row > row_count - 1:
            return

        selected_item = self.ui.task_list_widget.item(row)
        task = self.ui.task_list_widget.itemWidget(selected_item)
        self.logger.debug(f' Moving task {task.type} ({task.uuid}) up')

        item = self.ui.task_list_widget.takeItem(row)
        self.ui.task_list_widget.insertItem(row - 1, item)

        task = task.copy()
        task.load_list_ui()
        self.ui.task_list_widget.setItemWidget(item, task)
        self.update_task_list_index(row - 1)

        self.unsaved_changes.emit(True)

    def move_task_to_bottom(self):
        # If there are 0 or 1 rows, exit
        row_count = self.ui.task_list_widget.count()
        if row_count <= 1:
            return
        row = self.ui.task_list_widget.currentRow()
        # If the selected row is already at the end of the list, exit
        if row == row_count - 1 or row < 0:
            return
        
        selected_item = self.ui.task_list_widget.item(row)
        task = self.ui.task_list_widget.itemWidget(selected_item)
        self.logger.debug(f' Moving task {task.type} ({task.uuid}) up')

        item = self.ui.task_list_widget.takeItem(row)
        self.ui.task_list_widget.insertItem(row_count, item)

        task = task.copy()
        task.load_list_ui()
        self.ui.task_list_widget.setItemWidget(item, task)
        self.update_task_list_index(row_count-1)

        self.unsaved_changes.emit(True)

    def move_task_down(self):
        # If there are 0 or 1 rows, exit
        row_count = self.ui.task_list_widget.count()
        if row_count <= 1:
            return
        row = self.ui.task_list_widget.currentRow()
        # If the selected row is already at the end of the list, exit
        if row == row_count - 1 or row < 0:
            return

        selected_item = self.ui.task_list_widget.item(row)
        task = self.ui.task_list_widget.itemWidget(selected_item)
        self.logger.debug(f' Moving task {task.type} ({task.uuid}) up')

        item = self.ui.task_list_widget.takeItem(row)
        self.ui.task_list_widget.insertItem(row + 1, item)

        task = task.copy()
        task.load_list_ui()
        self.ui.task_list_widget.setItemWidget(item, task)
        self.update_task_list_index(row + 1)

        self.unsaved_changes.emit(True)

    def duplicate_task(self):
        row = self.ui.task_list_widget.currentRow()
        selected_item = self.ui.task_list_widget.item(row)
        task_to_dupe = self.ui.task_list_widget.itemWidget(selected_item)

        task = task_to_dupe.clone()
        task.load_list_ui()
        list_item = QListWidgetItem()
        list_item.setSizeHint(task.minimumSizeHint())
        self.ui.task_list_widget.insertItem(row + 1, list_item)
        self.ui.task_list_widget.setItemWidget(list_item, task)
    
    def copy_task(self):
        row = self.ui.task_list_widget.currentRow()
        self.logger.info(f'Copying task at row: {row}')
        selected_item = self.ui.task_list_widget.item(row)
        task_to_dupe = self.ui.task_list_widget.itemWidget(selected_item)
        self.copied_task = task_to_dupe.clone()

    def cut_task(self):
        row = self.ui.task_list_widget.currentRow()
        self.logger.info(f'Cutting task at row: {row}')
        selected_item = self.ui.task_list_widget.item(row)
        task_to_dupe = self.ui.task_list_widget.itemWidget(selected_item)
        self.copied_task = task_to_dupe.clone()
        self.delete_task()

    def paste_task(self):
        row = self.ui.task_list_widget.currentRow()
        self.logger.info(f'Pasting task at row: {row}')
        task = self.copied_task.clone()
        task.load_list_ui()
        list_item = QListWidgetItem()
        list_item.setSizeHint(task.minimumSizeHint())
        self.ui.task_list_widget.insertItem(row + 1, list_item)
        self.ui.task_list_widget.setItemWidget(list_item, task)

    def delete_task(self):
        # This method removes the currently selected row from the task list
        row = self.ui.task_list_widget.currentRow()
        if row < 0:
            return
        row_count = self.ui.task_list_widget.count()
        if row_count == 0:
            return

        if not (self.running and row == self.idx):
            selected_item = self.ui.task_list_widget.item(row)
            task = self.ui.task_list_widget.itemWidget(selected_item)
            self.logger.info(f"Deleting {task.type} at row {row}")
            Task.tasks.pop(task.uuid)
            self.ui.task_list_widget.removeItemWidget(selected_item)
            item = self.ui.task_list_widget.takeItem(row)
            task.setParent(None)
            task.deleteLater()
            del (item)

        self.unsaved_changes.emit(True)

    def clear_task_list(self):
        # This method removes all tasks from the task list
        if self.running:
            self.logger.warning(f"Cannot clear task list while it's currently running!")
            return

        confirmation = MessageDialog('Clear Task List Confirmation',f"You are about to remove all tasks from the task list for robot [{self.robot_name}], would you like to proceed?", parent=self)
        ret_val = confirmation.exec()
        if ret_val == QMessageBox.No:
            self.logger.info(f"Confirmation denied, so not clearing task list for robot [{self.robot_name}].")
            return

        self.ui.task_list_widget.clear()
        self.unsaved_changes.emit(True)

    def ui_add_task(self):
        # Add a task to the Robot's task list from the UI elements
        new_task = self.get_editor_command()
        self.add_to_task_list(new_task,insert=True)

    def edit_task(self):
        if self.running:
            self.logger.warning(
                f"Tried to edit a task while the taskplan is running.")
            return

        row = self.ui.task_list_widget.currentRow()
        if row < 0:
            return
        row_count = self.ui.task_list_widget.count()
        if row_count == 0:
            return

        selected_item = self.ui.task_list_widget.item(row)
        task = self.ui.task_list_widget.itemWidget(selected_item)
        self.logger.info(f"Editing task [{task.type}] at row {row}")

        task_copy = task.copy()
        self.logger.debug(f"Task copy uuid is {task_copy.uuid}")
        confirmation = TaskEditor(task_copy, parent=self)
        if not confirmation.exec():
            self.logger.info(f"Task edits rejected.")
            return
        self.logger.info(f"Task edits accepted.")
        task_copy.apply_ui_to_task(
            int(confirmation.execute_once_checkbox.isChecked()))
        task.parameters = task_copy.parameters.copy()
        task.display_label.setText(task.to_string())
        self.unsaved_changes.emit(True)

    def add_task_internal(self, type='', parameters=Dict[str, Any], return_task=False, new_uuid: str = ''):
        if not new_uuid:
            new_uuid = str(uuid.uuid4())
        # Add a task to the Robot's task list from a JSON task lists save file
        if type == 'Move':
            new_task = MoveTask(self.robot_name, parameters,
                                self.appliance_client, self.presets_targets_dict, self.global_variables, uuid=new_uuid)

        elif type == 'Wait':
            new_task = WaitTask(self.robot_name, parameters, uuid=new_uuid)

        elif type == 'SetAlternateLocation':
            new_task = AlternateLocationTask(
                self.robot_name, parameters, self.appliance_client, self.presets_targets_dict, uuid=new_uuid)

        elif type == 'SetInterruptBehavior':
            new_task = InterruptBehaviorTask(
                self.robot_name, parameters, self.appliance_client, uuid=new_uuid)

        elif type == 'SetObjectState':
            new_task = ObjectStateTask(
                self.robot_name, parameters, self.appliance_client, uuid=new_uuid)

        elif type == 'SetRobotPreset':
            new_task = RobotPresetTask(
                self.robot_name, parameters, self.appliance_client, self.presets_targets_dict, uuid=new_uuid)

        elif type == 'CancelMove':
            new_task = CancelMoveTask(
                self.robot_name, parameters, self.appliance_client, uuid=new_uuid)

        elif type == 'ClearFaults':
            new_task = ClearFaultsTask(
                self.robot_name, parameters, self.appliance_client, uuid=new_uuid)
            
        elif type == 'Interlock':
            new_task = InterlockTask(
                self.robot_name, self.interlock_manager, parameters, uuid=new_uuid)

        elif type == 'WaitOnStatus':
            new_task = WaitOnStatusTask(
                self.robot_name, parameters, uuid=new_uuid)
            
        elif type == 'Robotiq 2F85':
            new_task = Robotiq2F85(self.robot_name,parameters, uuid=new_uuid)
        
        elif type == 'OnRobot RG2':
            new_task = OnRobotRG2(self.robot_name,parameters, uuid=new_uuid)
        
        elif type == 'AddBox':
            new_task = DSMAddBoxTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)
        
        elif type == 'RemoveBoxes':
            new_task = DSMRemoveBoxesTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'Reparent':
            new_task = ReparentTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'ScanScene':
            new_task = ScanScene(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)
        
        elif type == 'RS_ClearScene':
            new_task = RS_ClearSceneTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'RS_FindSceneDeviations':
            new_task = RS_FindSceneDeviationsTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'RS_AddExpectedObject':
            new_task = RS_AddExpectedObjectTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'GetRapidSenseStatus':
            new_task = GetRapidSenseStatusTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'RS_FindHeight':
            new_task = RS_FindHeightTask(self.robot_name,parameters,self.appliance_client,self.global_variables,uuid=new_uuid)
        
        elif type == 'UserLog':
            new_task = UserLogTask(self.robot_name,parameters,self.appliance_client,uuid=new_uuid)

        elif type == 'Digital I/O':
            new_task = DigitalIO(
                self.robot_name, parameters, uuid=new_uuid)

        elif type == 'SICKTrigger':
            new_task = SICKTriggerTask(
                self.robot_name, parameters, uuid=new_uuid)

        elif type == 'SICKLocateParts':
            new_task = SICKLocatePartsTask(
                self.robot_name, parameters, uuid=new_uuid, global_variables=self.global_variables)

        elif type == 'SICKCombinedPick1':
            new_task = SICKCombinedPick1(self.robot_name, parameters, appliance_client=self.appliance_client,
                                         uuid=new_uuid, global_variables=self.global_variables)

        elif type == 'SICKCombinedPick2':
            new_task = SICKCombinedPick2(self.robot_name, parameters, appliance_client=self.appliance_client,
                                         uuid=new_uuid, global_variables=self.global_variables)

        elif type == 'SICKMoveToDropBin1':
            new_task = SICKMoveToDropBin1(self.robot_name, parameters, appliance_client=self.appliance_client,
                                          uuid=new_uuid)

        elif type == 'SICKMoveToDropBin2':
            new_task = SICKMoveToDropBin2(self.robot_name, parameters, appliance_client=self.appliance_client,
                                          uuid=new_uuid)

        elif type == 'SICKCombinedPick3':
            new_task = SICKCombinedPick3(self.robot_name, parameters, appliance_client=self.appliance_client,
                                         uuid=new_uuid, global_variables=self.global_variables)

        if return_task:
            return new_task

        self.add_to_task_list(new_task)
        self.unselect_task_widget()

    def add_to_task_list(self, task: Task, insert = False):
        list_item = QListWidgetItem()
        task.load_list_ui()
        list_item.setSizeHint(task.minimumSizeHint())
        if insert:
            row = self.ui.task_list_widget.currentRow()
            self.ui.task_list_widget.insertItem(row + 1, list_item)
            self.ui.task_list_widget.setCurrentRow(row+1)
        else:
            self.ui.task_list_widget.addItem(list_item)
        self.ui.task_list_widget.setItemWidget(list_item, task)
        self.logger.info(
            f'Added task for robot [{self.robot_name}]: {task.to_string()}, uuid: {task.uuid}')
        self.unsaved_changes.emit(True)

    def set_speed_override(self, value):
        # Set the robot speed override
        self.speed_override = value

    def get_editor_command(self,editor_execute=False):
        # Return a task type object with parameters currently populated in the task editor
        type = self.ui.task_type_combobox.currentText()
        execute_once = int(self.ui.execute_once_checkbox.isChecked())
        if type == 'Move':
            self.move_task_editor.apply_ui_to_task(execute_once)
            parameters = self.move_task_editor.parameters.copy()
            new_task = MoveTask(self.robot_name, parameters,
                                self.appliance_client, self.presets_targets_dict, self.global_variables)

        elif type == 'Wait':
            self.wait_task_editor.apply_ui_to_task(execute_once)
            parameters = self.wait_task_editor.parameters.copy()
            new_task = WaitTask(self.robot_name, parameters)

        elif type == 'SetAlternateLocation':
            self.alternate_location_task_editor.apply_ui_to_task(execute_once)
            parameters = self.alternate_location_task_editor.parameters.copy()
            new_task = AlternateLocationTask(
                self.robot_name, parameters, self.appliance_client, self.presets_targets_dict)

        elif type == 'SetInterruptBehavior':
            self.interrupt_behavior_task_editor.apply_ui_to_task(execute_once)
            parameters = self.interrupt_behavior_task_editor.parameters.copy()
            new_task = InterruptBehaviorTask(
                self.robot_name, parameters, self.appliance_client)

        elif type == 'SetObjectState':
            self.object_state_task_editor.apply_ui_to_task(execute_once)
            parameters = self.object_state_task_editor.parameters.copy()
            new_task = ObjectStateTask(
                self.robot_name, parameters, self.appliance_client)

        elif type == 'SetRobotPreset':
            self.robot_preset_task_editor.apply_ui_to_task(execute_once)
            parameters = self.robot_preset_task_editor.parameters.copy()
            new_task = RobotPresetTask(
                self.robot_name, parameters, self.appliance_client, self.presets_targets_dict)

        elif type == 'CancelMove':
            self.cancel_move_task_editor.apply_ui_to_task(execute_once)
            parameters = self.cancel_move_task_editor.parameters.copy()
            new_task = CancelMoveTask(
                self.robot_name, parameters, self.appliance_client)

        elif type == 'ClearFaults':
            self.clear_faults_task_editor.apply_ui_to_task(execute_once)
            parameters = self.clear_faults_task_editor.parameters.copy()
            new_task = ClearFaultsTask(
                self.robot_name, parameters, self.appliance_client)
            
        elif type == 'Interlock':
            self.interlock_task_editor.apply_ui_to_task(execute_once)
            parameters = self.interlock_task_editor.parameters.copy()
            new_task = InterlockTask(
                self.robot_name, self.interlock_manager, parameters)

        elif type == 'Digital I/O':
            self.digitalio_task_editor.apply_ui_to_task(execute_once)
            parameters = self.digitalio_task_editor.parameters.copy()
            new_task = DigitalIO(
                self.robot_name, parameters)

        elif type == 'WaitOnStatus':
            self.wait_on_status_task_editor.apply_ui_to_task(execute_once)
            parameters = self.wait_on_status_task_editor.parameters.copy()
            new_task = WaitOnStatusTask(
                self.robot_name, parameters)
            
        elif type == 'Robotiq 2F85':
            self.robotiq_2f85_task_editor.apply_ui_to_task(execute_once)
            parameters = self.robotiq_2f85_task_editor.parameters.copy()
            new_task = Robotiq2F85(self.robot_name,parameters)
        
        elif type == 'OnRobot RG2':
            self.onrobot_rg2_task_editor.apply_ui_to_task(execute_once)
            parameters = self.onrobot_rg2_task_editor.parameters.copy()
            new_task = OnRobotRG2(self.robot_name,parameters)

        elif type == 'AddBox':
            self.dsm_add_box_task_editor.apply_ui_to_task(execute_once)
            parameters = self.dsm_add_box_task_editor.parameters.copy()
            new_task = DSMAddBoxTask(self.robot_name,parameters,self.appliance_client)
        
        elif type == 'RemoveBoxes':
            self.dsm_remove_boxes_task_editor.apply_ui_to_task(execute_once)
            parameters = self.dsm_remove_boxes_task_editor.parameters.copy()
            new_task = DSMRemoveBoxesTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'Reparent':
            self.reparent_task_editor.apply_ui_to_task(execute_once)
            parameters = self.reparent_task_editor.parameters.copy()
            new_task = ReparentTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'ScanScene':
            self.scan_scene_task_editor.apply_ui_to_task(execute_once)
            parameters = self.scan_scene_task_editor.parameters.copy()
            new_task = ScanScene(self.robot_name,parameters,self.appliance_client)

        elif type == 'RS_ClearScene':
            self.rs_clear_scene_task_editor.apply_ui_to_task(execute_once)
            parameters = self.rs_clear_scene_task_editor.parameters.copy()
            new_task = RS_ClearSceneTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'RS_FindSceneDeviations':
            self.rs_find_scene_deviations_task_editor.apply_ui_to_task(execute_once)
            parameters = self.rs_find_scene_deviations_task_editor.parameters.copy()
            new_task = RS_FindSceneDeviationsTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'RS_AddExpectedObject':
            self.rs_add_expected_object_task_editor.apply_ui_to_task(execute_once)
            parameters = self.rs_add_expected_object_task_editor.parameters.copy()
            new_task = RS_AddExpectedObjectTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'GetRapidSenseStatus':
            # editor_execute was added, so pressing the execute button updates
            # the editor ui, rather than creating a new task which the user cannot see
            # The reason a new task is created though, is so when you press add to task list,
            # the editor object remains unchanged and then the new task uses the display ui instead of
            # the editor ui
            # In general, adding tasks needs refactoring. There is now add_task_internal and get_editor_command with
            # two modalities
            if not editor_execute:
                self.get_rapidsense_status_task_editor.apply_ui_to_task(execute_once)
                parameters = self.get_rapidsense_status_task_editor.parameters.copy()
                new_task = GetRapidSenseStatusTask(self.robot_name,parameters,self.appliance_client)
            else:
                new_task = self.get_rapidsense_status_task_editor
        
        elif type == 'RS_FindHeight':
            self.height_check_task_editor.apply_ui_to_task(execute_once)
            parameters = self.height_check_task_editor.parameters.copy()
            new_task = RS_FindHeightTask(self.robot_name,parameters,self.appliance_client,self.global_variables)

        elif type == 'UserLog':
            self.user_log_task_editor.apply_ui_to_task(execute_once)
            parameters = self.user_log_task_editor.parameters.copy()
            new_task = UserLogTask(self.robot_name,parameters,self.appliance_client)

        elif type == 'SICKTrigger':
            self.sick_trigger_task_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_trigger_task_editor.parameters.copy()
            new_task = SICKTriggerTask(
                self.robot_name, parameters)

        elif type == 'SICKLocateParts':
            self.sick_locate_parts_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_locate_parts_editor.parameters.copy()
            new_task = SICKLocatePartsTask(
                self.robot_name, parameters, global_variables=self.global_variables)

        elif type == 'SICKCombinedPick1':
            self.sick_combined_pick1_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_combined_pick1_editor.parameters.copy()
            new_task = SICKCombinedPick1(
                self.robot_name, parameters, appliance_client=self.appliance_client, global_variables=self.global_variables)

        elif type == 'SICKCombinedPick2':
            self.sick_combined_pick2_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_combined_pick2_editor.parameters.copy()
            new_task = SICKCombinedPick2(
                self.robot_name, parameters, appliance_client=self.appliance_client, global_variables=self.global_variables)

        elif type == 'SICKMoveToDropBin1':
            self.sick_move_to_drop1_task_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_move_to_drop1_task_editor.parameters.copy()
            new_task = SICKMoveToDropBin1(
                self.robot_name, parameters, appliance_client=self.appliance_client)

        elif type == 'SICKMoveToDropBin2':
            self.sick_move_to_drop2_task_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_move_to_drop2_task_editor.parameters.copy()
            new_task = SICKMoveToDropBin2(
                self.robot_name, parameters, appliance_client=self.appliance_client)

        elif type == 'SICKCombinedPick3':
            self.sick_combined_pick3_editor.apply_ui_to_task(execute_once)
            parameters = self.sick_combined_pick3_editor.parameters.copy()
            new_task = SICKCombinedPick3(
                self.robot_name, parameters, appliance_client=self.appliance_client, global_variables=self.global_variables)

        self.logger.debug(f'\n\tCreating NEW TASK\n\t{new_task.uuid}\n\t{new_task}')
        return new_task

    def task_editor_execute(self):
        self.current_task = self.get_editor_command(editor_execute=True)
        self.current_task.execute(
            speed_override=self.speed_override, pause=self.pause, stop=self.stop)

    def keyPressEvent(self, event: QKeyEvent):
        key = event.key()
        modifiers = event.modifiers()

        if key == Qt.Key_Delete:
            self.delete_task()
        elif key == Qt.Key_C and modifiers == Qt.ControlModifier:
            self.copy_task()
        elif key == Qt.Key_X and modifiers == Qt.ControlModifier:
            self.cut_task()
        elif key == Qt.Key_V and modifiers == Qt.ControlModifier:
            self.paste_task()
        else:
            super().keyPressEvent(event)

    def unselect_task_widget(self):
        self.update_task_list_index(-1)

    def set_state_running(self, state: bool):
        self.running = state
        self.running_state.emit(state, str(self.uuid))

    def open_details(self):
        self.cycle_details_dialog.show()

    def get_presets_targets_dict(self):
        presets_targets_dict = {}

        # Get the presets
        handler = self.appliance_client.send_msg(
            ASCIIGetRobotPresets(self.robot_name))
        try:
            handler.wait(1)
        except Exception as e:
            self.logger.error(
                f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e}")
            presets_targets_dict = {}
            return presets_targets_dict
        presets = handler.received[-1]["data"]["robot_presets"]
        handler.close_handler()

        # Get the targets
        for preset in presets:
            handler = self.appliance_client.send_msg(
                ASCIIGetTargets(self.robot_name, preset))
            try:
                handler.wait(1)
            except Exception as e:
                self.logger.error(
                    f"[{self.robot_name}] {handler.original.get_topic()} ERROR: {e}")
                presets_targets_dict = {}
                return presets_targets_dict
            targets = handler.received[0]["data"]["targets"]
            handler.close_handler()
            presets_targets_dict.update({f"{preset}": targets})

        self.appliance_client.stop()
        return presets_targets_dict

    def clone(self):
        robot_dict = self.asdict()
        new_robot = Robot(robot_name=self.robot_name,
                          ip_address=self.ip_address)
        for count, task in enumerate(robot_dict['task_list']):
            task_type = robot_dict['task_list'][count]['type']
            task_parameters = robot_dict['task_list'][count]['parameters']
            new_robot.add_task_internal(task_type, task_parameters)
        return new_robot

    def update_interlock_tasks(self):
        count = self.ui.task_list_widget.count()
        for idx in range(count):
            item = self.ui.task_list_widget.item(idx)
            task = self.ui.task_list_widget.itemWidget(item)
            if task.type == 'Interlock':
                task.interlock_manager = self.interlock_manager
