class InterlockManager():
    def __init__(self, interlocks={}):
        self.interlocks = interlocks

    def asdict(self):
        return self.interlocks

    def set_interlock(self, id, value):
        self.interlocks[id] = value

    def clone(self):
        new = InterlockManager()
        new.interlocks = self.interlocks.copy()
        return new
