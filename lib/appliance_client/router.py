from random import randint
from threading import Thread
from typing import Any, Dict, Optional, Union

import yaml

from .messages.message import Message
from .socket_io.socket_client_pool import SocketClientPool
from .tools.dict_ext import deep_clear_nones
from .view.aliases import BIG_INT, LogCallback, MsgCallback
from .view.defaults import DEFAULT_POLLING_PERIOD, DELIMITER, ENCODING, NO_ACTION_LOG
from .view.exceptions import ApplianceClientException
from .view.handler import Handler
from .view.log_event import LogEvent


class Router():
    def __init__(self,
                 client_pool: SocketClientPool,
                 polling_period: float = DEFAULT_POLLING_PERIOD,
                 log_callback: LogCallback = NO_ACTION_LOG) -> None:
        self._handler_map: Dict[Union[str, int], Handler] = {}
        self._client_pool: SocketClientPool = client_pool
        self._polling_period: float = polling_period
        self._running: bool = False
        self.log: LogCallback = log_callback

    def start(self) -> None:
        if self._running:
            raise ApplianceClientException("Cannot start, router already running!")

        self._reader: Thread = Thread(target=self._do_message_reading, daemon=True)
        self._client_pool.start()
        self._running = True
        self._reader.start()

    def stop(self) -> None:
        if not self._running:
            raise ApplianceClientException("Cannot stop, router not running!")

        self._client_pool.close()
        self._running = False
        self._reader.join(self._polling_period + 0.01)

    def send_message(self,
                     msg: Message,
                     msg_callback: MsgCallback = None,
                     id: Optional[Union[str, int]] = None) -> Handler:
        if not self._running:
            raise ApplianceClientException()

        if not id:
            id = randint(0, BIG_INT)

        if id in self._handler_map:
            raise ApplianceClientException()

        handler: Handler = Handler(msg, id, msg_callback)
        self._handler_map[id] = handler

        msg_dict = self._get_msg_dict(msg, id)
        data = self._get_msg_bytes(msg_dict)
        self._client_pool.send(data)
        self.log(LogEvent.SENT, msg_dict)
        return handler

    def _get_msg_dict(self, msg: Message, id: Union[str, int]) -> Any:
        msg_dict = msg.to_msg()
        msg_dict['id'] = id
        deep_clear_nones(msg_dict)
        return msg_dict

    def _get_msg_bytes(self, msg_dict: Any) -> bytes:
        msg_yaml = yaml.dump(data=msg_dict,
                             default_flow_style=True,
                             line_break=DELIMITER,
                             width=BIG_INT)
        data: bytes = msg_yaml.encode(ENCODING)
        return data

    def _do_message_reading(self) -> None:
        while self._running:
            try:
                while self._read_message():
                    pass
            except:
                # TODO: log error. Careful because socket closing is also an exception
                pass

    def _read_message(self) -> bool:
        bytes = self._client_pool.try_get_msg(self._polling_period)
        if bytes is None:
            return False

        msg_dict = yaml.safe_load(bytes.decode(ENCODING))
        self.log(LogEvent.RECEIVED, msg_dict)
        id = msg_dict["id"]
        handler = self._handler_map[id]
        handler.add_message(msg_dict)

        if Message.is_last(msg_dict):
            handler.close_handler()
            self._handler_map.pop(id)

        return True
