from __future__ import annotations

import select
import socket
import time
from typing import Any, Callable, Literal, Optional

import yaml

from ..messages.message import Message
from ..tools.dict_ext import deep_clear_nones
from ..view.aliases import BIG_INT, MsgCallback
from ..view.defaults import DEFAULT_BUFFER_SIZE, DELIMITER, DELIMITER_BYTES, ENCODING
from ..view.exceptions import ApplianceClientException, MessageException, MessageTimeoutException


class SyncClient():
    def __init__(self, ip: str, port: int, buffer_size: int = DEFAULT_BUFFER_SIZE) -> None:
        if (buffer_size <= 0):
            raise ApplianceClientException()
        self._address = (ip, port)
        self._delimiter: bytes = DELIMITER_BYTES
        self._buffer_size: int = buffer_size
        self._recv_buffer: memoryview = memoryview(bytearray(b' ' * self._buffer_size))
        self._msg_buffer: bytearray = bytearray()
        self._socket: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.on_send: Optional[Callable[[], None]] = None
        self.on_arrive: Optional[Callable[[], None]] = None

    def connect(self) -> None:
        self._socket.connect(self._address)

    def disconnect(self) -> None:
        try:
            self._socket.shutdown(socket.SHUT_RDWR)
            self._socket.close()
        except Exception:  # pragma: no cover
            pass

    def send_msg(self,
                 msg: Message,
                 timeout: float,
                 msg_callback: MsgCallback = None,
                 ignore_failure: bool = False) -> list[Any]:
        send_data = self._get_msg_bytes(msg)
        recv: list[Any] = []

        if self.on_send:
            self.on_send()
        self._socket.sendall(send_data)

        max_time = time.time() + timeout
        while time.time() < max_time:
            ready = select.select([self._socket], [], [], timeout)
            if not ready[0]:
                continue
            data = self._socket.recv_into(self._recv_buffer, self._buffer_size)
            if not data:  # pragma: no cover
                continue
            self._msg_buffer.extend(self._recv_buffer[:data])
            delimiter_pos = self._msg_buffer.find(self._delimiter)
            while delimiter_pos > 0:
                if self.on_arrive:
                    self.on_arrive()
                arrived = yaml.safe_load(self._msg_buffer[:delimiter_pos].decode(ENCODING))
                self._msg_buffer = self._msg_buffer[delimiter_pos + len(self._delimiter):]
                recv.append(arrived)
                if msg_callback:
                    msg_callback(arrived)
                if not ignore_failure and Message.has_error(arrived):
                    topic = msg.get_topic()
                    error_code = Message.get_error_code(arrived)
                    raise MessageException(arrived,
                                           f"'{topic}' received error code '{error_code}'.")
                if Message.is_last(arrived):
                    return recv

                delimiter_pos = self._msg_buffer.find(self._delimiter)

        raise MessageTimeoutException("Message timed out")

    def _get_msg_bytes(self, msg: Message) -> bytes:
        msg_dict = msg.to_msg()
        deep_clear_nones(msg_dict)
        msg_yaml = yaml.dump(data=msg_dict,
                             default_flow_style=True,
                             line_break=DELIMITER,
                             width=BIG_INT)
        data: bytes = msg_yaml.encode(ENCODING)
        return data

    def __enter__(self) -> SyncClient:
        self.connect()
        return self

    def __exit__(self, *args: Any, **kwargs: Any) -> Literal[False]:
        self.disconnect()
        del self
        return False
