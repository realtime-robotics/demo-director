import socket
from queue import Queue
from threading import Thread
from time import sleep
from typing import List

from ..view.exceptions import SocketClientException


class SocketClient():
    def __init__(self, sock: socket.socket, delimiter: bytes, buffer_size: int,
                 recv_queue: "Queue[bytearray]") -> None:
        self._socket: socket.socket = sock
        self._delimiter: bytes = delimiter
        self._recv_queue: "Queue[bytearray]" = recv_queue
        self._buffer_size: int = buffer_size
        self._recv_buffer: memoryview = memoryview(bytearray(b' ' * self._buffer_size))
        self._msg_buffer: bytearray = bytearray()
        self._running: bool = False
        self.throttle: float = 0
        self._errors: List[Exception] = list()

    @property
    def errors(self) -> List[Exception]:
        return self._errors.copy()

    def start(self, ip: str, port: int) -> None:
        if self._running:
            raise SocketClientException()
        self._reader: Thread = Thread(target=self._message_reader, daemon=True)
        self._running = True
        self._socket.connect((ip, port))
        self._reader.start()
        # TODO: logger info (started)

    def send(self, data: bytes) -> None:
        if not self._running:
            raise SocketClientException()
        self._socket.sendall(data)

    def close(self, timeout: float = 0.01) -> None:
        if not self._running:
            raise SocketClientException()
        self._running = False
        try:
            self._socket.shutdown(socket.SHUT_RDWR)
        except Exception as _:  # pragma: no cover
            # Not covering because it only throws on windows
            pass
        try:
            self._socket.close()
        except Exception as _:  # pragma: no cover
            # Not covering because this is highly unlikely
            # TODO: log error (socket not connected)
            pass
        self._reader.join(timeout)
        if (self._reader.is_alive()):
            pass
            # TODO: logger warn
            #print("Socket client is still alive!")

        # TODO: logger info (stopped)

    def _message_reader(self) -> None:
        while self._running:
            try:
                self._read_message()
            except Exception as e:
                # TODO: log error. Careful because socket closing is also an exception
                # Also careful because delaying execution on exception handling can make the close() call fail to join
                self._errors.append(e)

    def _read_message(self) -> None:
        # TODO: log info (receiving)
        data = self._socket.recv_into(self._recv_buffer, self._buffer_size)
        if self.throttle > 0:
            sleep(self.throttle)
        if not data:  # pragma: no cover
            return
        self._msg_buffer.extend(self._recv_buffer[:data])
        # TODO: start search from (last position - delimiter size + 1)
        delimiter_pos = self._msg_buffer.find(self._delimiter)
        while delimiter_pos > 0:
            # TODO: log info (message read)
            self._recv_queue.put(self._msg_buffer[:delimiter_pos])
            self._msg_buffer = self._msg_buffer[delimiter_pos + len(self._delimiter):]
            delimiter_pos = self._msg_buffer.find(self._delimiter)
