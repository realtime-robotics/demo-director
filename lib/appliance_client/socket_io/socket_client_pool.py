import socket
import threading
from queue import Empty, Queue
from typing import List, Optional

from .socket_client import SocketClient


class SocketClientPool():
    def __init__(self, ip: str, port: int, delimiter: bytes, buffer_size: int,
                 pool_size: int) -> None:
        self._ip: str = ip
        self._port: int = port
        self._delimiter: bytes = delimiter
        self._buffer_size: int = buffer_size
        self._pool_size: int = pool_size
        self._current: int = 0
        self._recv_queue: Queue[bytearray] = Queue()
        self._selector_lock: threading.Lock = threading.Lock()

    def start(self) -> None:
        self._clients: List[SocketClient] = [
            self._build_client(self._delimiter, self._buffer_size, self._recv_queue)
            for _ in range(self._pool_size)
        ]
        for client in self._clients:
            client.start(self._ip, self._port)

    def close(self) -> None:
        # TODO: throw exception if not started
        for client in self._clients:
            try:
                client.close()
            except:
                # TODO: log warn (error closing client)
                pass
        self._clients.clear()
        self._current = 0

    def send(self, data: bytes) -> None:
        self._clients[self._current].send(data)
        self._update_current()

    def try_get_msg(self, timeout: float) -> Optional[bytes]:
        try:
            return self._recv_queue.get(True, timeout)
        except Empty:
            return None

    def _update_current(self) -> None:
        with self._selector_lock:
            self._current += 1
            if (self._current >= self._pool_size):
                self._current = 0

    def _build_client(self, delimiter: bytes, buffer_size: int,
                      recv_queue: "Queue[bytearray]") -> SocketClient:
        sock: socket.socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client = SocketClient(sock, delimiter, buffer_size, recv_queue)
        return client
