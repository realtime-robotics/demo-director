from typing import Any


class ApplianceClientException(Exception):
    pass


class SocketClientException(ApplianceClientException):
    pass


class NoReplyException(ApplianceClientException):
    pass


class MessageException(ApplianceClientException):
    def __init__(self, error_message: Any, *args: object) -> None:
        super().__init__(*args)
        self.error_message = error_message


class MessageTimeoutException(ApplianceClientException):
    pass
