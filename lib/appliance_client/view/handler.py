from __future__ import annotations

from threading import Event
from typing import Any, Optional, Union

from ..messages.message import Message
from ..tools.scheduler import Scheduler
from .aliases import MsgCallback
from .exceptions import MessageException, MessageTimeoutException, NoReplyException


class Handler():
    def __init__(self,
                 original: Message,
                 id: Union[str, int],
                 recv_callback: MsgCallback = None) -> None:
        self.original: Message = original
        self.id = id
        self.received: list[Any] = []
        self._recv_callback: MsgCallback = recv_callback
        self._closed: Event = Event()
        self._scheduler: Optional[Scheduler] = None

    def add_message(self, msg: Any) -> None:
        self.received.append(msg)
        self._process_callback(msg)

    def close_handler(self) -> None:
        self._closed.set()
        if self._scheduler:
            self._scheduler.stop()

    @property
    def closed(self) -> bool:
        return self._closed.is_set()

    def wait(self,
             timeout: float,
             ignore_failure: bool = False,
             wait_callbacks_timeout: float = -1) -> Handler:
        # TODO: log info (waiting)
        is_set = self._closed.wait(timeout)
        if not ignore_failure and not is_set:
            topic = self.original.get_topic()
            raise MessageTimeoutException(
                f"'{topic}' with id '{self.id}' did not receive all responses within '{timeout}' seconds."
            )
        self._wait_scheduler(wait_callbacks_timeout)
        if not ignore_failure:
            self._ensure_received()
        return self

    def _ensure_received(self) -> None:
        if len(self.received) == 0:
            raise NoReplyException()
        if "error" in self.received[-1]:
            topic = self.original.get_topic()
            error_code = Message.get_error_code(self.received[-1])
            raise MessageException(
                self.received[-1],
                f"'{topic}' with id '{self.id}' received error code '{error_code}'.")
        if not Message.is_last(self.received[-1]):
            raise MessageException(self.received[-1])

    def _process_callback(self, msg: Any) -> None:
        if self._recv_callback is None:
            return
        if self._scheduler is None:
            self._scheduler = Scheduler()
            self._scheduler.start()

        def work() -> None:
            if self._recv_callback:
                self._recv_callback(msg)

        self._scheduler.add_work(work)

    def _wait_scheduler(self, timeout: float) -> None:
        if self._scheduler is None:
            return
        if timeout > 0:
            self._scheduler.wait(timeout)

    @staticmethod
    def wait_all(handlers: list[Handler], timeout: float, wait_callbacks: bool = False) -> None:
        callback_timeout = timeout if wait_callbacks else -1
        for handler in handlers:
            handler.wait(timeout, wait_callbacks_timeout=callback_timeout)
