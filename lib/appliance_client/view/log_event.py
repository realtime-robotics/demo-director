from enum import Enum, auto


class LogEvent(Enum):
    STARTED = auto()
    STOPPED = auto()
    SENT = auto()
    RECEIVED = auto()
