from typing import Any, Callable, Optional

from ..view.log_event import LogEvent

MsgCallback = Optional[Callable[[Any], None]]
ErrCallback = Optional[Callable[[Exception], None]]
Action = Callable[[], None]
LogCallback = Callable[[LogEvent, Any], None]

BIG_INT = 2_147_483_647
