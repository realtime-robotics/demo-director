from .aliases import LogCallback

ENCODING: str = "ASCII"
DELIMITER: str = '\r\n'
DELIMITER_BYTES: bytes = '\r\n'.encode(ENCODING)
DEFAULT_BUFFER_SIZE: int = 65536
DEFAULT_POOL_SIZE: int = 1
DEFAULT_POLLING_PERIOD: float = 0.005

NO_ACTION_LOG: LogCallback = lambda l, d: None
