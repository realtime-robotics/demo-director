from __future__ import annotations

from typing import Any, Literal, Optional, Union

from .messages.message import Message
from .router import Router
from .socket_io.socket_client_pool import SocketClientPool
from .view.aliases import LogCallback, MsgCallback
from .view.defaults import DEFAULT_BUFFER_SIZE, DEFAULT_POOL_SIZE, DELIMITER_BYTES, NO_ACTION_LOG
from .view.exceptions import ApplianceClientException
from .view.handler import Handler
from .view.log_event import LogEvent


class ApplianceClient():
    def __init__(self,
                 ip: str,
                 port: int,
                 buffer_size: int = DEFAULT_BUFFER_SIZE,
                 client_pool_size: int = DEFAULT_POOL_SIZE,
                 log_callback: LogCallback = NO_ACTION_LOG) -> None:
        if (client_pool_size <= 0):
            raise ApplianceClientException()
        if (buffer_size <= 0):
            raise ApplianceClientException()
        client_pool: SocketClientPool = SocketClientPool(ip, port, DELIMITER_BYTES, buffer_size,
                                                         client_pool_size)
        self._router: Router = Router(client_pool, log_callback=log_callback)
        self._log: LogCallback = log_callback

    def start(self) -> None:
        self._router.start()
        self._log(LogEvent.STARTED, None)

    def stop(self) -> None:
        self._router.stop()
        self._log(LogEvent.STOPPED, None)

    def send_msg(self,
                 msg: Message,
                 recv_callback: MsgCallback = None,
                 id: Optional[Union[str, int]] = None) -> Handler:
        return self._router.send_message(msg, recv_callback, id)

    def __enter__(self) -> ApplianceClient:
        '''
        Magic method necessary to be able to use the with context manager. 
        Returns the class instance.
        '''
        self.start()
        return self

    def __exit__(self, *args: Any, **kwargs: Any) -> Literal[False]:
        '''
        Magic method necessary to be able to use the with context manager. 
        Deletes the instance when leaving the with context.
        Since the method does not handle exceptions, just propagates them, it should return False.
        '''
        self.stop()
        del self
        return False
