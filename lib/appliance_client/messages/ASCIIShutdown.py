from dataclasses import dataclass
from typing import Any

from .message import Message


@dataclass
class ASCIIShutdown(Message):
    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIShutdown.get_topic()}

    @staticmethod
    def get_topic() -> str:
        return "Shutdown"
