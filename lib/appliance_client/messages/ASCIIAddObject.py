from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIAddObject(Message):
    object_name: Optional[str] = None
    object_type: Optional[str] = None
    parent_frame: Optional[str] = None
    offset: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIAddObject.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "AddObject"
