from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIRS_FindHeight(Message):
    filter_id: Optional[str] = None
    centroid: Optional[List[float]] = None
    width: Optional[float] = None
    length: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIRS_FindHeight.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "RS_FindHeight"