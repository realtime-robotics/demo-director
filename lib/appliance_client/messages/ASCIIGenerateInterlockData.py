from dataclasses import dataclass
from typing import Any

from .message import Message


@dataclass
class ASCIIGenerateInterlockData(Message):
    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGenerateInterlockData.get_topic()}

    @staticmethod
    def get_topic() -> str:
        return "GenerateInterlockData"
