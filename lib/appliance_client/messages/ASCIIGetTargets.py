from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIGetTargets(Message):
    robot_name: Optional[str] = None
    preset_name: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetTargets.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "GetTargets"
