from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIGetTCPPose(Message):
    robot_name: Optional[str] = None
    ref_frame: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetTCPPose.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "GetTCPPose"
