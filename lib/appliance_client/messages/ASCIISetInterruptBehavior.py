from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetInterruptBehavior(Message):
    robot_name: Optional[str] = None
    timeout: Optional[float] = None
    max_replans: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetInterruptBehavior.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetInterruptBehavior"
