from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIRemoveConnection(Message):
    robot_name: Optional[str] = None
    autoconnect_name: Optional[str] = None
    preset: Optional[str] = None
    targets: Optional[List[str]] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIRemoveConnection.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "RemoveConnection"
