from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetMaxLinearSpeed(Message):
    robot_name: Optional[str] = None
    max_speed: Optional[float] = None
    reset: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetMaxLinearSpeed.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetMaxLinearSpeed"
