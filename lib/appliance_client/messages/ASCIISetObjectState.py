from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetObjectState(Message):
    object_name: Optional[str] = None
    state_name: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetObjectState.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetObjectState"
