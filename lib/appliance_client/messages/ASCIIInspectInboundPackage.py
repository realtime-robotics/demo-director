from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIInspectInboundPackage(Message):
    conveyor: Optional[str] = None
    package_id: Optional[str] = None
    package_size: Optional[object] = None
    package_orientation: Optional[str] = None
    package_type: Optional[str] = None
    content_type: Optional[str] = None
    edge_reduce_top_size: Optional[object] = None
    edge_reduce_bottom_size: Optional[object] = None
    place_location: Optional[object] = None
    place_orientation: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIInspectInboundPackage.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "InspectInboundPackage"
