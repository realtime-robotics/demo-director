from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIDeactivateRobot(Message):
    robot_name: Optional[str] = None
    treat_as_obstacle: Optional[bool] = None
    preset_name: Optional[str] = None
    joint_config: Optional[List[float]] = None
    target_name: Optional[str] = None
    use_last_known_config: Optional[bool] = None
    project_name: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIDeactivateRobot.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "DeactivateRobot"
