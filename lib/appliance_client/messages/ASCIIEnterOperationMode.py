from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIEnterOperationMode(Message):
    connect: Optional[bool] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIEnterOperationMode.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "EnterOperationMode"
