from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIMove(Message):
    robot_name: Optional[str] = None
    pose: Optional[object] = None
    target: Optional[str] = None
    joint_config: Optional[List[float]] = None
    speed: Optional[float] = None
    move_type: Optional[object] = None
    smoothing: Optional[float] = None
    interp: Optional[object] = None
    collision_check: Optional[object] = None
    collision_check_dsm: Optional[object] = None
    timeout: Optional[float] = None
    relative: Optional[str] = None
    ref_frame: Optional[str] = None
    preset_name: Optional[str] = None
    trigger: Optional[Any] = None
    external_axes: Optional[Any] = None
    shortest_path_only: Optional[bool] = None
    costs: Optional[Any] = None
    route: Optional[str] = None
    constraints: Optional[List[int]] = None
    roadmap_mode: Optional[int] = None
    revert_to_roadmap: Optional[bool] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIMove.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "Move"
