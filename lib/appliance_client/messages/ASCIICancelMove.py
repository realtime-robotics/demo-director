from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIICancelMove(Message):
    robot_name: Optional[str] = None
    acceleration_factor: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIICancelMove.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "CancelMove"
