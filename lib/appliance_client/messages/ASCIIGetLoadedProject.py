from dataclasses import dataclass
from typing import Any

from .message import Message


@dataclass
class ASCIIGetLoadedProject(Message):
    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetLoadedProject.get_topic()}

    @staticmethod
    def get_topic() -> str:
        return "GetLoadedProject"
