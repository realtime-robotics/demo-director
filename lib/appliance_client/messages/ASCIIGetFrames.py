from dataclasses import dataclass
from typing import Any

from .message import Message


@dataclass
class ASCIIGetFrames(Message):
    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetFrames.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "GetFrames"
