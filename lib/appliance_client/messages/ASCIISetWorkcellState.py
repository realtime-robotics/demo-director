from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetWorkcellState(Message):
    objects: Optional[object] = None
    robots: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetWorkcellState.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetWorkcellState"
