from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIICombinedMove(Message):
    robot_name: Optional[str] = None
    moves: Optional[object] = None
    preset_name: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIICombinedMove.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "CombinedMove"
