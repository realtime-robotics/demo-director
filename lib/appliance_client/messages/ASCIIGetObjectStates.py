from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIGetObjectStates(Message):
    object_name: Optional[str]

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetObjectStates.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "GetObjectStates"
