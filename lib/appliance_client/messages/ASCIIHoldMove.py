from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIHoldMove(Message):
    robot_name: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIHoldMove.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "HoldMove"
