from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIRS_AddExpectedObject(Message):
    filter_id: Optional[str] = None
    centroid: Optional[List[float]] = None
    length: Optional[float] = None
    width: Optional[float] = None
    height: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIRS_AddExpectedObject.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "RS_AddExpectedObject"