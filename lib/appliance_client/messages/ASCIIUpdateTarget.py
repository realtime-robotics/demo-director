from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIUpdateTarget(Message):
    robot_name: Optional[str] = None
    target_name: Optional[str] = None
    retrigger_autoconnect: Optional[object] = None
    object_states: Optional[object] = None
    pose: Optional[object] = None
    joint_config: Optional[object] = None
    frame: Optional[object] = None
    validation: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIUpdateTarget.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "UpdateTarget"
