from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetResponseType(Message):
    response_type: Optional[str] = None
    set_ip_default: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetResponseType.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetResponseType"
