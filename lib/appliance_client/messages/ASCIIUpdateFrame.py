from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIUpdateFrame(Message):
    frame_name: Optional[str] = None
    offset: Optional[str] = None
    pose: Optional[str] = None
    reference_frame: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIUpdateFrame.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "UpdateFrame"
