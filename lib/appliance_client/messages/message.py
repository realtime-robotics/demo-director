from __future__ import annotations

from abc import abstractmethod
from typing import Any, Protocol

from ..view.exceptions import MessageException


class Message(Protocol):
    @abstractmethod
    def to_msg(self) -> Any:
        raise NotImplementedError()

    @staticmethod
    @abstractmethod
    def get_topic() -> str:
        raise NotImplementedError()

    @staticmethod
    def is_last(msg: Any) -> bool:
        if msg["type"] == "DelayedResponse":
            return True
        if "error" in msg:
            return True
        if "data" in msg and not msg["data"]:
            return True
        if "data" in msg and "seq" not in msg["data"]:
            return True
        return False

    @staticmethod
    def is_feedback(msg: Any) -> bool:
        return bool(msg.get("type", "") == "Feedback")

    @staticmethod
    def is_response(msg: Any) -> bool:
        return bool(msg.get("type", "") == "Response")

    @staticmethod
    def is_delayed_response(msg: Any) -> bool:
        return bool(msg.get("type", "") == "DelayedResponse")

    @staticmethod
    def is_trigger_feedback(msg: Any) -> bool:
        return Message.is_feedback(msg) and "trigger" in msg.get("data", {})

    @staticmethod
    def has_error(msg: Any) -> bool:
        return "error" in msg

    @staticmethod
    def get_error_code(msg: Any) -> int:
        if not Message.has_error(msg):
            raise MessageException(msg, "Message has no error code")
        return int(msg.get("error").get("code"))
