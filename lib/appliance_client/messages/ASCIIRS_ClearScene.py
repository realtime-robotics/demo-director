from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIRS_ClearScene(Message):
    filter_id: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIRS_ClearScene.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "RS_ClearScene"
