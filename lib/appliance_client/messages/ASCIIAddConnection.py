from dataclasses import dataclass
from typing import Any, List, Optional

from .message import Message


@dataclass
class ASCIIAddConnection(Message):
    robot_name: Optional[str] = None
    preset: Optional[str] = None
    targets: Optional[List[str]] = None
    type: Optional[str] = None
    autoconnect_name: Optional[str] = None
    validation: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIAddConnection.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "AddConnection"
