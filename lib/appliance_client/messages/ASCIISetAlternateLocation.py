from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetAlternateLocation(Message):
    robot_name: Optional[str] = None
    enabled: Optional[object] = None
    mode: Optional[object] = None
    target_name: Optional[str] = None
    pose: Optional[str] = None
    complete_move: Optional[object] = None
    timeout: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetAlternateLocation.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetAlternateLocation"
