from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIGetJointConfiguration(Message):
    robot_name: Optional[str] = None
    native: Optional[bool] = None
    with_joint_names: Optional[bool] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIGetJointConfiguration.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "GetJointConfiguration"
