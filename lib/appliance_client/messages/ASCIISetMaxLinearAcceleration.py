from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIISetMaxLinearAcceleration(Message):
    robot_name: Optional[str] = None
    max_accel: Optional[float] = None
    reset: Optional[object] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIISetMaxLinearAcceleration.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "SetMaxLinearAcceleration"
