from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIIsOnTarget(Message):
    robot_name: Optional[str] = None
    target_name: Optional[str] = None
    epsilon_rot: Optional[float] = None
    epsilon_lin: Optional[float] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIIsOnTarget.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "IsOnTarget"
