from dataclasses import dataclass
from typing import Any, Optional

from .message import Message


@dataclass
class ASCIIQueryClearance(Message):
    package_id: Optional[str] = None

    def validate_msg(self) -> bool:
        # Validate message is currently not implemented
        return True

    def to_msg(self) -> Any:
        return {"topic": ASCIIQueryClearance.get_topic(), "data": vars(self)}

    @staticmethod
    def get_topic() -> str:
        return "QueryClearance"
