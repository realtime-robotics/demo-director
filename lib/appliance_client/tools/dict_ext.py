from typing import Any


def deep_clear_nones(orig: Any) -> None:
    for k, v in orig.copy().items():
        if v is None:
            del orig[k]
        elif type(v) is dict:
            deep_clear_nones(v)
