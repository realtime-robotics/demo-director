from queue import Queue
from threading import Thread
from typing import Optional

from ..view.aliases import Action, ErrCallback
from ..view.exceptions import ApplianceClientException


class Scheduler():
    def __init__(self, on_error: ErrCallback = None) -> None:
        self._action_queue: Queue[Action] = Queue()
        self._worker: Thread = Thread(target=self._do_work, daemon=True)
        self._running: bool = False
        self._accepting: bool = False
        self._on_error = on_error

    def start(self) -> None:
        if self._running:
            raise ApplianceClientException("Cannot start, already running")

        self._accepting = True
        self._running = True
        self._worker.start()

    def add_work(self, action: Action) -> None:
        if not self._accepting:
            raise ApplianceClientException("Cannot add work, not accepting")

        self._action_queue.put(action)

    def stop(self) -> None:
        if not self._accepting:
            raise ApplianceClientException("Cannot stop, stop already requested")

        self._accepting = False

        def _stop() -> None:
            self._running = False

        self._action_queue.put(_stop)

    def wait(self, timeout: Optional[float] = None) -> bool:
        self._worker.join(timeout)
        return not self._worker.is_alive()

    def _do_work(self) -> None:
        while self._running:
            try:
                action = self._action_queue.get()
                action()
            except Exception as e:
                # TODO: log error
                if (self._on_error):
                    self._on_error(e)
