#!/usr/bin/env python3

from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException
from lib.appliance_client.messages.ASCIIEnterConfigurationMode import ASCIIEnterConfigurationMode
from lib.appliance_client.messages.ASCIIEnterOperationMode import ASCIIEnterOperationMode
from lib.Utils import initialize_logger, FilePaths, InfoLabel, MessageDialog
from lib.appliance_client.messages.ASCIIClearFaults import ASCIIClearFaults
from PyQt5.QtWidgets import QWidget, QFileDialog, QMessageBox, QApplication
from lib.appliance_client.messages.ASCIIConnect import ASCIIConnect
from lib.appliance_client.appliance_client import ApplianceClient
from lib.RuntimeControllerDialog import RuntimeControllerDialog
from lib.PythonRESTCommander import PythonRESTCommander
from lib.ProjectLoadDialog import LoadProjectDialog
from lib.InterlockManager import InterlockManager
from lib.GlobalVariables import GlobalVariables
from PyQt5.QtCore import pyqtSignal, QTimer, Qt
from lib.sick_plb.sick_plb import SickPLBCamera
from lib.sick_plb.sick_plb import SickPLBCamera
from ui.task_manager import Ui_TaskManager
from ui.main_window import Ui_MainWindow
from PyQt5.QtGui import QKeyEvent
from lib.TaskPlan import TaskPlan
from datetime import datetime
from lib.Robot import Robot
import os, sys, json, uuid
from typing import List

# Module Architecture
# TaskManager -> TaskPlan -> Robot -> Task

DEMO_DIRECTOR_VERSION = '2.10.0'

class TaskManager(QWidget):
    add_task_plan_sig = pyqtSignal(TaskPlan)
    clear_task_plans = pyqtSignal(str)
    remove_menu_item = pyqtSignal(TaskPlan, int)
    speed_override_change = pyqtSignal()
    key_pressed = pyqtSignal(QKeyEvent)
    config_loaded = pyqtSignal(bool, str, str)
    unsaved_changes = pyqtSignal(bool)
    refresh_menu_bar_enabled_states = pyqtSignal()
    set_progress_signal = pyqtSignal(int, str)

    def __init__(self, main_window_ui:Ui_MainWindow):
        super().__init__()
        self.ui = Ui_TaskManager()
        self.ui.setupUi(self)
        self.setFocusPolicy(Qt.StrongFocus)
        self.file_paths = FilePaths()
        self.logger = initialize_logger()
        self.main_window_ui = main_window_ui

        # Runtime controller dialog to connect to the Realtime Robotics Runtime Controller
        self.runtime_controller_dialog = RuntimeControllerDialog(
            self, parent=self)
        self.runtime_controller_dialog.ui.connect_commanders_button.clicked.connect(
            self.connect_commanders)
        # Current mode of the appliance. Updated by a thread polling the State rest endpoint
        self.mode: str = "DISCONNECTED"
        self.previous_mode = None
        self.get_mode_polling_hz: int = 4
        # The name of the currently loaded project on the runtime controller
        # self.loaded_project: str = None
        # Boolean storing the connection status to the runtime controller
        self.connected: bool = False
        # List of task plans created. All task plans show up as a MenuItem on the DemoDirector page
        self.task_plans: List[TaskPlan] = []
        # Default task plans path to be loaded when the ProjectLoadWorker finishes
        self.default_task_plans_path: str = None
        # Variables containing the task plan config name and full file path
        self.loaded_config_name: str = None
        self.loaded_config_path: str = None
        # Varaibles storing if the task plan is currently running aka the execute method is still alive
        self.running: bool = False
        self.running_uuid_list = []
        # Speed override slider to scale the move command speeds
        self.ui.speed_slider.valueChanged.connect(lambda: self.get_speed_override(
            self.ui.speed_slider.value()))  # Slider updates as value changes
        self.ui.speed_slider.valueChanged.connect(self.update_slider_label)
        self.speed_override = self.ui.speed_slider.value() / 100
        self.update_slider_label()
        # Global variables management dialog
        self.global_variables = GlobalVariables(parent=self)
        self.project_robots: List[Robot] = []

        # Connect the tab signals to the apropriate methods
        self.ui.task_plans_tab_widget.currentChanged.connect(
            self.prevent_selecting_new_task_plan_tab)
        self.ui.task_plans_tab_widget.tabBarClicked.connect(
            self.task_plan_tab_selected)

        # If connection cannot be established from cached ip address, add the
        # new task plan tab which contains an info label telling the user to
        # connect to the runtime controller
        self.add_new_task_plan_tab()
        # Update the mode label
        self.update_mode_label()

        # plug-ins
        self.sick_plb_cameras: List[SickPLBCamera] = []

    def load_cached_data(self):
        # DemoDirector can cache the appliance IP address, project name, and task plans config file to
        # apply on application launch.
        if "connection_cache.json" in os.listdir(f'{self.file_paths.lib_path}'):
            # Load the cached data.
            with open(f'{self.file_paths.lib_path}connection_cache.json', 'r') as fp:
                data = json.load(fp)
        else:
            self.logger.info(f"Creating connection cache.")
            data = {"ip_address": None, "project_name": None,
                    "task_plans_path": None}
            with open(f'{self.file_paths.lib_path}connection_cache.json', 'w') as fp:
                json.dump(data, fp)

        # Connect with cached ip address automatically
        self.set_progress_signal.emit(
            40, "Connecting to RTR controller...")
        if data['ip_address'] != None:
            if ':' in data['ip_address']:
                ip, port = data['ip_address'].split(':')
                self.runtime_controller_dialog.ui.web_server_port_label.setText(
                    port)
            else:
                ip = data['ip_address']
            self.runtime_controller_dialog.ui.controller_ip_label.setText(ip)

        self.connect_commanders()
        if not self.connected:
            self.logger.error(
                f'Failed to connect to the runtime controller. Loading cached data aborted.')
            return

        project_name = data['project_name']
        # If a project name is cached, and that project is not loaded, then call the loaded_project_dialogs
        # load_project method, and override the combobox with the cached project name
        self.set_progress_signal.emit(
            60, "Loading cached project...")
        if project_name:
            if self.load_project_dialog.loaded_project == None:
                # Ask for user confirmation before loading the default project
                confirmation = MessageDialog('Default Project Found',f'Project [{project_name}] is the default project, would you like to load it?', parent=self)
                ret_val = confirmation.exec()
                if ret_val == QMessageBox.No:
                    self.logger.info(f"Confirmation denied, so not loading cached project [{project_name}].")
                    return
                self.logger.info(f"Loading default project [{project_name}]")
                self.load_project_dialog.load_project(
                    project_name=project_name)
                self.load_project_dialog.loading_default_project = True
                self.refresh_projects()

        task_plans_path = data['task_plans_path']
        # If the project was already loaded, call load_task_plans now
        # if the project is not loaded, store the task plan config file name and load_task_plans will
        # be called in the finished_loading method. You cannot load task_plans without a project loaded.
        if task_plans_path:
            self.logger.info(f"Loading default task plans [{task_plans_path}]")
            self.default_task_plans_path = task_plans_path
            if self.load_project_dialog.loaded_project:
                self.load_task_plans(path=task_plans_path)
                # Set the default path back to None, so that they will only be loaded on application launch and not subsequent manual
                # load calls
                self.default_task_plans_path = None
            if self.loaded_config_name == None:
                self.logger.info(
                    f"Detected a failed load, so skipping the rest of the cached data workflow.")
                return
            self.main_window_ui.action_save_as_default.setChecked(True)
        self.unsaved_changes.emit(False)

    def update_tab_titles(self, text):
        for idx, task_plan in enumerate(self.task_plans):
            self.ui.task_plans_tab_widget.setTabText(idx, task_plan.title)
        self.unsaved_changes.emit(True)

    def remove_task_plan_tabs(self):
        count = self.ui.task_plans_tab_widget.count()
        for idx in range(count):
            self.ui.task_plans_tab_widget.removeTab(0)
        self.task_plans: List[TaskPlan] = []
        self.add_new_task_plan_tab()

    def add_new_task_plan_tab(self):
        if not self.connected:
            info_label = InfoLabel(
                f"Connect to the Runtime Controller from the menu bar.")
        elif self.load_project_dialog.loading_default_project:
            info_label = InfoLabel(f"Loading default project...")
        elif self.load_project_dialog.loading:
            info_label = InfoLabel(f"Project load in progress...")
        elif self.load_project_dialog.loaded_project == None:
            info_label = InfoLabel(f"Load a Project from the menu bar.")
        else:
            info_label = InfoLabel(f"Click the '+' to add a Task Plan.")
        self.ui.task_plans_tab_widget.addTab(info_label, '+')

    def poll_get_mode(self):
        try:
            state = self.rest_commander.get_state()
            self.mode = state
            if self.mode != self.previous_mode:
                self.logger.info(f'Appliance mode change detected.')
                self.refresh_menu_bar_enabled_states.emit()
            self.previous_mode = self.mode
        except:
            self.logger.error(f'Failed to get state from runtime controller.')
            return
        self.update_mode_label()

        if self.mode != 'OPERATION':
            for task_plan in self.task_plans:
                if not task_plan.is_stopped():
                    self.logger.warning(f'Stopping task plans since not in OPERATION mode.')
                    task_plan.stop_robots()

    def update_mode_label(self):
        self.ui.mode_label.setText(f' {self.mode} ')
        if self.mode == 'CONFIG':
            color = '#fffea8'  # light yellow
        elif self.mode == 'OPERATION':
            color = '#acffa8'  # light greed
        elif self.mode == 'FAULT':
            color = '#ffa8a8'  # light red
        else:
            color = '#ffffff'  # white
        self.ui.mode_label.setStyleSheet(
            f"background-color: {color}; border-radius: 4px")

    def connect_commanders(self, cached_connect=False):
        if self.connected:
            self.logger.info(f"Already connected to the runtime controller.")
            return

        ip = self.runtime_controller_dialog.ui.controller_ip_label.text()
        port = self.runtime_controller_dialog.ui.web_server_port_label.text()
        self.ip_address = ip + ':' + port
        self.logger.info(
            f'Attempting to connect to RTR Controller at {self.ip_address}')

        # Commander Setup
        try:
            self.rest_commander = PythonRESTCommander(ip + ":" + port)

            # Check that the rapidplan version major and minor versions match the demo director
            # major and minor versions
            version = self.rest_commander.get_rapidplan_version()
            self.logger.info(f"Rapidplan version: {version} Demo Director Version: {DEMO_DIRECTOR_VERSION}")
            if version[0:3] != DEMO_DIRECTOR_VERSION[0:3]:
                self.logger.error(f"Version mismatch between demo director and rapidplan control.")
                text = f"Demo Director expects RapidPlan Control version {DEMO_DIRECTOR_VERSION} but detected version {version}.\n"\
                    f"Compaibility is not guarenteed unless versions match, would you like to proceed?"
                mismatch_dialog = MessageDialog("Version Mismatch",text,parent=self)
                ret_val = mismatch_dialog.exec()
                if ret_val == QMessageBox.No:
                    return

            self.appliance_client = ApplianceClient(
                ip, 9999, client_pool_size=1)
            # Dialog for the installed appliance projects dialog
            self.load_project_dialog = LoadProjectDialog(ip, port, parent=self)
            self.load_project_dialog.finished_loading_signal.connect(
                self.finished_loading)
            self.load_project_dialog.refresh_projects_signal.connect(
                self.refresh_projects)

            # Get robot names from active project
            if self.load_project_dialog.loaded_project:
                robot_names: List = self.rest_commander.get_robot_names(
                    self.load_project_dialog.loaded_project)
                robot_names.sort()
                self.logger.info(f'Found robot names: {robot_names}')
                for robot_name in robot_names:
                    self.project_robots.append(
                        Robot(robot_name=robot_name, ip_address=self.ip_address, global_variables=self.global_variables))

            # make sure that the appliance can be reached at this IP and port
            # a source build needs localhost:3000
            state = self.rest_commander.get_state()
            self.logger.info(f"Rest commander initialized with state: {state}")

            self.connected = True
            self.get_mode_timer = QTimer()
            self.get_mode_timer.timeout.connect(self.poll_get_mode)
            self.get_mode_timer.start(int(1000 / self.get_mode_polling_hz))
            # Call poll_get_mode manually, to ensure that the menu bar states are up to date when the signal
            # is emitted at the end of this method
            self.poll_get_mode()

            self.runtime_controller_dialog.ui.controller_ip_label.setEnabled(False)
            self.runtime_controller_dialog.ui.web_server_port_label.setEnabled(False)
            self.runtime_controller_dialog.hide()

            self.logger.info(f'Successfully connected to the RTR Controller at: {self.ip_address}')
            # Update the cache with the last successful IP address
            with open(f'{self.file_paths.lib_path}connection_cache.json', 'r') as fp:
                data = json.load(fp)
            with open(f'{self.file_paths.lib_path}connection_cache.json', 'w') as fp:
                data['ip_address'] = self.ip_address
                json.dump(data, fp)
        except Exception as e:
            self.logger.error(f'Failed to connect to RTR Controller with exception {e}!')
            self.rest_commander = None
            self.load_project_dialog = None
            self.connected = False
            self.runtime_controller_dialog.ui.controller_ip_label.setEnabled(
                True)
            self.runtime_controller_dialog.ui.web_server_port_label.setEnabled(
                True)
            text = "Failed to connect to the runtime controller. Ensure the IP address "\
                "and port number are correct and that the runtime controller is on the same network."
            QMessageBox.critical(self, "Connection Error", text)
            return

        # self.refresh_menu_bar_enabled_states.emit()
        # Get project data over the REST interface
        self.refresh_projects()
        self.refresh_menu_bar_enabled_states.emit()

    def disconnect_commanders(self):
        if not self.connected:
            self.logger.error(
                f'Tried to disconnect without connecting to the Runtime controller.')
            return
        self.logger.info(f'Disconnecting from RTR Controller...')
        self.get_mode_timer.stop()

        self.rest_commander = None
        self.load_project_dialog = None
        self.connected = False
        self.runtime_controller_dialog.ui.controller_ip_label.setEnabled(True)
        self.runtime_controller_dialog.ui.web_server_port_label.setEnabled(
            True)
        self.mode = 'DISCONNECTED'
        self.update_mode_label()
        self.refresh_menu_bar_enabled_states.emit()
        self.remove_task_plan_tabs()
        self.clear_loaded_configuration()
        self.project_robots = []

    def refresh_projects(self):
        # Get all project data from the appliance, and update the UI elements.
        if not self.connected:
            return
        # If a project is loaded, switch to the unload button state,
        # and set the projects combobox to display the loaded project.
        self.remove_task_plan_tabs()

    def finished_loading(self):
        # Called by the LoadProjectWorker when the LoadProject delayed response
        # has come in. Make the load button enabled again, since it will now become
        # the unload button.
        self.load_project_dialog.loading_default_project = False

        # Get robot names from active project
        robot_names: List = self.rest_commander.get_robot_names(
            self.load_project_dialog.loaded_project)
        robot_names.sort()
        self.logger.info(f'Found robot names: {robot_names}')
        for robot_name in robot_names:
            self.project_robots.append(
                Robot(robot_name=robot_name, ip_address=self.ip_address, global_variables=self.global_variables))

        self.refresh_menu_bar_enabled_states.emit()
        self.refresh_projects()
        if self.default_task_plans_path:
            self.logger.info(f"Loading default task plans.")
            self.load_task_plans(path=self.default_task_plans_path)
            # Set the default path back to None, so that they will only be loaded on application launch and not subsequent manual
            # load calls
            self.default_task_plans_path = None

    def prevent_selecting_new_task_plan_tab(self):
        # If the user uses the mouse wheel to scroll between tabs, prevent them from seeing
        # the empty 'New Task Plan Tab'
        count = self.ui.task_plans_tab_widget.count()
        idx = self.ui.task_plans_tab_widget.currentIndex()
        if idx == count - 1:
            self.ui.task_plans_tab_widget.setCurrentIndex(count - 2)

    def task_plan_tab_selected(self, idx):
        # If the '+' tab is selected, create a new task plan, otherwise
        # just display the existing taskplan's tab
        if not self.connected:
            return
        elif self.load_project_dialog.loaded_project == None:
            return

        count = self.ui.task_plans_tab_widget.count()
        if idx == count - 1:
            self.new_task_plan()

    def add_task_plan(self, task_plan: TaskPlan):
        # Connect signals
        task_plan.title_changed_signal.connect(self.update_tab_titles)
        task_plan.ui.delete_task_plan_button.clicked.connect(
            self.remove_task_plan)
        task_plan.ui.duplicate_task_plan_button.clicked.connect(
            self.duplicate_task_plan)
        task_plan.unsaved_changes.connect(self.relay_unsaved_changes)
        task_plan.running_state.connect(self.update_play_buttons)
        # task_plan.unsaved_changes.connect(self.relay_unsaved_changes)
        # task_plan.running_state.connect(self.update_play_buttons)
        task_plan.switch_task_plans_request_sig.connect(self.switch_task_plans)
        task_plan.stop_task_plans_request_sig.connect(self.stop_task_plans)
        self.connect_robots_signals(task_plan)

        # add task plan to tab widget
        self.ui.task_plans_tab_widget.insertTab(
            self.ui.task_plans_tab_widget.count() - 1, task_plan, task_plan.title)
        self.ui.task_plans_tab_widget.setCurrentIndex(
            self.ui.task_plans_tab_widget.count() - 1)

        # add task plan to task plans list
        self.task_plans.append(task_plan)
        self.unsaved_changes.emit(True)

    def switch_task_plans(self, title: str):
        bin1_title = "Bin 1"
        bin2_title = "Bin 2"
        current_task_plan = None
        for task_plan in self.task_plans:
            if task_plan.title == title:
                current_task_plan = task_plan
                break
        if not current_task_plan:
            self.logger.error(
                "No active task plan detected when switching task plans...")
            return
        if current_task_plan.title == bin1_title:
            next_task_plan_title = bin2_title
        if current_task_plan.title == bin2_title:
            next_task_plan_title = bin1_title
        current_task_plan.stop_robots()
        for idx, task_plan in enumerate(self.task_plans):
            if task_plan.title == next_task_plan_title:
                Robot.no_move_available_1 = False
                Robot.no_move_available_2 = False
                Robot.no_move_available_3 = False
                Robot.no_parts_detected = False
                # always set no move availalbe to True for RV4 if running bin1
                if task_plan.title == bin1_title:
                    Robot.no_move_available_3 = True
                self.logger.debug(
                    f"Switching task plans to: {task_plan.title}")
                self.ui.task_plans_tab_widget.setCurrentIndex(idx)
                task_plan.execute()
                break

    def stop_task_plans(self, title: str):
        for task_plan in self.task_plans:
            if title != task_plan.title:
                task_plan.stop_robots()

    def new_task_plan(self):
        if not self.connected:
            return
        elif self.load_project_dialog.loaded_project == None:
            return

        # Create an empty task plan with the title TaskPlan# where # is the number of existing task plans plus one.
        count = self.ui.task_plans_tab_widget.count()
        title = f'TaskPlan{count}'
        self.logger.info(f"Adding task plan: {title}")
        # Make clones of project robots so that each task plan has its own copy
        new_robots = []
        for robot in self.project_robots:
            new_robots.append(robot.clone())
        self.add_task_plan(TaskPlan(title=title, description='Task Plan Description',
                           ip_address=self.ip_address, robots=new_robots, global_variables=self.global_variables))
        self.unsaved_changes.emit(True)

    def remove_task_plan(self):
        # Removes the current selected tab
        if len(self.task_plans) == 0:
            return

        # Get the current task plan tab index and title
        idx = self.ui.task_plans_tab_widget.currentIndex()
        task_plan_title = self.task_plans[idx].title

        # Get confirmation that the user wants to delete the task plan
        confirmation = MessageDialog('Delete Task Plan Confirmation',f"You are about to delete task plan [{task_plan_title}], would you like to proceed?", parent=self)
        ret_val = confirmation.exec()
        if ret_val == QMessageBox.No:
            self.logger.info(f"Confirmation denied, so not deleting task plan [{task_plan_title}].")
            return
        self.logger.info(f'Removing task plan: {idx}')

        # Remove the tab widget, emit a signal saying the task plan was deleted, pop the task plan from the task plans list
        self.ui.task_plans_tab_widget.removeTab(idx)
        self.remove_menu_item.emit(self.task_plans[idx], idx)
        self.task_plans.pop(idx)

        # Emit a signal that there are now unsaved changes
        self.unsaved_changes.emit(True)

    def duplicate_task_plan(self):
        # Takes the current selected tab, creates a copy of it, and appends " copy" to the title
        if len(self.task_plans) == 0:
            return

        idx = self.ui.task_plans_tab_widget.currentIndex()
        duplicate_count = 0
        for task_plan in self.task_plans:
            if self.task_plans[idx].title in task_plan.title:
                duplicate_count += 1
        new_task_plan = self.task_plans[idx].clone()
        new_task_plan.title = f'{self.task_plans[idx].title} ({duplicate_count})'
        self.logger.info(
            f"Duplicating task plan: {self.task_plans[idx].title}")
        self.add_task_plan(new_task_plan)
        self.unsaved_changes.emit(True)

    def default_task_plans_directory_check(self):
        # If there is no default task plans path, create one
        if not os.path.exists(self.file_paths.task_plans_path):
            self.logger.info(f"Creating task plans directory.")
            os.mkdir(self.file_paths.task_plans_path)
            fp = open(
                f"{self.file_paths.task_plans_path}.place_task_plans_here", 'w')
            fp.close()

    def config_name_from_path(self, path):
        # Retrieve the task plan config name from the path returned by the file dialog and set it as the window title
        if sys.platform == 'win32':
            # Split the path by the delimiter, and take the last element which should be <name>.json
            task_plan_config = path.split('\\')[-1]
            # Split by the '.' which leaves you with the user specified name
            task_plan_config = task_plan_config.split('.')[0]
        elif sys.platform == 'linux':
            # Split the path by the delimiter, and take the last element which should be <name>.json
            task_plan_config = path.split('/')[-1]
            # Split by the '.' which leaves you with the user specified name
            task_plan_config = task_plan_config.split('.')[0]
        return task_plan_config

    def clear_loaded_configuration(self):
        # Clear the name and path of the loaded task plans, and then emit the signal to update the window title
        self.loaded_config_name = None
        self.loaded_config_path = None
        self.config_loaded.emit(False, '', '')

    def new_project(self,bypass_confirmation=False):
        # Create a new project by deleting all task plans and removing all tabs
        if not bypass_confirmation:
            confirmation = MessageDialog('New Project Confirmation',f"By creating a new project, any unsaved changes will be lost. Would you like to proceed?", parent=self)
            ret_val = confirmation.exec()
            if ret_val == QMessageBox.No:
                self.logger.info(f"Confirmation denied, so not creating a new project.")
                return
        
        self.logger.info(f'Creating a new project.')
        self.task_plans: List[TaskPlan] = []
        self.remove_task_plan_tabs()
        self.clear_loaded_configuration()

    def load_task_plans(self, path=None):
        # Check if the default directory exists, and create one if it doesnt
        selected_path = path
        self.default_task_plans_directory_check()

        # If a specific path was not specified, launch a file dialog
        if not selected_path:
            # Open a specific json file that stores task plans
            selected_path, filter = QFileDialog.getOpenFileName(
                self, 'Open Task Plans Save', f'{self.file_paths.task_plans_path}', "Save Files (*.json)")
            if not selected_path:
                return

        # Try to load the json file selected by the user or from the connection cache
        self.logger.info(f'Loading Task Plans at: {selected_path}')
        try:
            with open(selected_path, 'r') as fp:
                data = json.load(fp)
        except FileNotFoundError:
            self.logger.error(f"Task plans file could not be opened.")
            text = f"Task plan file failed to open.\nEither the file has been deleted or the path has changed."
            QMessageBox.critical(self, "Load Task Plans Failed", text)
            return
        except json.JSONDecodeError as e:
            self.logger.error(
                f"Task plans file failed to deserialize with error: {e}")
            text = f"Task plan failed to load with error:\n{e}"
            QMessageBox.critical(self, "Load Task Plans Failed", text)
            return
        except:
            self.logger.error(f"Unhandled exception when loading task plans.")
            return

        # Check that task plans project name matches the loaded project name
        if not data['project_name'] == self.load_project_dialog.loaded_project:
            text = f"Currently loaded project [{self.load_project_dialog.loaded_project}] does not match\nthe project for the saved Task Plans [{data['project_name']}]!"
            self.logger.error(text)
            QMessageBox.critical(self, "Load Task Plans Failed", text)
            return

        # Check if the demo director version key exists and meets the minimum required version.
        if 'demo_director_version' not in list(data.keys()):
            text = f"Task plans do not meet the required minimum Demo Director version >1.0.0"
            self.logger.error(text)
            QMessageBox.critical(self, "Load Task Plans Failed", text)
            return

        # Since load will overwrite everything, delete all task plans, task plan tabs
        # and Menu Items
        self.new_project(bypass_confirmation=True)

        # store the name of the task plan, and emit the name over the config loaded signal
        self.loaded_config_name = self.config_name_from_path(selected_path)
        self.loaded_config_path = selected_path
        self.config_loaded.emit(True, self.loaded_config_name, self.loaded_config_path)

        # Populate the task_plans dictionary
        for idx, task_plan in enumerate(data['task_plans']):
            self.logger.info(f'Found task plan [{task_plan["title"]}]')
            # Validate the task plan data
            if not self.validate_task_plan_dict(task_plan):
                text = f'Task plan [{task_plan["title"]}] failed to validate.'
                self.logger.error(text)
                QMessageBox.critical(self, "Load Task Plans Failed", text)
                self.remove_task_plan_tabs()

            # Build the robots
            robots: List[Robot] = []
            self.logger.info(
                f'Building robots for task plan [{task_plan["title"]}]')
            for robot in task_plan["robots"]:
                new_robot = Robot(
                    robot_name=robot["robot_name"], ip_address=self.ip_address, global_variables=self.global_variables)
                self.logger.info(f'Robot created: {new_robot.robot_name}')
                for count, task in enumerate(robot["task_list"]):
                    try:
                        task_type = robot["task_list"][count]['type']
                        task_parameters = robot["task_list"][count]['parameters']
                        if 'uuid' in robot["task_list"][count]:
                            copy_uuid = robot["task_list"][count]['uuid']
                        else:
                            copy_uuid = str(uuid.uuid4())
                        self.logger.debug(
                            f'Adding task from json:\n\t{copy_uuid}\n\t{task_type}\n\t{task_parameters}')
                        new_robot.add_task_internal(
                            task_type, task_parameters, new_uuid=copy_uuid)
                    except KeyError:
                        text = f'Missing or invalid key in Task object for [{task_plan["title"]}] -> {robot["robot_name"]} -> Task {count}'
                        self.logger.error(text)
                        QMessageBox.critical(
                            self, "Load Task Plans Failed", text)
                        self.remove_task_plan_tabs()
                        return
                robots.append(new_robot)

            # Add the task plan
            try:
                # Default interlock values to false when loading from saved file
                default_interlocks = {
                    key: 'False' for key in task_plan["interlocks"]}
                interlock_manager = InterlockManager(
                    interlocks=default_interlocks)
                self.add_task_plan(TaskPlan(title=task_plan["title"], description=task_plan["description"],
                                            loop=task_plan["loop"], sync_loop_start_time=task_plan["sync_loop_start_time"],
                                            ip_address=self.ip_address, robots=robots, interlock_manager=interlock_manager, global_variables=self.global_variables))
            except KeyError as e:
                text = f'Missing or invalid key {e.args[0]} in TaskPlan object for [{task_plan["title"]}]'
                self.logger.error(text)
                QMessageBox.critical(self, "Load Task Plans Failed", text)
                self.remove_task_plan_tabs()
                return

        self.ui.task_plans_tab_widget.setCurrentIndex(0)

        # Add the global variables
        if 'global_variables' in data:
            self.logger.info(
                f'Found global variables: {data["global_variables"]}')
            for key in data['global_variables'].keys():
                value = data['global_variables'][key]
                self.global_variables.add_variable(
                    key, value)
                self.logger.debug(f'Added global variable {key}: {value}')
        self.unsaved_changes.emit(False)

    def save_task_plans_as(self):
        self.save_task_plans(save_as=True)

    def save_task_plans(self, save_as=False):
        if not self.connected:
            self.logger.error(f"Must be connected to the Runtime Controller.")
            return
        elif self.load_project_dialog.loaded_project == None:
            self.logger.error(
                f"Project must be loaded in order to save Task Plans!")
            return

        # Check if the default directory exists, and create one if it doesn't
        self.default_task_plans_directory_check()

        if not self.loaded_config_name or save_as:
            # Launch a file dialog to get the save directory and file name
            path, filter = QFileDialog.getSaveFileName(
                self, 'Save Task Plans', f'{self.file_paths.task_plans_path}', "Save Files (*.json)")
            if path == '':
                return
            if not '.json' in path:
                path = path + '.json'
            self.loaded_config_name = self.config_name_from_path(path)
            self.loaded_config_path = path
            self.config_loaded.emit(True, self.loaded_config_name, self.loaded_config_path)
        else:
            path = self.loaded_config_path

        # Update the cache with the project name and task plans path
        with open(f'{self.file_paths.lib_path}connection_cache.json', 'r') as fp:
            data = json.load(fp)
        with open(f'{self.file_paths.lib_path}connection_cache.json', 'w') as fp:
            # If save as default checkbox is checked, update the cache with the project name and task plans path
            if self.main_window_ui.action_save_as_default.isChecked():
                self.logger.info(
                    f"Writing project name and task plans path to the cache.")
                data['project_name'] = self.load_project_dialog.loaded_project
                data['task_plans_path'] = path
            # If save as default checkbox is NOT checked, clear the project name and task plans path from the cache
            else:
                self.logger.info(
                    f"Clearing project name and task plans path from the cache.")
                data['project_name'] = None
                data['task_plans_path'] = None
            json.dump(data, fp)

        # You need to manually convert python->json types for json.loads() for some reason. All booleans in the task plans list
        # use integers instead to prevent save/load errors
        self.logger.info(f'Saving task plans at: {path}')
        with open(path, 'w') as fp:
            task_plans_dict = {"project_name": self.load_project_dialog.loaded_project,
                               "save_date": str(datetime.now()),
                               "rapidplan_version": self.rest_commander.get_rapidplan_version(),
                               "demo_director_version": DEMO_DIRECTOR_VERSION,
                               "task_plans": json.loads(str(self.task_plans).replace("'", '"').replace(
                                   "True", "true").replace("False", "false").replace("None", "null")),
                               "global_variables": json.loads(str(self.global_variables).replace("'", '"').replace(
                                   "True", "true").replace("False", "false").replace("None", "null"))}
            json.dump(task_plans_dict, fp, indent=4)
        self.unsaved_changes.emit(False)

    def enter_operation_mode(self):
        if not self.connected:
            self.logger.error(
                f'Tried to enter operation mode without connecting to the Runtime controller.')
            return
        self.logger.info('Entering operation mode...')
        self.appliance_client.start()
        message = ASCIIConnect()
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(30)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        handler.close_handler()

        message = ASCIIEnterOperationMode()
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(30)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        handler.close_handler()
        self.appliance_client.stop()

    def enter_config_mode(self):
        if not self.connected:
            self.logger.error(
                f'Tried to enter config mode without connecting to the Runtime controller.')
            return
        self.logger.info('Entering config mode...')
        self.appliance_client.start()
        message = ASCIIEnterConfigurationMode()
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(30)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        handler.close_handler()
        self.appliance_client.stop()

    def clear_faults(self):
        self.logger.info('Clearing faults...')
        self.appliance_client.start()
        message = ASCIIClearFaults()
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(30)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        handler.close_handler()
        self.appliance_client.stop()

    def get_speed_override(self, value):
        # Connected to speed slider on MainMenu
        self.speed_override = value / 100
        # Repeat signal with robot's set_speed_override connected
        self.speed_override_change.emit()

    def keyPressEvent(self, event: QKeyEvent):
        modifiers = QApplication.keyboardModifiers()
        key = event.key()
        if key == Qt.Key_N:
            self.new_task_plan()
            self.prevent_selecting_new_task_plan_tab()
        else:
            # self.key_pressed.emit(event)
            super().keyPressEvent(event)

    def relay_unsaved_changes(self, is_unsaved):
        self.unsaved_changes.emit(is_unsaved)

    def update_slider_label(self):
        value = self.ui.speed_slider.value()
        if value < 10:
            text = f"    {value}"
        elif value < 100:
            text = f"  {value}"
        else:
            text = f"{value}"
        text += "%"
        self.ui.speed_override_label.setText(text)

    def update_play_buttons(self, state: bool, uuid: str):
        # Enable/Disable the play buttons within the TaskManager when it goes to a running state
        def set_states():
            for task_plan in self.task_plans:
                task_plan.ui.play_task_plan.setEnabled(not state)
                for robot in task_plan.robots:
                    robot.ui.play_button.setEnabled(not state)
                    robot.ui.step_forward_button.setEnabled(not state)
                    robot.ui.step_back_button.setEnabled(not state)
                task_plan.ui.loop_task_plan_checkbox.setEnabled(not state)
                task_plan.ui.sync_loop_start_checkbox.setEnabled(not state)
                if not state:
                    # If the task plan is stopping, only re-enable the sync loop
                    # checkbox if the loop checkbox is checked
                    task_plan.loop_task_plan_update()
        
        if state:  # Add uuid if running and disable play button
            self.running_uuid_list.append(uuid)
            set_states()
        # Remove uuid from list and check if list is empty.  Re-enable play buttons if empty.
        else:
            if uuid in self.running_uuid_list:
                self.running_uuid_list.remove(uuid)
            if len(self.running_uuid_list) == 0:
                set_states()

    def open_global_variables(self):
        self.global_variables.show()

    def connect_robots_signals(self, task_plans: TaskPlan):
        for robot in task_plans.robots:
            self.speed_override_change.connect(
                lambda a=robot: a.set_speed_override(self.speed_override))
            robot.speed_override = self.speed_override
            robot.key_pressed.connect(self.keyPressEvent)

    def validate_task_plan_dict(self, task_plan: dict) -> bool:
        # check that robots are in project
        robot_names = []
        # populate robot names from project
        for robot in self.project_robots:
            robot_names.append(robot['robot_name'])
        # compare cached robot names to project robot names
        for robot in task_plan['robots']:
            if robot['robot_name'] not in robot_names:
                self.logger.error(
                    f"Robot {robot['robot_name']} does not exist in project")
                return False
        return True
