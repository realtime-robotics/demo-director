#!/usr/bin/env python3
import datetime
import enum
import time
import re
import yaml
from os import SEEK_END, SEEK_CUR
from typing import Callable, Tuple, Union
from rtr_appliance import PythonASCIICommander
from lib.Utils import initialize_logger

#RTR_TIMESTAMP_REGEX = "(?<=\[)[^][]*(?=\])"
RTR_TIMESTAMP_REGEX = "[0-9]{4}-[0-9]{2}-[0-9]{2}T[0-9]{2}:[0-9]{2}:[0-9]{2}"
SIEMENS_TIMESTAMP_REGEX = "(?<=\<)[^><]*(?=\>)"



class TimeFmt(enum.Enum):
    RTR = 0
    EPOCH = 1
    SIEMENS = 2
    RTR2 = 3
    RTR3 = 4
    
class CmdData:
        timestamp = None
        cmd = None

        def __init__(self, t, cmd):
            self.timestamp = t
            self.cmd = cmd

        def __repr__(self):
            return f"[{self.timestamp}] {self.cmd}"

# Timestamp Formats
def datetime_from_rtr(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from RTR human-readable string"""
    try:
        
        return datetime.datetime.strptime(dstr, '%Y-%m-%d %H:%M:%S.%f %z')
        #return datetime.datetime.strptime(dstr, '%d/%m/%Y %H:%M:$S %p %z')
    except:
        return None

def datetime_from_epoch(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from epoch time"""
    try:
        return datetime.datetime.fromtimestamp(float(dstr))
    except:
        return None

def datetime_from_siemens(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object from Siemens timestamp string"""
    try:
        return datetime.datetime.strptime(dstr, "%Y.%m.%d %H:%M:%S.%f")
    except:
        return None
    RTR = 0
    EPOCH = 1
    SIEMENS = 2

def datetime_from_rtr_2(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from RTR human-readable string"""
    try:
        return datetime.datetime.strptime(dstr, '%Y-%m-%d %H:%M:%S')
        #return datetime.datetime.strptime(dstr, '%Y-%m-%d %H:%M:%S.%f %z')
        #return datetime.datetime.strptime(dstr, '%d/%m/%Y %H:%M:$S %p %z')
    except:
        return None

def datetime_from_rtr_3(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from RTR human-readable string"""
    try:
        return datetime.datetime.strptime(dstr, '%Y-%m-%dT%H:%M:%S')
    except:
        return None

def find_ascii_cmd(log_line: str, is_siemens_logs: bool) -> Union[str, None]:
    """Returns ASCII command string given a line from log file"""
    if is_siemens_logs and re.search("Sending command", log_line) is not None:
        return re.search("\{.*\}", log_line)[0]
    elif re.search("ASCII Command", log_line):
        match = re.search("\{.*\}", log_line)

        # Invalid ASCII commands can show up in logs
        if match is not None:
            return match[0]

    return None


def find_timestamp_fmt(log_line: str,
                    timestamp_regex: str) -> Tuple[Union[CmdData, None], Union[Callable, None]]:
    """Determines/validates the timestamp format

    Args:
        log_line (str): line of text from log file
        timestamp_regex (str): regex

    Returns:
        Tuple(TimeFmt, index): time format and regex index

    """
    matches = re.findall(timestamp_regex, log_line)

    for i, m in reversed(list(enumerate(matches))):
        # Try every format and return the type that succeeded + match index
        for fmt, converter in GET_TIME.items():
            if converter(m):
                return (fmt, i)
    return (None, None)

# Mapping from time format to a converter function
GET_TIME = {
    TimeFmt.RTR: datetime_from_rtr,
    TimeFmt.EPOCH: datetime_from_epoch,
    TimeFmt.SIEMENS: datetime_from_siemens,
    TimeFmt.RTR2: datetime_from_rtr_2,
    TimeFmt.RTR3: datetime_from_rtr_3
}

class LogFile:
    def __init__(self, manager=None, filename=None, dry_run=False, is_siemens_log=False, range=None, ip_address='localhost', port=9999):
        
        self.fpath = filename
        self.log_file = None
        self.dry_run = dry_run
        self.is_siemens_logs = is_siemens_log
        self.time_range = []
        self.ip_address = ip_address
        self.port = port
        self.logger = initialize_logger()
        self.status_message = ""
        self.manager = manager
        self.manager.set_status_message("Open Log File To Begin")

        # Init variables
        self.datetime_range = None
        self.timestamp_regex = RTR_TIMESTAMP_REGEX
        if self.is_siemens_logs:
            self.timestamp_regex = SIEMENS_TIMESTAMP_REGEX
        self.start_datetime = None
        self.end_datetime = None


        # Collect commands
        self.file_read = False
        self.cmd_sequence = []
        self.time_fmt = None
        self.match_idx = None

        self.stop = False
        self.pause = False
        self.send_all = False
        
        self.waiting = False
    
    def open_log_file(self):
        self.log_file = open(self.fpath, "r")
        self.status_message = "File Opened"
        self.manager.set_status_message("Select Date Range and then press Load Log")

    def get_date_range_from_file(self):
        line = self.log_file.readline()
        # Initialize/Setup time format and match index
        if self.time_fmt is None or self.match_idx is None:
            fmt, idx = find_timestamp_fmt(line, self.timestamp_regex)
            self.time_fmt = fmt
            self.logger.info(f'Time format: {self.time_fmt}')
            self.match_idx = idx

            # If we could not find either, then abort script
            if self.time_fmt is None or self.match_idx is None:
                print(f"Failed to parse line: {line}")
                return
            
        first_timestamp = re.findall(self.timestamp_regex, line)[self.match_idx]
        # Convert to datetime object
        self.start_datetime = GET_TIME[self.time_fmt](first_timestamp)
        for line in self.log_file:
            pass

        last_timestamp = re.findall(self.timestamp_regex, line)[self.match_idx]
        # Convert to datetime object
        self.end_datetime = GET_TIME[self.time_fmt](last_timestamp)


        self.logger.info(f'Log file start: {self.start_datetime}. Log File end: {self.end_datetime}')

        return [self.start_datetime, self.end_datetime]
    
    def set_date_range(self, start_datetime, end_datetime):
        self.start_datetime = start_datetime
        self.end_datetime = end_datetime

        self.time_range = [self.start_datetime, self.end_datetime]

    def load_logs(self):
        self.logger.info(f"Reading File")
        self.manager.set_status_message(f'Loading Logs from: {self.start_datetime} to {self.end_datetime}')
        self.cmd_sequence = []
        #Open File
        with open(self.fpath, "r") as self.log_file:

            #Iterate through all the lines
            for line in self.log_file:
                
                #Check if this is an ASCII Command
                cmd = find_ascii_cmd(line, self.is_siemens_logs)
                if cmd:

                    # Initialize/Setup time format and match index
                    if self.time_fmt is None or self.match_idx is None:
                        fmt, idx = find_timestamp_fmt(line, self.timestamp_regex)
                        self.time_fmt = fmt
                        self.match_idx = idx

                        # If we could not find either, then abort script
                        if self.time_fmt is None or self.match_idx is None:
                            print(f"Failed to parse line: {line}")
                            return

                        # Set time range
                        if self.time_range:
                            if (len(self.time_range) != 2):
                                print(
                                    f"Error invalid range input: {self.time_range}. Must be 2 length array.")
                                return
                            datetime_range = [GET_TIME[self.time_fmt](t) for t in self.time_range]

                    date_str = re.findall(self.timestamp_regex, line)[self.match_idx]

                    # Convert to datetime object
                    d_obj = GET_TIME[self.time_fmt](date_str)

                    # Check if datetime is out of range
                        
                    if d_obj > self.end_datetime:
                        break
                    if d_obj < self.start_datetime:
                        continue
                    # Extract ascii command
                    dict_cmd = yaml.safe_load(cmd)
                    self.cmd_sequence.append(CmdData(d_obj, dict_cmd))
                    
        self.file_read = True
        self.manager.set_status_message("Logs Loaded")

    def playback(self):
        # Playback Commands
        print("Begining Log Playback")
        self.manager.set_status_message(f"Begining Log Playback")
        cmdr = None

        if not self.file_read:
            self.read_file()

        if not self.dry_run:
            cmdr = PythonASCIICommander.PythonASCIICommander(self.ip_address, self.port)
        for i in range(1, len(self.cmd_sequence) + 1):
            curr_cmd_data = self.cmd_sequence[i - 1]
            next_cmd_data = None
            if i < len(self.cmd_sequence):
                next_cmd_data = self.cmd_sequence[i]

            tdelta = 0
            if next_cmd_data:
                tdelta = (next_cmd_data.timestamp - curr_cmd_data.timestamp).total_seconds()

            if not self.dry_run:
                self.manager.set_last_command(curr_cmd_data.cmd)

                self.manager.set_list_row(i-1)
                command_result =cmdr.send_raw_yaml(curr_cmd_data.cmd, delay_timeout=10)

                self.manager.set_last_response(command_result.response_dict)


                if self.send_all:
                    if tdelta > 5:
                        tdelta=5
                    self.manager.set_status_message(f"Sleep duration before next command: {tdelta}s\n")
                    wait_start = datetime.datetime.now()
                    cur_time = datetime.datetime.now()
                    while ((cur_time-wait_start).total_seconds() < tdelta) and not self.stop:
                        cur_time = datetime.datetime.now()
                        self.waiting = True
                    self.waiting = False
                else:
                    print(f"Sleep duration before next command: {tdelta}s\n")
                    self.manager.set_status_message(f"Sleep duration before next command: {tdelta}s\n")
                    wait_start = datetime.datetime.now()
                    cur_time = datetime.datetime.now()
                    while ((cur_time-wait_start).total_seconds() < tdelta) and not self.stop:
                        cur_time = datetime.datetime.now()
                        self.waiting = True
                    print(f'(Time Waited: {cur_time - wait_start}')
                    self.waiting = False

            if self.stop:
                self.logger.info(f"Stop Flag Reached in Thread")
                self.manager.set_status_message("Playback Stopped")
                self.stop = False
                return
            
        self.logger.info(f"Playback Complete")
        self.manager.set_status_message(f"Log Playback Done")
    
    def set_stop(self):
        self.stop = True

    def set_pause(self):
        self.pause = True