#!/usr/bin/env python3

from ui.runtime_controller_dialog import Ui_RuntimeControllerDialog
from lib.Utils import initialize_logger
from PyQt5.QtWidgets import QDialog
from PyQt5 import uic

class RuntimeControllerDialog(QDialog):

    def __init__(self, task_manager, parent=None):
        super().__init__(parent)
        self.ui = Ui_RuntimeControllerDialog()
        self.ui.setupUi(self)
        self.logger = initialize_logger()
        self.task_manager = task_manager
