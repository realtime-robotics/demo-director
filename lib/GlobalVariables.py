#!/usr/bin/env python3

from PyQt5.QtWidgets import QDialog, QTableWidgetItem
from PyQt5 import QtCore
from ui.global_variables import Ui_GlobalVariables
from typing import Any, List
from lib.Utils import initialize_logger


class GlobalVariables(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.logger = initialize_logger()
        self.ui = Ui_GlobalVariables()
        self.ui.setupUi(self)

        self.ui.add_variable_button.clicked.connect(self.add_row)
        self.ui.delete_variable_button.clicked.connect(self.delete_row)

    def add_variable(self, name: str, value: Any):
        matched_items = self.ui.global_variables_table.findItems(
            name, QtCore.Qt.MatchFlag.MatchExactly)
        if len(matched_items) > 1:
            raise Exception(
                f'More than 1 instance of {name} already exists in the Global Variable list')
        if matched_items:
            self.logger.info(f'Updating variable {name} to {value}')
            row_position = self.ui.global_variables_table.row(matched_items[0])
        else:
            row_position = self.ui.global_variables_table.rowCount()
            self.ui.global_variables_table.insertRow(row_position)
        self.ui.global_variables_table.setItem(
            row_position, 0, QTableWidgetItem(name))
        self.ui.global_variables_table.setItem(
            row_position, 1, QTableWidgetItem(value))

    def add_row(self):
        self.ui.global_variables_table.setRowCount(
            self.ui.global_variables_table.rowCount() + 1)

    def delete_row(self):
        self.ui.global_variables_table.removeRow(
            self.ui.global_variables_table.currentRow())

    def get_variables(self) -> list:
        var_list = []
        for row in range(self.ui.global_variables_table.rowCount()):
            item = self.ui.global_variables_table.item(row, 0)
            var = item.text() if item is not None else ''
            var_list.append(var)
        return var_list

    def get_value(self, var: str) -> Any:
        var_list = self.get_variables()
        if var in var_list:
            return self.ui.global_variables_table.item(var_list.index(var), 1).text()
        else:
            self.logger.error(
                f'Variable {var} does not exist in Global Variable list')

    def clear_all(self):
        self.ui.global_variables_table.clearContents()
        self.ui.global_variables_table.setRowCount(0)
        self.logger.info('Cleared all global variables')

    def asdict(self):
        global_variables_dict = {}
        variables = self.get_variables()
        for var in variables:
            global_variables_dict.update({var: self.get_value(var)})
        return global_variables_dict

    def __repr__(self) -> str:
        return str(self.asdict())

    def __str__(self) -> str:
        return str(self.asdict())
