#!/usr/bin/env python3

from PyQt5.QtWidgets import QLabel, QDialog, QMessageBox
from logging.handlers import RotatingFileHandler
from ui.about_dialog import Ui_AboutDialog
from PyQt5.QtCore import Qt
import pathlib, sys
import logging

class InfoLabel(QLabel):
    def __init__(self, text):
        super().__init__()
        self.setText(text)
        self.setAlignment(Qt.AlignCenter)
        self.setStyleSheet("font: 18pt Open Sans;")

class MessageDialog(QMessageBox):
    def __init__(self, title: str, text: str, icon_type=QMessageBox.Question, parent=None):
        super().__init__(parent)

        self.setWindowTitle(title)
        self.setStyleSheet("font: 12pt Open Sans;")

        self.setText(text)
        self.setStandardButtons(QMessageBox.Yes | QMessageBox.No)
        self.setIcon(icon_type)

class FilePaths(object):
    if sys.platform == 'win32':
        user_path = str(pathlib.Path().absolute()) + '\\'
        lib_path = user_path + 'lib\\'
        ui_path = user_path + 'ui\\'
        font_path = ui_path + 'fonts\\'
        task_plans_path = user_path + 'task_plans\\'
    elif sys.platform == 'linux':
        user_path = str(pathlib.Path().absolute()) + '/'
        lib_path = user_path + 'lib/'
        ui_path = user_path + 'ui/'
        font_path = ui_path + 'fonts/'
        task_plans_path = user_path + 'task_plans/'
    else:
        raise Error('OS not recognized!')
    
def set_logging_level(log_level=logging.INFO) -> None:
    logger = logging.getLogger("DemoDirector")
    logger.setLevel(log_level)
    logger.info(f'Changing logging level to: {log_level}')

def initialize_logger(log_level=logging.DEBUG) -> logging.Logger:
    file_paths = FilePaths()
    logger = logging.getLogger("DemoDirector")

    if len(logger.handlers) == 0:
        logger.setLevel(log_level)
        max_size = 1024 * 1024 * 10  # 10Mb
        path = f'{file_paths.user_path}DemoDirector.log'
        formatter = logging.Formatter(
            '[%(asctime)s.%(msecs)03d] (%(filename)s:%(lineno)d) %(levelname)s - %(message)s', datefmt="%Y-%m-%d %H:%M:%S")

        file_handler = RotatingFileHandler(
            path, maxBytes=max_size, backupCount=5)
        file_handler.setFormatter(formatter)

        console_handler = logging.StreamHandler()
        console_handler.setFormatter(formatter)

        logger.addHandler(console_handler)
        logger.addHandler(file_handler)
        logger.info(f"Console and File loggers initialized.")

    return logger

def initialize_move_time_logger(robot_name, log_level=logging.INFO) -> logging.Logger:
    file_paths = FilePaths()
    logger = logging.getLogger("MoveTimeLogger")

    if len(logger.handlers) == 0:
        logger.setLevel(log_level)
        max_size = 1024 * 1024 * 10  # 10Mb
        path = f'{file_paths.user_path}MoveTimes.log'
        formatter = logging.Formatter('%(asctime)s.%(msecs)03d %(message)s', datefmt="%Y-%m-%d %H:%M:%S")
        file_handler = RotatingFileHandler(path, maxBytes=max_size, backupCount=5)
        logger.addHandler(file_handler)
        logger.info(f"\ndate time robot-name goal move-type speed smoothing(mm) planning-time(s) execution-time(s)")
        file_handler.setFormatter(formatter)

    return logger

def initialize_height_check_logger(log_level=logging.INFO) -> logging.Logger:
    file_paths = FilePaths()
    logger = logging.getLogger("HeightCheckLogger")

    if len(logger.handlers) == 0:
        logger.setLevel(log_level)
        max_size = 1024 * 1024 * 10  # 10Mb
        path = f'{file_paths.user_path}HeightCheckResults.csv'
        formatter = logging.Formatter('%(asctime)s.%(msecs)03d,%(message)s', datefmt="%Y-%m-%d %H:%M:%S")
        file_handler = RotatingFileHandler(path, maxBytes=max_size, backupCount=5)
        logger.addHandler(file_handler)
        logger.info(f"\ndate-time,name,execution time (s),height (mm),expected height (mm)")
        file_handler.setFormatter(formatter)

    return logger

class AboutDialog(QDialog):
    def __init__(self, parent=None):
        super().__init__(parent)
        self.ui = Ui_AboutDialog()
        self.ui.setupUi(self)
