#!/usr/bin/env python3

from PyQt5.QtWidgets import QTableWidgetItem, QDialog
from ui.cycle_time_details import Ui_CycleTimeDetails
from lib.Utils import FilePaths, initialize_logger
from PyQt5.QtWidgets import QFileDialog
from PyQt5.QtGui import QColor
from PyQt5.QtCore import Qt

class CycleDetailsDialog(QDialog):

    def __init__(self, robot_name, parent=None):
        super().__init__(parent)
        self.logger = initialize_logger()
        self.file_paths = FilePaths()
        self.ui = Ui_CycleTimeDetails()
        self.ui.setupUi(self)
        self.setWindowTitle(f'{robot_name} Cycle Times')

        self.robot_name = robot_name
        self.cycle_count = 0
        self.cycle_times = []
        
        self.ui.clear_cycle_times_button.clicked.connect(self.clear_cycle_times)
        self.ui.export_csv_button.clicked.connect(self.export_csv)
        self.clear_cycle_times()

    def export_csv(self):
        path, filter = QFileDialog.getSaveFileName(self, 'Save Cycle Times', f'{self.file_paths.user_path}', "*.csv")
        if path == '':
            self.logger.info(f"no path specified, so not saving cycle times as CSV.")
            return
        if not '.csv' in path:
            path = path + '.csv'
        
        self.logger.info(f'Saving cycle times for robot {self.robot_name} at: {path}')
        with open(path,'w') as fp:
            fp.write(f'Cycle Number,Cycle Time\n')
            table = self.ui.cycle_time_table
            cycle_time_column = 1
            for row in range(table.rowCount()):
                cycle_count = table.item(row, 0).text()
                cycle_time = table.item(row, cycle_time_column).text()
                fp.write(f'{cycle_count},{cycle_time}\n')
    
    def get_table_item(self,text):
        table_item = QTableWidgetItem()
        table_item.setFlags(Qt.ItemFlag(0))  # make the item non-editable
        table_item.setForeground(QColor("black"))
        table_item.setTextAlignment(4)
        table_item.setText(str(text))
        return table_item

    def add_cycle_time(self, cycle_time):
        self.cycle_count += 1
        self.cycle_times.insert(0,float(cycle_time))
        table = self.ui.cycle_time_table
        # Add cycle item
        table.insertRow(0)
        cycle_item = self.get_table_item(self.cycle_count)
        table.setItem(0, 0, cycle_item)
        # add time item (yes, they need to be uniquely defined QTableWidgetItem)
        time_item = self.get_table_item(cycle_time)
        table.setItem(0, 1, time_item)

        # Update other labels in dialog
        average = round(sum(self.cycle_times)/len(self.cycle_times),3)
        if len(self.cycle_times) > 10:
            average_10 = round(sum(self.cycle_times[0:10])/10.0,3)
        else:
            average_10 = average

        self.ui.avg.setText(f'{average} s')
        self.ui.last_10_avg.setText(f'{average_10} s')
        self.ui.cycle_count.setText(str(self.cycle_count))

    def clear_cycle_times(self):
        self.cycle_count = 0
        self.cycle_times = []
        self.ui.cycle_time_table.setRowCount(0)
        self.ui.cycle_count.setText('0')
        self.ui.last_10_avg.setText(f'0.000 s')
        self.ui.avg.setText(f'0.000 s')