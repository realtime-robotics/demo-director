#!/usr/bin/env python3

from lib.appliance_client.view.exceptions import MessageException, MessageTimeoutException, NoReplyException
from lib.appliance_client.messages.ASCIIUnloadProject import ASCIIUnloadProject
from lib.appliance_client.messages.ASCIILoadProject import ASCIILoadProject
from lib.appliance_client.appliance_client import ApplianceClient
from ui.load_project_dialog import Ui_LoadProjectDialog
from lib.PythonRESTCommander import PythonRESTCommander
from PyQt5.QtCore import pyqtSignal, QTimer, QThread
from PyQt5.QtCore import pyqtSignal, QThread
from lib.Utils import initialize_logger
from PyQt5.QtWidgets import QDialog
from PyQt5.QtCore import QObject
from PyQt5 import QtGui
import time


class LoadProjectDialog(QDialog):
    finished_loading_signal = pyqtSignal()
    refresh_projects_signal = pyqtSignal()

    def __init__(self, ip, port, parent=None):
        super().__init__(parent)
        self.ui = Ui_LoadProjectDialog()
        self.ui.setupUi(self)
        self.logger = initialize_logger()
        self.setWindowTitle('Load RPC Project')

        self.appliance_client = ApplianceClient(ip, 9999)
        self.rest_commander = PythonRESTCommander(ip + ":" + port)

        self.loading = False
        self.loading_default_project = False
        # Timer used to animate the load button animation
        self.load_bullets: int = 0
        self.load_spinner_timer = QTimer()
        self.load_spinner_timer.timeout.connect(self.load_spinner)

        self.loaded_project = self.rest_commander.get_loaded_project()
        if self.loaded_project:
            self.logger.info(f'Found loaded project: {self.loaded_project}')
        else:
            self.logger.info('No loaded project found')
        installed_projects = self.rest_commander.get_installed_project_names()
        self.ui.installed_projects_dropdown.addItems(installed_projects)

        self.ui.load_project_button.clicked.connect(self.load_project)

    def showEvent(self, a0: QtGui.QShowEvent) -> None:
        if not self.loaded_project:
            self.ui.installed_projects_dropdown.setEnabled(True)
            self.ui.load_project_button.setEnabled(True)
        else:
            self.ui.installed_projects_dropdown.setEnabled(False)
            self.ui.load_project_button.setEnabled(False)
        return super().showEvent(a0)

    def load_spinner(self):
        load_str = ' ' * self.load_bullets + 'Loading' + '.' * self.load_bullets
        # self.load_unload_project_button.setText(load_str)
        self.load_bullets += 1
        if self.load_bullets > 3:
            self.load_bullets = 0

    def load_project(self, project_name=None):
        # The load project method by default will load the current project name selected from the combobox,
        # but if a cached project name is to be loaded then the optional project_name argument will override it
        # The type of project name must be checked, since the QPushButton connected to this method emits a bool
        # which is incorrectly interpreted as the value of project name.
        if project_name != None and type(project_name) != bool:
            if project_name in self.rest_commander.get_installed_project_names():
                self.selected_project = project_name
                idx = self.ui.installed_projects_dropdown.findText(
                    self.selected_project)
                self.ui.installed_projects_dropdown.setCurrentIndex(idx)
            else:
                self.logger.info(f'Project {project_name} is not installed!')
                return
        else:
            self.selected_project = self.ui.installed_projects_dropdown.currentText()

        self.loading = True
        self.load_thread = QThread()
        self.load_worker = LoadWorker()
        self.load_worker.moveToThread(self.load_thread)
        self.load_thread.started.connect(self.load_worker.run)
        self.load_worker.finished.connect(self.finished_loading)
        self.load_worker.finished.connect(self.load_thread.quit)
        self.load_worker.finished.connect(self.load_worker.deleteLater)
        self.load_thread.finished.connect(self.load_thread.deleteLater)

        self.logger.info(f'Loading project {self.selected_project}...')
        self.ui.installed_projects_dropdown.setEnabled(False)
        self.ui.load_project_button.setEnabled(False)
        self.load_worker.appliance_client = self.appliance_client
        self.load_worker.project = self.selected_project
        self.load_thread.start()
        # Emit the refresh projects signal so the info label will display 'loading project...'
        self.refresh_projects_signal.emit()

    def finished_loading(self, result, project_name):
        self.loading = False
        self.loading_default_project = False
        self.loaded_project = self.selected_project
        self.finished_loading_signal.emit()
        self.hide()

    def unload_project(self):
        self.appliance_client.start()
        message = ASCIIUnloadProject()
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(60)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        handler.close_handler()
        self.appliance_client.stop()
        # Since unload_project does not return a delayed response, wait in a while
        # loop until we confirm that the project has unloaded
        while not self.loaded_project == None:
            self.loaded_project = self.rest_commander.get_loaded_project()
            time.sleep(0.1)
        self.refresh_projects_signal.emit()


class LoadWorker(QObject):
    '''
        This class is a QThread object. When called, this class will attempt to load a project in the background.
        When finished, a signal is emitted to indicate the result and project name.
    '''
    finished = pyqtSignal(int, str)

    def __init__(self):
        super().__init__()
        self.appliance_client: ApplianceClient = None
        self.project = None
        self.logger = initialize_logger()

    def run(self):
        self.appliance_client.start()
        message = ASCIILoadProject(self.project)
        handler = self.appliance_client.send_msg(message)
        try:
            handler.wait(30)
        except MessageException as e:
            topic = handler.original.get_topic()
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: {e.error_message}")
            self.finished.emit(0, self.project)
        except NoReplyException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: No reply received")
        except MessageTimeoutException:
            self.logger.error(
                f"{handler.original.get_topic()} ERROR: Message timed out")
        else:
            handler.close_handler()
            self.appliance_client.stop()
            time.sleep(0.5)
            self.finished.emit(1, self.project)
            self.logger.info('Load finished.')
