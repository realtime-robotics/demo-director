from PyQt5.QtWidgets import QWidget,QFileDialog
from PyQt5.QtCore import QDate, QDateTime
from lib.Utils import initialize_logger
from ui.log_manager import Ui_LogManager
from threading import Thread
from lib.Log import LogFile
import os

class LogManager(QWidget):

    def __init__(self):
        #Init logger
        self.logger = initialize_logger()
        self.logger.info("Setting up LogManager Widget")

        #Init super
        super().__init__()

        #UI Setup
        self.ui = Ui_LogManager()
        self.ui.setupUi(self)


        #Initialize some variables
        self.loop = False
        self.send_all = False
        self.ip_address = "localhost"

        #Setup playback stuff
        self.threads = None
        self.pause = False
        self.stop = False

        #Handle Loop Check Box
        '''self.ui.loop_log_checkbox.stateChanged.connect(
            self.loop_task_plan_update)
        if loop:
            self.ui.loop_log_checkbox.setChecked(True)'''

        #Handle Send all Checkbox
        self.ui.send_all_checkbox.stateChanged.connect(
            self.send_all_update)
        if self.send_all:
            self.ui.send_all_checkbox.setChecked(True)
        
        #Init Log Object
        self.log:LogFile = None
        self.status_message = ""

        #Connect Pushbutton
        self.ui.load_log_button.clicked.connect(self.load_log)
        self.ui.play_log_button.clicked.connect(self.start_replay)
        #self.ui.pause_log_button.clicked.connect(self.pause_robots)
        self.ui.stop_log_button.clicked.connect(self.stop_replay)

    def get_log(self):
        self.logger.info(f"Opening file explorer for user to select log file")
        
        #Use File Dialog to get filename
        filename = QFileDialog.getOpenFileName(self,'Open File')
        self.ui.fileNameLabel.setText(str(filename[0]))
        
        self.logger.info(f"Creating LogFile Object")
        self.log = LogFile(manager=self, filename = filename[0],
                        dry_run=False, 
                        is_siemens_log=False, 
                        range=None, 
                        ip_address='localhost', 
                        port=9999)
        
        self.logger.info(f"Reading Log File")
        self.log.open_log_file()

        #Get Date Range
        self.find_daterange()
    
    def find_daterange(self):
        #Get datetime range of Log File
        self.logger.info(f"Getting start and end time of Log File")
        [start_datetime, end_datetime] = self.log.get_date_range_from_file()
        self.ui.start_datetime.setDate(start_datetime.date())
        self.ui.start_datetime.setTime(start_datetime.time())
        self.ui.end_datetime.setDate(end_datetime.date())
        self.ui.end_datetime.setTime(end_datetime.time())
   
    def get_daterange(self):
        pass

    def load_log(self):
        start_datetime = self.ui.start_datetime.dateTime().toPyDateTime()
        end_datetime = self.ui.end_datetime.dateTime().toPyDateTime()

        self.logger.info(f'Reading Logs from: {start_datetime} to {end_datetime}')
        self.log.set_date_range(start_datetime, end_datetime)
        self.log.load_logs()
        
        self.ui.log_list_widget.clear()

        for i in range(1, len(self.log.cmd_sequence) + 1):
            cmd_data = self.log.cmd_sequence[i - 1]
            self.ui.log_list_widget.addItem(f"{cmd_data.timestamp} --> {cmd_data.cmd}")
    
    def send_all_update(self):
        value = self.ui.send_all_checkbox.isChecked()
        self.logger.info(f'Setting send all to {value}')

        if value:
            self.send_all = True
        else:
            self.send_all = False
        
        self.log.send_all = self.send_all

    def set_status_message(self, status_message):
        self.status_message = status_message
        self.ui.status_label.setText(f'Status: {self.status_message}')

    def set_last_command(self, command):
        self.ui.command_label.setText(f'Last Command {command}')

    def set_last_response(self, resposne):
        self.ui.response_label.setText(f'Last Response: {resposne}')
    
    def set_list_row(self, row):
        self.ui.log_list_widget.setCurrentRow(row)

    def start_replay(self):
        '''if self.thread != None:
            if self.thread.is_alive():
                self.logger.warn(
                    f"Active log thread detected, so new log [{self.title}] will not execute!")
                return'''
            
        # Reset the stop and pause flags
        self.log.stop = False

        # Start
        self.logger.info(f"**** Executing log replay. ****")
        
        self.thread = Thread(target= self.log.playback,name="log")
        self.thread.start()

    def stop_replay(self):
        self.logger.debug(f'Stopping log replay.')

        if self.thread.is_alive():
            self.log.set_stop()
            self.thread.join(5)

            self.logger.info(
                f'Log replay threads have stopped.')
        else:
            self.logger.info(
                f'Stop Requested but no log playing')