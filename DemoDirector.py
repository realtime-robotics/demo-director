#!/usr/bin/env python3

from PyQt5.QtWidgets import QApplication, QMainWindow, QStackedWidget, QWidget, QLabel, QDialog, QVBoxLayout, QProgressBar, QAction, QDesktopWidget
from lib.Utils import FilePaths, initialize_logger, AboutDialog, set_logging_level
from PyQt5.QtGui import QIcon, QFont, QFontDatabase, QKeyEvent, QPixmap
from lib.sick_plb.sick_plb import SickPLBCamera
from ui.sick_plug_in import Ui_SickPlugin
from ui.main_window import Ui_MainWindow
from lib.TaskManager import TaskManager
from lib.LogReplayUtility import LogReplayUtility
from PyQt5 import QtWidgets, QtCore
from typing import Optional
import sys, os, argparse
import logging, json
import faulthandler

class StartupSplashScreen(QDialog):

    def __init__(self, parent: Optional[QWidget] = None):
        super().__init__(parent=parent)
        self.file_paths = FilePaths()

        self.setWindowFlags(QtCore.Qt.WindowType.FramelessWindowHint)
        self.setStyleSheet(
            'background-color: #efefef; border-radius: 10px;')

        self.vbox = QVBoxLayout()

        # Setup icon
        self.icon = QLabel(self)
        pixmap = QPixmap(self.file_paths.ui_path +
                         'icons/demo_director_icon.png')
        pixmap = pixmap.scaledToHeight(100)
        self.icon.setPixmap(pixmap)
        self.icon.adjustSize()
        self.icon.setAlignment(QtCore.Qt.AlignmentFlag.AlignCenter)

        # Setup progress bar
        self.progress_bar = QProgressBar(self)
        self.progress_bar.setRange(0, 100)

        # Setup loading label
        self.label = QLabel("Loading...", self)
        self.label.setAlignment(QtCore.Qt.AlignmentFlag.AlignRight)

        self.vbox.addWidget(self.icon)
        self.vbox.addWidget(self.progress_bar)
        self.vbox.addWidget(self.label)

        self.setLayout(self.vbox)
        self.show()

    def set_progress(self, value: int, message: Optional[str] = None):
        self.progress_bar.setValue(value)
        if message:
            self.label.setText(message)

class DemoDirector(QMainWindow):
    '''
    This class creates the window, sets up the stacked layout, and handles all keyboard input.
    Parameters:
            screen_resolution (QRect): The screen resolution of the monitor the application is launched in
    '''
    def __init__(self, screen_resolution):
        faulthandler.enable()
        super().__init__()
        self.parse_args()

        self.file_paths = FilePaths()
        self.screen_resolution = [screen_resolution.size().width(), screen_resolution.size().height()]

        # Create the startup splash screen
        self.splash_screen = StartupSplashScreen(parent=self)
        self.splash_screen.set_progress(0, "Starting up...")

        self.ui = Ui_MainWindow()
        self.ui.setupUi(self)

        self._check_logging_arg()

        # Load the local fonts to application
        self.splash_screen.set_progress(10, "Loading GUI...")
        self.font_database = QFontDatabase()
        for filename in os.listdir(self.file_paths.font_path):
            f = os.path.join(self.file_paths.font_path, filename)
            if os.path.isfile(f):
                self.font_database.addApplicationFont(f)
        self.font = QFont("Open Sans")
        self.setFont(self.font)

        # Create the two widgets displayed to the user and add them to the stacked layout   
        self.is_unsaved = False
        self.is_default = False
        self.stackedWidget = QStackedWidget()
        self.task_manager = TaskManager(self.ui)

        self.task_manager.config_loaded.connect(self.update_window_title)
        self.task_manager.unsaved_changes.connect(self.update_unsaved_changes_status)
        self.task_manager.key_pressed.connect(self.keyPressEvent)
        self.task_manager.refresh_menu_bar_enabled_states.connect(self.set_enabled_states)
        self.stackedWidget.addWidget(self.task_manager)
        self.setCentralWidget(self.stackedWidget)
        self.task_manager.set_progress_signal.connect(self.set_progress_slot)

        # Connect menubar actions to appropriate methods
        self.ui.action_new_project.triggered.connect(self.task_manager.new_project)
        self.ui.action_save.triggered.connect(self.task_manager.save_task_plans)
        self.ui.action_save_as.triggered.connect(self.task_manager.save_task_plans_as)
        self.ui.action_load.triggered.connect(self.task_manager.load_task_plans)
        self.ui.action_save_as_default.toggled.connect(self.set_default_task_plans)
        self.ui.action_close_application.triggered.connect(self.shutdown)
        self.ui.actionASCII_Log_Replay.triggered.connect(self.ascii_log_replay)

        self.generate_recent_projects_menu()

        # Runtime Controller dialog
        self.ui.action_connect_to_rtr.triggered.connect(self.connect_to_rtr)
        self.ui.action_disconnect_from_rtr.triggered.connect(self.disconnect_from_rtr)
        self.ui.action_load_project.triggered.connect(self.load_project)
        self.ui.action_unload_project.triggered.connect(self.unload_project)
        self.ui.action_enter_operation_mode.triggered.connect(self.task_manager.enter_operation_mode)
        self.ui.action_enter_config_mode.triggered.connect(self.task_manager.enter_config_mode)
        self.ui.action_clear_faults.triggered.connect(self.task_manager.clear_faults)
        # Global Variables
        self.ui.action_open_global_variables_dialog.triggered.connect(self.task_manager.open_global_variables)
        # About
        self.ui.action_about_demo_director.triggered.connect(self.show_about_dialog)
        self.ui.action_enable_debug_logs.toggled.connect(self.toggle_debug_logs)

        # Plug-ins
        # SICK PLB -> add if command line option is passed
        if self.args.sick_plb or self.args.sick_plb_pack:
            self.ui.menuPlug_ins = QtWidgets.QMenu(self.ui.menubar)
            self.ui.menuPlug_ins.setObjectName("menuPlug_ins")
            self.ui.menuPlug_ins.setTitle("Plug-ins")
            self.ui.action_sick_plb = QtWidgets.QAction(self)
            self.ui.action_sick_plb.setObjectName("action_sick_plb")
            self.ui.menuPlug_ins.addAction(self.ui.action_sick_plb)
            self.ui.menubar.addAction(self.ui.menuPlug_ins.menuAction())
            self.ui.action_sick_plb.setText("SICK PLB")
            self.create_sick_plb_dialog()
            self.ui.action_sick_plb.triggered.connect(self.show_sick_plb_dialog)

        # automatically add the camera
        if self.args.sick_plb_pack:
            self.splash_screen.set_progress(
                20, "Connecting to SICK PLB..")
            self.add_sick_plb_camera(SickPLBCamera(
                ip='192.168.0.30', name='PACK EXPO PC'))
            self.task_manager.sick_plb_cameras[-1].connect()

        # Define the starting window size
        width = round(self.screen_resolution[0] / 2)
        height = self.screen_resolution[1]
        x = self.screen_resolution[0] - width
        if sys.platform == 'win32':
            y = 30
        elif sys.platform == 'linux':
            y = 0
        self.setGeometry(x, y, width, height)

        # Add a title and task bar icon, and then show the window
        self.window_name: str = "DemoDirector"
        self.setWindowTitle(self.window_name)
        self.setWindowIcon(QIcon(f'{self.file_paths.ui_path}icons/demo_director_icon.png'))
        self.show()

        # Load the cached data now, rather than in TaskManager.__init__ because
        # now all the signals/slots are in place and the task plan menu items will be
        # created properly
        if '--noload' in sys.argv:
            pass  # Don't load the cached data
        else:
            self.splash_screen.set_progress(30, "Loading cached data...")
            self.task_manager.load_cached_data()

        # Create a new project, and then an empty task plan to display on startup
        self.set_enabled_states()

        # Close the splash screen
        self.splash_screen.close()

        #Create Replay Utility
        self.replay_utility = LogReplayUtility()

    def clear_recent_projects(self):
        self.logger.info(f'Clearing recent projects cache.')
        with open(f'{self.file_paths.lib_path}recent_projects.json','w') as fp:
            self.recent_projects = {}
            json.dump(self.recent_projects,fp)
        self.generate_recent_projects_menu()

    def generate_recent_projects_menu(self):
        # Load recent project actions
        self.ui.menu_load_recent.clear()

        if 'recent_projects.json' in os.listdir(f'{self.file_paths.lib_path}'):
            self.logger.info(f'Populating recent project menu option...')
            with open(f'{self.file_paths.lib_path}recent_projects.json','r') as fp:
                self.recent_projects = json.load(fp)
            
            for project in self.recent_projects:
                recent_project = QAction(f'{project}',self)
                recent_project.triggered.connect(self.load_recent_action)
                self.ui.menu_load_recent.addAction(recent_project)

        else: # Create an empty recent projects dictionary
            self.logger.warning(f'No recent projects json file exists, so creating one...')
            with open(f'{self.file_paths.lib_path}recent_projects.json','w') as fp:
                self.recent_projects = {}
                json.dump(self.recent_projects,fp)

        self.ui.menu_load_recent.addSeparator()
        clear_recent_projects = QAction('Clear Recent Projects',self)
        clear_recent_projects.triggered.connect(self.clear_recent_projects)
        self.ui.menu_load_recent.addAction(clear_recent_projects)
    
    def load_recent_action(self):
        project_name = self.sender().text()
        self.logger.info(f'Loading recent project: {project_name}')
        project_path = self.recent_projects[project_name]
        self.task_manager.load_task_plans(path=project_path)

    def toggle_debug_logs(self):
        if self.ui.action_enable_debug_logs.isChecked():
            set_logging_level(log_level=logging.DEBUG)
        else:
            set_logging_level(log_level=logging.INFO)

    def set_enabled_states(self):
        self.logger.info(f'Updating menu bar enabled states.')

        # Set the appliance mode actions first, that way if a project is unloaded or the appliance is disconnected,
        # the mode action states will be overwritten. Otherwise the if statements would also need to check that the project
        # is loaded and that the appliance is connected, and we already have that logic below
        if self.task_manager.mode == 'FAULT':
            self.ui.action_clear_faults.setEnabled(True)
            self.ui.action_enter_config_mode.setEnabled(False)
            self.ui.action_enter_operation_mode.setEnabled(False)
        elif self.task_manager.mode == 'CONFIG':
            self.ui.action_clear_faults.setEnabled(False)
            self.ui.action_enter_config_mode.setEnabled(False)
            self.ui.action_enter_operation_mode.setEnabled(True)
        elif self.task_manager.mode == 'OPERATION':
            self.ui.action_clear_faults.setEnabled(False)
            self.ui.action_enter_config_mode.setEnabled(True)
            self.ui.action_enter_operation_mode.setEnabled(False)
        else:
            self.logger.error(f'Task manager mode attribute is not set.')

        # Based on if the appliance is disconnected, a project is unloaded, or a project is loaded, set the
        # actions available to the user
        if not self.task_manager.connected:
            # If you are not connected, only show the 'connect' menu action
            self.ui.action_connect_to_rtr.setEnabled(True)
            self.ui.action_disconnect_from_rtr.setEnabled(False)
            self.ui.action_load_project.setEnabled(False)
            self.ui.action_unload_project.setEnabled(False)
            self.ui.action_clear_faults.setEnabled(False)
            self.ui.action_enter_config_mode.setEnabled(False)
            self.ui.action_enter_operation_mode.setEnabled(False)
            self.ui.action_save.setEnabled(False)
            self.ui.action_save_as.setEnabled(False)
            self.ui.action_load.setEnabled(False)
            self.ui.menu_load_recent.setEnabled(False)
            self.ui.action_save_as_default.setEnabled(False)
        elif not self.task_manager.load_project_dialog.loaded_project:
            # If you are connected, but there is no project loaded, only show the
            # 'disconnect' and 'load project' menu actions
            self.ui.action_connect_to_rtr.setEnabled(False)
            self.ui.action_disconnect_from_rtr.setEnabled(True)
            self.ui.action_load_project.setEnabled(True)
            self.ui.action_unload_project.setEnabled(False)
            self.ui.action_clear_faults.setEnabled(False)
            self.ui.action_enter_config_mode.setEnabled(False)
            self.ui.action_enter_operation_mode.setEnabled(False)
            self.ui.action_save.setEnabled(False)
            self.ui.action_save_as.setEnabled(False)
            self.ui.action_load.setEnabled(False)
            self.ui.menu_load_recent.setEnabled(False)
            self.ui.action_save_as_default.setEnabled(False)
        else:
            # If you are connected, and a project is loaded, then show
            # 'disconnect', 'unload project'
            # Don't set the appliance mode settings, since those are handled above
            self.ui.action_connect_to_rtr.setEnabled(False)
            self.ui.action_disconnect_from_rtr.setEnabled(True)
            self.ui.action_load_project.setEnabled(False)
            self.ui.action_unload_project.setEnabled(True)
            self.ui.action_save.setEnabled(True)
            self.ui.action_save_as.setEnabled(True)
            self.ui.action_load.setEnabled(True)
            self.ui.menu_load_recent.setEnabled(True)
            self.ui.action_save_as_default.setEnabled(True)

    def connect_to_rtr(self):
        # When the connect action is pressed, show the connect dialog allowing the user to edit
        # connection parameters
        self.task_manager.runtime_controller_dialog.show()

    def disconnect_from_rtr(self):
        # When the disconnect action is pressed, no user input is needed, so call disconnect directly
        self.task_manager.disconnect_commanders()
        self.set_enabled_states()

    def load_project(self):
        # When the load project action is pressed, show the load project dialog allowing the user to select a project
        if not self.task_manager.connected:
            self.logger.error(f'Tried to access the load project dialog without connecting to the Runtime controller.')
            return
        self.task_manager.load_project_dialog.show()

    def unload_project(self):
        # When the unload project action is pressed, no user input is needed
        if not self.task_manager.connected:
            self.logger.error(f'Tried to unload a project without connecting to the Runtime controller.')
            return
        self.task_manager.load_project_dialog.unload_project()
        # Make sure to clear the loaded task plans data
        self.task_manager.project_robots = []
        self.task_manager.clear_loaded_configuration()
        self.ui.action_save_as_default.setChecked(False)
        self.set_enabled_states()

    def set_default_task_plans(self, checked):
        # Sets whether the project name and task plans will be saved as default or not.
        # The default project and task plans will be loaded on application launch.
        self.update_unsaved_changes_status(True)
        self.is_default = checked

    def update_unsaved_changes_status(self, is_unsaved):
        # Sets the window title according to if there are or are not unsaved changes.
        # When there are unsaved changes, an * is appended to the title name and when there
        # are none the * is removed
        title = self.windowTitle()
        if not self.is_unsaved and is_unsaved:
            self.setWindowTitle(f'{title}*')
        elif self.is_unsaved and not is_unsaved:
            title = title.strip('*')
            self.setWindowTitle(title)
        self.is_unsaved = is_unsaved

    def update_window_title(self, is_loaded, config_name, config_path):
        # When a task plan json file is loaded add the name of the file in ()
        # next to the window title
        self.is_unsaved = False
        if is_loaded:
            self.setWindowTitle(f'{self.window_name} ({config_name})')
        else:
            self.setWindowTitle(self.window_name)
            return

        # When the window title is updated, that means the project was saved or loaded, so update
        # the recent projects cache
        max_recent_projects = 10
        new_dict = {config_name:config_path}
        with open(f'{self.file_paths.lib_path}recent_projects.json','r') as fp:
            recent_projects = json.load(fp)
        if config_name in recent_projects:
            recent_projects.pop(config_name)
        new_dict.update(recent_projects)

        if len(new_dict) > max_recent_projects:
            self.logger.info(f'Recent projects exceeded max length, so removing end.')
            new_dict.pop(list(new_dict.keys())[-1])

        with open(f'{self.file_paths.lib_path}recent_projects.json','w') as fp:
            json.dump(new_dict,fp)
        
        self.generate_recent_projects_menu()

    def keyPressEvent(self, event: QKeyEvent):
        key = event.key()
        super().keyPressEvent(event)

    def show_about_dialog(self):
        # When the about menu action is pressed, show the about dialog.
        self.about = AboutDialog(parent=self)
        self.about.show()

    def closeEvent(self, e):
        # Hide the main window, and then force the event to be processed.
        self.logger.info(f'Closing application...')
        self.hide()
        QApplication.processEvents()
        # for task_plan in self.task_manager.task_plans:
        #     task_plan.stop_robots()

    def shutdown(self):
        self.close()

    def create_sick_plb_dialog(self):
        self.sick_plb_widget = QWidget()
        self.sick_plb_dialog = Ui_SickPlugin()
        self.sick_plb_dialog.setupUi(self.sick_plb_widget)
        self.sick_plb_dialog.add_camera_button.clicked.connect(
            self.add_sick_plb_camera)

    def show_sick_plb_dialog(self):
        self.sick_plb_widget.show()

    def add_sick_plb_camera(self, camera: SickPLBCamera):
        if not camera.name:
            camera.name = f'PC {len(self.task_manager.sick_plb_cameras) + 1}'
        self.task_manager.sick_plb_cameras.append(camera)
        self.task_manager.sick_plb_cameras[-1].name_changed_signal.connect(
            self.update_sick_tabs)
        self.sick_plb_dialog.camera_tab_widget.addTab(
            self.task_manager.sick_plb_cameras[-1], camera.name)
        # self.update_sick_plb_dialog()
        self.logger.info(f'Added SICK PC: {camera.name}')
        self.sick_plb_dialog.add_camera_button.setDisabled(True)

    def update_sick_plb_dialog(self):
        self.sick_plb_dialog.camera_tab_widget.clear()
        for camera in self.task_manager.sick_plb_cameras:
            self.sick_plb_dialog.camera_tab_widget.addTab(
                camera, camera.ui.name_lineedit.text())

    def update_sick_tabs(self):
        for idx, camera in enumerate(self.task_manager.sick_plb_cameras):
            self.sick_plb_dialog.camera_tab_widget.setTabText(
                idx, camera.ui.name_lineedit.text())

    def parse_args(self):
        '''Parse command line arguments'''
        parser = argparse.ArgumentParser(
            description='Run DemoDirector application for controlling RapidPlan')
        parser.add_argument(
            '--sick_plb', help='Run the Sick PLB camera plugin', action='store_true')
        parser.add_argument(
            '-s', '--sick_plb_pack', help='Run the Sick PLB camera plugin and automatically add camera at 192.168.0.30', action='store_true')
        parser.add_argument(
            '--noload', help='Do not load cached task plans', action='store_true')
        parser.add_argument(
            '-d', '--debug', help='Enable debug logs.', action='store_true')
        self.args = parser.parse_args()
        self.logger = initialize_logger()
        self.logger.info(
            f'Launching DemoDirector with the following arguments: {self.args}')

    def _check_logging_arg(self):
        '''Set logging level based on command line argument'''
        log_level = logging.INFO
        if self.args.debug:
            log_level = logging.DEBUG
            self.ui.action_enable_debug_logs.setChecked(True)
        set_logging_level(log_level=log_level)

    def set_progress_slot(self, value: int, message: str):
        self.splash_screen.set_progress(value, message)

    def ascii_log_replay(self):
        self.logger.info(f'Opening Replay Manager')
        #self.replay_utility.setupUi()
        self.replay_utility.show()

def main():
    QApplication.setStyle("fusion")
    app = QApplication(sys.argv)
    screen = QDesktopWidget().availableGeometry()
    demo_director = DemoDirector(screen)
    sys.exit(app.exec_())


if __name__ == '__main__':
    main()
