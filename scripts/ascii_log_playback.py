#!/usr/bin/env python3

import argparse
import datetime
import enum
import time
import re
import yaml

from typing import Callable, Tuple, Union

from rtr_appliance import PythonASCIICommander


# Timestamp Formats
def datetime_from_rtr(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from RTR human-readable string"""
    try:
        return datetime.datetime.strptime(dstr, '%Y-%m-%d %H:%M:%S.%f %z')
    except:
        return None


def datetime_from_epoch(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object converted from epoch time"""
    try:
        return datetime.datetime.fromtimestamp(float(dstr))
    except:
        return None


def datetime_from_siemens(dstr: str) -> Union[datetime.datetime, None]:
    """Return datetime object from Siemens timestamp string"""
    try:
        return datetime.datetime.strptime(dstr, "%Y.%m.%d %H:%M:%S.%f")
    except:
        return None


class TimeFmt(enum.Enum):
    RTR = 0
    EPOCH = 1
    SIEMENS = 2


# Mapping from time format to a converter function
GET_TIME = {
    TimeFmt.RTR: datetime_from_rtr,
    TimeFmt.EPOCH: datetime_from_epoch,
    TimeFmt.SIEMENS: datetime_from_siemens
}


def find_ascii_cmd(log_line: str, is_siemens_logs: bool) -> Union[str, None]:
    """Returns ASCII command string given a line from log file"""
    if is_siemens_logs and re.search("Sending command", log_line) is not None:
        return re.search("\{.*\}", log_line)[0]
    elif re.search("ASCII Command", log_line):
        match = re.search("\{.*\}", log_line)

        # Invalid ASCII commands can show up in logs
        if match is not None:
            return match[0]

    return None


RTR_TIMESTAMP_REGEX = "(?<=\[)[^][]*(?=\])"
SIEMENS_TIMESTAMP_REGEX = "(?<=\<)[^><]*(?=\>)"


class CmdData:
    timestamp = None
    cmd = None

    def __init__(self, t, cmd):
        self.timestamp = t
        self.cmd = cmd

    def __repr__(self):
        return f"[{self.timestamp}] {self.cmd}"


def find_timestamp_fmt(log_line: str,
                       timestamp_regex: str) -> Tuple[Union[CmdData, None], Union[Callable, None]]:
    """Determines/validates the timestamp format

    Args:
        log_line (str): line of text from log file
        timestamp_regex (str): regex

    Returns:
        Tuple(TimeFmt, index): time format and regex index

    """
    matches = re.findall(timestamp_regex, log_line)
    for i, m in reversed(list(enumerate(matches))):
        # Try every format and return the type that succeeded + match index
        for fmt, converter in GET_TIME.items():
            if converter(m):
                return (fmt, i)

    return (None, None)


def main():
    # Argparse
    parser = argparse.ArgumentParser(
        description=
        "Extract ASCII commands from appliance logs, then optinally replay commands in order.",
        usage="%(prog)s appliance_log_file [options]")
    parser.add_argument("log_file", help="File path to log file")
    parser.add_argument("-d",
                        "--dry_run",
                        help="Dry run ASCII commands",
                        action="store_true",
                        default=False)
    parser.add_argument("-s",
                        "--siemens",
                        help="Enable if Siemens logs",
                        action="store_true",
                        default=False)
    parser.add_argument("-r",
                        "--range",
                        help="Read section of logs. "
                        "Example: -r T0 Tf finds every message from [T0 Tf]",
                        nargs="+",
                        type=str,
                        default=None)
    args = parser.parse_args()
    fpath = args.log_file
    dry_run = args.dry_run
    is_siemens_logs = args.siemens
    time_range = args.range

    # Init variables
    datetime_range = None
    timestamp_regex = RTR_TIMESTAMP_REGEX
    if is_siemens_logs:
        timestamp_regex = SIEMENS_TIMESTAMP_REGEX

    # Collect commands
    cmd_sequence = []
    time_fmt = None
    match_idx = None
    with open(fpath, "r") as log_file:
        for line in log_file:
            cmd = find_ascii_cmd(line, is_siemens_logs)
            if cmd:
                # Initialize/Setup time format and match index
                if time_fmt is None or match_idx is None:
                    fmt, idx = find_timestamp_fmt(line, timestamp_regex)
                    time_fmt = fmt
                    match_idx = idx

                    # If we could not find either, then abort script
                    if time_fmt is None or match_idx is None:
                        print(f"Failed to parse line: {line}")
                        return

                    # Set time range
                    if time_range:
                        if (len(time_range) != 2):
                            print(
                                f"Error invalid range input: {time_range}. Must be 2 length array.")
                            return
                        datetime_range = [GET_TIME[time_fmt](t) for t in time_range]

                date_str = re.findall(timestamp_regex, line)[match_idx]

                # Convert to datetime object
                d_obj = GET_TIME[time_fmt](date_str)

                # Check if datetime is out of range
                if datetime_range:
                    if d_obj > datetime_range[1]:
                        break
                    if d_obj < datetime_range[0]:
                        continue

                # Extract ascii command
                dict_cmd = yaml.safe_load(cmd)
                cmd_sequence.append(CmdData(d_obj, dict_cmd))

    # Playback Commands
    cmdr = None
    if not dry_run:
        cmdr = PythonASCIICommander.PythonASCIICommander('localhost', 9999)
    for i in range(1, len(cmd_sequence) + 1):
        curr_cmd_data = cmd_sequence[i - 1]
        next_cmd_data = None
        if i < len(cmd_sequence):
            next_cmd_data = cmd_sequence[i]

        tdelta = 0
        if next_cmd_data:
            tdelta = (next_cmd_data.timestamp - curr_cmd_data.timestamp).total_seconds()

        print(curr_cmd_data)
        print(f"Sleep duration before next command: {tdelta}s\n")

        if not dry_run:
            cmdr.send_raw_yaml(curr_cmd_data.cmd)
            time.sleep(tdelta)


if __name__ == "__main__":
    main()
