from pymodbus.client.sync import ModbusTcpClient as ModbusClient


class ModbusTest():
    def __init__(self):
        self.modbus_client = ModbusClient(
            host='192.168.0.61', port=502, stopbits=1, bytesize=8, parity='E', baudrate=115200, timeout=1)

    def connect(self):
        self.modbus_client.connect()
        print(f"Successfully connected to the modbus hardware")

    def close(self):
        self.modbus_client.close()
        print(f"Successfully closed the modbus hardware")

    def read_digital_input(self):
        di_port_value = self.modbus_client.read_discrete_inputs(
            address=0, count=8)
        print(f'Read value is: {di_port_value}')
        di_line_x = di_port_value.bits[0]
        print(di_line_x)

    def write_digital_output(self):
        self.modbus_client.write_coil(
            address=0, value=1)


def main():
    modbus_test = ModbusTest()
    modbus_test.connect()
    modbus_test.read_digital_input()
    # modbus_test.write_digital_output()
    modbus_test.close()


if __name__ == "__main__":
    main()
